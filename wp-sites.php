<?php
$sites = array(
	'mendoza' => array(
		'site_display' => 'Mendoza',
		'bingcode' => '719797437648FB681440DDF25534411C',
		'fancy_name' => 'Mendoza.Travel',
		'locale' => 'es_ES',
		'country' => 'Argentina',
		'static_folder' => 'mendoza',
		'analytics' => 'UA-160767-4',
		'featured_category' => 6,
		'pxsol_return_url' => 'http://reservas.mendoza.travel/list.html',
		'pxsol_ListID' => '328',
		'featured_images' => 10,
		'no_img_thumb_src' => 'no-image-available-277x277.png', 
		'excursions_categories' => array(
			56,		//Excursiones Tradicionales
			57,		//Excursiones a Bodegas
			161
		),
		'redirect_categories' => array(
			'pxsol_cat' => array(
				85, 79, 80, 81, 82, 83 //Hotels.
			),
			'pxsol_uri' => 'http://bit.ly/1pjyWJi',
			'despegar_cat' => array(
				86, 88, 89, 91 //Other kind of hotels.
			),
			'despegar_uri' => 'http://bit.ly/1qdJlmw'
		),
		'ads' => array(
			'pub-id' => 'ca-pub-9191705010011352',
			'exclude_categories' => array(
				56, 57, 166, //Excursiones Tradicionales y Bodegas //Buses
				87, //Cabanas Andinas
				//85, //Hoteles Mendoza
				//82, //Apart Hoteles Mendoza
				//79, //Hoteles 5 Estrellas
				//80, //Hoteles 4 Estrellas
				//81, //Hoteles 3 Estrellas
				//83, //Departamentos en Mendoza
				//86, //Hoteles en Chacras de Coria
				//88, //Hoteles en Valle Grande
				//89, //Hoteles en San Rafael
				//91, //Hoteles en Malargue
				//155, //Hoteles en Mendoza Capital
				//156, //Hoteles Económicos
			),
			'atf-single-right-content' => array(
					'id'    => '8705756166',
					'custom-channel' => '8683906566'
			),
			'btf-single-right-content' => array(
					'id'    => '2659222567',
					'custom-channel' => '9462635766'
			),
			'atf-left-sidebar' => array(
					'id'    => '8705756166',
					'custom-channel' => '5612688969'
			),
			'atf-category-right-content' => array(
					'id'    => '8705756166',
					'custom-channel' => '9829235760'
			),
			'btf-category-right-content' => array(
					'id'    => '8705756166',
					'custom-channel' => '2305968962'
			),
			'atf-left-top-content' => array(
					'id'	=> '4169165763',
					'custom-channel' => '6520354565'
			)
		),
		'email' => array(
			'host' => 'box735.bluehost.com',
			'port' => 465,
			'smtpauth' => true,
			'smtpsecure' => 'ssl',
			'username' => 'noreply@mendoza.travel',
			'password' => 'dbu0j=(0VO5|,',
			'from' => array(
				'name' => 'No Reply',
				'email' => 'noreply@mendoza.travel'
			),
			'default_subject' => array(
				'es'	=> 'Nuevo contacto desde Mendoza.travel',
				'en'	=> 'New message from Mendoza.travel',
				'pt'	=> 'Nouveau message de Mendoza.travel',
			),
			'contact_excursions' => array(
				'excursiones@mendoza.travel'
			)
		),
		'facebook' => array(
			'appID' 		=> '1463022800613932',
			'FBPixelId'		=> '191094691252807',
			'pageID'		=> '120877651270846',

		),
		'recaptcha' => array(
			'key' => array(
				'private' => '6Ldh9PgSAAAAACOzwb3j91h0qK26JBCqg8u6t-o0',
				'public'  => '6Ldh9PgSAAAAADnh_UCG7PRMtoe3dItPKcaMiqy7'
			)
		),
		'widgets' => array(
			'featured_post_category' => 4,
			'recommended_post_category' => 4,
			'top_footer' => array(
				'titles' => array(
					'menu-footer-one'	=> 'Tourist Information',
					'menu-footer-two'	=> 'Tourism Products',
					'menu-footer-three'	=> 'Mendoza Travel Guide',
					'menu-footer-four'	=> 'General Information',
					'menu-footer-five'	=> 'Tourist Guide Wine'
				)
			),
			'main_footer'		=> true,
			'bottom_footer'		=> true,
			'middle_footer'		=> true
		),
		'social' => array(
			'Twitter' => array(
				'site' 		=> '@mendozatravel',
			),
			'Facebook' => array(
				'site_name' 	=> 'Mendoza Travel',
				'appID' 		=> '1463022800613932',
				'default_img' 	=> 'amerian-mendoza-001.jpg'
			),
			'Google' => array(
			)
		),
		'crm' => array(
			'db_name'		=> 'mendozat_crm',
			'db_user'		=> 'mendozat_apicumb',
			'db_password'	=> 'L?#G4gktoCZh',
			'db_host'		=> 'crm.cumbresandinas.com',
			'site_id'		=> 1
		),
		'plugins' => array(
			'booking' => array(
				'enabled'		=> true,
				'aid'			=> '809548',
				'resultpage'	=> 'https://www.booking.com/searchresults.html',
				'si'			=> 'ai,co,ci,re,di',
				'errorurl'		=> '//www.booking.com/searchresults.html?aid=',
				'dest_id'		=> '-1003869',
				'dest_type'		=> 'city'
			),
			'despegar' => array(
				'enabled' 		=> false,
				'affiliate'		=> 'AG51607',
				'country'		=> 'AR',
				'url'			=> 'http://despegar.com/reddeafiliados/tracking/link?params=search/Hotel/',
				'destiny'		=> 'MDZ',
				'autocomplete'	=> '/wp-content/themes/travels/library/despegar/autocomplete.php'
			),
			'default_form' => array(
				'enabled'	=> true,
				'action'	=> 'pxsol/post.php'
			),
			'smtp-validate' => array(
				'enabled' => false,
				'categories' => array(
					56,		//Excursiones Tradicionales
					57		//Excursiones a Bodegas
				)
			),
			'tawkto' => array(
				'enabled' => false,
				'categories' => array(
					56,		//Excursiones Tradicionales
					57		//Excursiones a Bodegas
				)
			)
		)
	),
	'buenosaires' => array(
		'site_display' => 'BuenosAires',
		'bingcode' => '719797437648FB681440DDF25534411C',
		'fancy_name' => 'BuenosAires.Travel',
		'locale' => 'es_ES',
		'country' => 'Argentina',
		'static_folder' => 'buenosaires',
		'analytics' => 'UA-160767-6',
		'featured_category' => 66,
		'pxsol_return_url' => 'http://reservas.buenosaires.travel/list.html',
		'pxsol_ListID' => '341',
		'featured_images' => 10,
		'no_img_thumb_src' => 'no-image-available-277x277.png',
		'excursions_categories' => array(
			41,		//Excursiones Tradicionales
		),
		'redirect_categories' => array(
			'pxsol_cat' => array(
				//85, 79, 80, 81, 82, 83 //Hotels.
			),
			'pxsol_uri' => 'http://bit.ly/1pjyWJi',
			'despegar_cat' => array(
				//86, 88, 89, 91 //Other kind of hotels.
			),
			'despegar_uri' => 'http://bit.ly/1qdJlmw'
		),
		'ads' => array(
			'pub-id' => 'ca-pub-9191705010011352',
			'exclude_categories' => array(
				41, // Excursiones Tradicionales
				//53, //Hoteles 5 Estrellas
				//54, //Hoteles 4 Estrellas
				//55, //Hoteles 3 Estrellas
				//89, //Hoteles 2 Estrellas
				//56, //Apart Hoteles en Buenos Aires
				//57, //Departamentos en Buenos Aires
				//59, //Hoteles Boutique
				//74, //Hoteles en Buenos Aires
				//58, //Hoteles en San Nicolás
				//65, //Hoteles en Retiro
				//60, //Hoteles en Monserrat
				//61, //Hoteles en Recoleta
				//63, //Hoteles en Palermo
				//62, //Hoteles en Puerto Madero
				//64, //Hoteles en San Telmo
				//91, //Hoteles en Buenos Aires Capital
				//90, //Hoteles Económicos
			),
			'atf-single-right-content' => array(
					'id'    => '5385137766',
					'custom-channel' => '8683906566'
			),
			'btf-single-right-content' => array(
					'id'    => '5105936162',
					'custom-channel' => '9462635766'
			),
			'atf-left-sidebar' => array(
					'id'    => '5385137766',
					'custom-channel' => '5612688969'
			),
			'atf-category-right-content' => array(
					'id'    => '5385137766',
					'custom-channel' => '9536135761'
			),
			'btf-category-right-content' => array(
					'id'    => '5385137766',
					'custom-channel' => '2305968962'
			),
			'atf-left-top-content' => array(
					'id'	=> '4169165763',
					'custom-channel' => '2012868960'
			)
		),
		'facebook' => array(
			'appID'			=> '1463022800613932',
			'FBPixelId'		=> '191094691252807',
			'pageID'		=> '120877651270846',
		),
		'email' => array(
			'host' => 'box735.bluehost.com',
			'port' => 465,
			'smtpauth' => true,
			'smtpsecure' => 'ssl',
			'username' => 'no-reply@buenosaires.travel',
			'password' => 'gst9s_m}F7-B',
			'from' => array(
				'name' => 'No Reply',
				'email' => 'no-reply@buenosaires.travel'
			),
			'default_subject' => array(
				'es'	=> 'Nuevo contacto desde BuenosAires.travel',
				'en'	=> 'New message from BuenosAires.travel',
				'pt'	=> 'Nouveau message de BuenosAires.travel',
			),
			'contact_excursions' => array(
				'info@eurotur.tur.ar',
				'excursiones@buenosaires.travel'
			)
		),
		'widgets' => array(
			'featured_post_category' => 4,
			'recommended_post_category' => 4,
			'top_footer' => array(
				'titles' => array(
					'menu-footer-one'	=> 'Tourist Information',
					'menu-footer-two'	=> 'Tourism Products',
					'menu-footer-three'	=> 'Buenos Aires',
					'menu-footer-four'	=> 'General Information',
					'menu-footer-five'	=> 'General Interest'
				)
			),
			'main_footer'		=> true,
			'bottom_footer'		=> true,
			'middle_footer'		=> true
		),
		'social' => array(
			'Twitter' => array(
				'site' 		=> '@buenosairestravel',
			),
			'Facebook' => array(
				'site_name' 	=> 'Buenos Aires Travel',
				'appID' 		=> '1463022800613932',
				'default_img' 	=> 'imperial-park-008.jpg'
			),
			'Google' => array(
			)
		),
		'crm' => array(
			'db_name'		=> 'mendozat_crm',
			'db_user'		=> 'mendozat_apicumb',
			'db_password'	=> 'L?#G4gktoCZh',
			'db_host'		=> 'crm.cumbresandinas.com',
			'site_id'		=> 2
		),
		'plugins' => array(
			'booking' => array(
				'enabled'		=> true,
				'aid'			=> '809548',
				'resultpage'	=> 'https://www.booking.com/searchresults.html',
				'si'			=> 'ai,co,ci,re,di',
				'errorurl'		=> '//www.booking.com/searchresults.html?aid=',
				'dest_id'		=> '-1003869',
				'dest_type'		=> 'city'
			),
			'despegar' => array(
				'enabled' 		=> false,
				'affiliate'		=> 'AG51607',
				'country'		=> 'AR',
				'url'			=> 'http://despegar.com/reddeafiliados/tracking/link?params=search/Hotel/',
				'destiny'		=> 'BUE',
				'autocomplete'	=> '/wp-content/themes/travels/library/despegar/autocomplete.php'
			),
			'default_form' => array(
				'enabled'	=> false,
				'action'	=> 'pxsol/post.php'
			),
			'smtp-validate' => array(
				'enabled' => false,
				'categories' => array(
					56,		//Excursiones Tradicionales
					57		//Excursiones a Bodegas
				)
			),
			'tawkto' => array(
				'enabled' => false,
				'categories' => array(
					56,		//Excursiones Tradicionales
					57		//Excursiones a Bodegas
				)
			)
		)
	),
	'sanrafael' => array(
		'site_display' => 'SanRafael',
		'fancy_name' => 'SanRafael.Travel',
		'locale' => 'es_ES',
		'country' => 'Argentina',
		'static_folder' => 'sanrafael',
		'analytics' => 'UA-47886238-1',
		'featured_category' => 23,
		'pxsol_return_url' => 'http://reservas.sanrafael.travel/list.html',
		'pxsol_ListID' => '341',
		'featured_images' => 10,
		'no_img_thumb_src' => 'no-image-available-277x277.png',
		'excursions_categories' => array(
			41,		//Excursiones Tradicionales
		),
		'redirect_categories' => array(
			'pxsol_cat' => array(
				//85, 79, 80, 81, 82, 83 //Hotels.
			), 
			'pxsol_uri' => 'http://bit.ly/1pjyWJi',
			'despegar_cat' => array(
				//86, 88, 89, 91 //Other kind of hotels.
			),
			'despegar_uri' => 'http://bit.ly/1qdJlmw'
		),
		'ads' => array(
			'pub-id' => 'ca-pub-1594222975674743',
			'exclude_categories' => array(
				//56, 57
			),
			'atf-single-right-content' => array(
					'id'    => '7579044451',
					'custom-channel' => '1532510856'
			),
			'btf-single-right-content' => array(
					'id'    => '4067174854',
					'custom-channel' => '3009244059'
			),
			'atf-left-sidebar' => array(
					'id'    => '7579044451',
					'custom-channel' => '7439443650	'
			),
			'atf-category-right-content' => array(
					'id'    => '7579044451',
					'custom-channel' => '8916176851'
			),
			'btf-category-right-content' => array(
					'id'    => '7579044451',
					'custom-channel' => '1392910050'
			),
			'atf-left-top-content' => array(
					'id'	=> '7579044451',
					'custom-channel' => '2869643258'
			)
		),
		'facebook' => array(
			'appID'			=> '1463022800613932',
			'FBPixelId'		=> '191094691252807',
			'pageID'		=> '120877651270846',
		),
		'email' => array(
			'host' => 'box735.bluehost.com',
			'port' => 465,
			'smtpauth' => true,
			'smtpsecure' => 'ssl',
			'username' => 'no-reply@sanrafael.travel',
			'password' => '.\2T@ls</vt|dwr',
			'from' => array(
				'name' => 'No Reply',
				'email' => 'no-reply@sanrafael.travel'
			),
			'default_subject' => array(
				'es'	=> 'Nuevo contacto desde SanRafael.travel',
				'en'	=> 'New message from SanRafael.travel',
				'pt'	=> 'Nouveau message de SanRafael.travel',
			),
			'contact_excursions' => 'excursiones@sanrafael.travel'
		),
		'widgets' => array(
			'featured_post_category' => 22,
			'recommended_post_category' => 22,
			'top_footer' => array(
				'titles' => array(
					'menu-footer-one'	=> 'Tourist Information',
					'menu-footer-two'	=> 'Tourism Products',
					'menu-footer-three'	=> 'Mendoza Travel Guide',
					'menu-footer-four'	=> 'General Information',
					'menu-footer-five'	=> 'Tourist Guide Wine'
				)
			),
			'main_footer'		=> false,
			'bottom_footer'		=> true,
			'middle_footer'		=> true
		),
		'social' => array(
			'Twitter' => array(
				'site' 		=> '@sanrafaeltravel',
			),
			'Facebook' => array(
				'site_name' 	=> 'San Rafael Travel',
				'appID' 		=> '1463022800613932',
				'default_img' 	=> 'imperial-park-008.jpg'
			),
			'Google' => array(
			)
		),
		'crm' => array(
			'db_name'		=> 'mendozat_crm',
			'db_user'		=> 'mendozat_apicumb',
			'db_password'	=> 'L?#G4gktoCZh',
			'db_host'		=> 'crm.cumbresandinas.com',
			'site_id'		=> 3
		),
		'plugins' => array(
			'booking' => array(
				'enabled'		=> true,
				'aid'			=> '828239',
				'resultpage'	=> 'https://www.booking.com/searchresults.html',
				'si'			=> 'ai,co,ci,re,di',
				'errorurl'		=> '//www.booking.com/searchresults.html?aid=',
				'dest_id'		=> '-1013899',
				'dest_type'		=> 'city'
			),
			'despegar' => array(
				'enabled' 		=> false,
				'affiliate'		=> 'AG51607',
				'country'		=> 'AR',
				'url'			=> 'http://despegar.com/reddeafiliados/tracking/link?params=search/Hotel/',
				'destiny'		=> 'BUE',
				'autocomplete'	=> '/wp-content/themes/travels/library/despegar/autocomplete.php'
			),
			'default_form' => array(
				'enabled'	=> false,
				'action'	=> 'pxsol/post.php'
			),
			'smtp-validate' => array(
				'enabled' => false,
				'categories' => array(
					56,		//Excursiones Tradicionales
					57		//Excursiones a Bodegas
				)
			),
			'tawkto' => array(
				'enabled' => false,
				'categories' => array(
					56,		//Excursiones Tradicionales
					57		//Excursiones a Bodegas
				)
			)
		)
	)
);
?>
