<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Include environment configuration. */
(!file_exists(ABSPATH . 'wp-environments.php')) ? die('Missing Environments File!') :require_once(ABSPATH . 'wp-environments.php');

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', $environment['DB_NAME']);

/** MySQL database username */
define('DB_USER', $environment['DB_USER']);

/** MySQL database password */
define('DB_PASSWORD', $environment['DB_PASSWORD']);

/** MySQL hostname */
define('DB_HOST', $environment['DB_HOST']);

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', $environment['DB_CHARSET']);

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', $environment['DB_COLLATE']);

define('DISALLOW_FILE_EDIT', true);

/** Dynamically set WP_SITEURL based on $_SERVER['SERVER_NAME']. */
define('WP_SITEURL', 'http://' . ($_SERVER['HTTP_HOST'] ?: $_SERVER['SERVER_NAME']));

/** Dynamically set WP_HOME based on $_SERVER['SERVER_NAME']. */
define('WP_HOME', 'http://' . ($_SERVER['HTTP_HOST'] ?: $_SERVER['SERVER_NAME']));

/** Force Wordpress Admin over SSL */
define('FORCE_SSL_ADMIN', $environment['FORCE_SSL_ADMIN']);

/** Set Default Theme for Wordpress. */
define('WP_DEFAULT_THEME', 'travels');

/** Define custom media upload folder. */
define('UPLOADS', $environment['UPLOADS']);

/** Configure WP cronjobs */
define('DISABLE_WP_CRON', $environment['DISABLE_WP_CRON']);

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = $environment['TABLE_PREFIX'];

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', $environment['WP_DEBUG']);

/**
 * This will log all errors notices and warnings to a file called debug.log in
 * wp-content only when WP_DEBUG is true. if Apache does not have write permission,
 * you may need to create the file first and set the appropriate permissions (i.e. use 666).
 */

/** Display PHP errors. Same as display_errors = On in php.ini file. */
define('WP_DEBUG_DISPLAY', $environment['WP_DEBUG_DISPLAY']);

/** Write a debug.log by default on /wp-content/debug.log */
define('WP_DEBUG_LOG', $environment['WP_DEBUG_LOG']);

/** The uncompressed versions of scripts and stylesheets in wp-includes/js, wp-includes/css, wp-admin/js, and wp-admin/css will be loaded instead of the .min.css and .min.js versions. */
define('SCRIPT_DEBUG', $environment['SCRIPT_DEBUG']);

/** The SAVEQUERIES definition saves the database queries to an array and that array can be displayed to help analyze those queries. The information saves each query, what function called it, and how long that query took to execute. */
define('SAVEQUERIES', $environment['SAVEQUERIES']);

/** Block external URL requests by defining WP_HTTP_BLOCK_EXTERNAL */
define('WP_HTTP_BLOCK_EXTERNAL', $environment['WP_HTTP_BLOCK_EXTERNAL']);

/** The constant WP_ACCESSIBLE_HOSTS will allow additional hosts to go through for requests. Example: define('WP_ACCESSIBLE_HOSTS', '*.github.com, google.com'); */
define('WP_ACCESSIBLE_HOSTS', $environment['WP_ACCESSIBLE_HOSTS']);

/** The WP_CACHE setting, if true, includes the wp-content/advanced-cache.php script, when executing wp-settings.php. */
define('WP_CACHE', $environment['WP_CACHE']);

/** API settings */
define('API_URL', $environment['API_URL']);
define('API_VERSION', $environment['API_VERSION']);
define('API_APPID', $environment['API_APPID']);
define('API_SECRET', $environment['API_SECRET']);

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

/** Include Sites configuration. */
(!file_exists(ABSPATH . 'wp-sites.php')) ? die('Missing Sites File!') :require_once(ABSPATH . 'wp-sites.php');

if (empty($sites)) {
	echo "No sites found.\n";
	die;
}

if (defined('SC')) {
	if (array_key_exists(SC, $sites)) {
		$site = $sites[SC];
	} else {
		echo "Site not found in config.\n";
		die;
	}
} else {
	echo "SC constant is not defined.\n";
	die;
}
