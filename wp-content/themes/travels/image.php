<?php

/*
 * Redirect all images url to post parent.
 *
 */

wp_redirect(get_permalink($post->post_parent), 301);
exit;
?>