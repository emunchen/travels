<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html <?=set_scheme_org('Article');?> class="no-js"> <!--<![endif]-->
<head>
	<title><?php bloginfo('name'); ?><?php wp_title(); ?></title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
	<link rel="stylesheet" href="<?php echo get_bloginfo('template_url'); ?>/css/bootstrap.min.css">
	<link href="<?php echo get_bloginfo('template_url'); ?>/img/<?=SC;?>/favicon.ico" rel="icon" type="image/x-icon" />
	<link href='https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="<?php echo get_bloginfo('template_url'); ?>/css/<?=$GLOBALS['sites'][SC]['static_folder']?>/style.css">
	<link rel="stylesheet" href="<?php echo get_bloginfo('template_url'); ?>/css/jquery-ui-1.10.4.custom.min.css">
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
	<link rel="stylesheet" href="<?php echo get_bloginfo('template_url'); ?>/css/bjqs.css">
	<?=getBingMeta(); ?>
	<?php wp_head(); ?>
	<?=social_markup(); ?>
	<?=get_auto_ads()?>
</head>
<body>
	<!--[if lt IE 7]>
		<p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
	<![endif]-->
<div class="navbar navbar-inverse tr-navbar">
	<div class="container header-container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand hidden-xs" href="<?php echo home_url() .'/'. qtranxf_getLanguage(); ?>"><img src="<?php echo get_bloginfo('template_url'); ?>/img/<?php echo $GLOBALS['sites'][SC]['static_folder']?>/logo.png" class="img-responsive" width="300px" /></a>
			<a class="navbar-brand hidden-lg hidden-md hidden-sm" href="<?php echo home_url() .'/'. qtranxf_getLanguage(); ?>"><img src="<?php echo get_bloginfo('template_url'); ?>/img/<?php echo $GLOBALS['sites'][SC]['static_folder']?>/logo.png" class="img-responsive" width="230px" /></a>
			<?php custom_qTransalte_menu(); ?>
		</div>
		
		<?php wp_nav_menu( array(
			'theme_location'	=> 'primary',
			'menu'				=> 'Header Menu',
			'container'			=> 'div',
			'container_class'	=> 'navbar-collapse collapse',
			'menu_class'		=> 'nav navbar-nav tr-navbar-nav'
		)); ?>
	</div>
</div>
