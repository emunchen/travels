<div class="tr-category-sidebar">
	<div class="tr-ad-sidebar col-md-12">
		<?=google_ads('atf-left-sidebar', 'ad-margin-bottom', 'rectangle'); ?>
	</div>
	<div class="tr-form form-category col-md-12">
		<?php
			if (is_single()):
				echo displayForm('single-top');
			else:
				echo displayForm('category');
			endif;
		?>
	</div>
	<div class="clearfix">
		<?php dynamic_sidebar ('Category Left Sidebar'); ?>
	</div>
</div>
