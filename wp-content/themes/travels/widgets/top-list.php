<?php
// Creating the widget 
class top_list_widget extends WP_Widget {

	function __construct() {
		parent::__construct(
		// Base ID of your widget
		'top_list_widget',
		// Widget name will appear in UI
		__('Show List Posts on Category', 'top_list_widget_domain'),
		// Widget description
		array( 'description' => __( 'Show Top Posts', 'top_list_widget_domain' ) ) 
		);
	}

	// Creating widget front-end
	// This is where the action happens
	public function widget( $args, $instance ) { ?>

		<li class="tr-footer-main-li">
			<?php	$title = apply_filters( 'widget_title', $instance['title'] ); ?>
			<?php	echo $args['before_widget']; ?>
			<?php	if ( ! empty( $title ) ) ?>
				<?php	echo $args['before_title'] . $title . $args['after_title']; ?>
			<ul class="tr-footer-top-list">
				<?php
				$cat = $instance['category'];
				global $post;
				$args = array( 'posts_per_page' => 10, 'category' => $cat );
				$posts = get_posts( $args ); ?>
				<?php foreach ( $posts as $post ) : setup_postdata( $post ); ?>
					<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
				<?php endforeach; ?>
				<?php wp_reset_postdata(); ?>
			</ul>
		</li>

<?php	}

	// Widget Backend
	public function form( $instance ) {
		if ( isset( $instance[ 'title' ] ) ) {
			$title = $instance[ 'title' ];
		} else {
			$title = __( 'New title', 'top_list_widget_domain' );
		}
		// Widget admin form
		?>

		<p>
		<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Título:' ); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>
		<p>
		<label for="<?php echo $this->get_field_id( 'category' ); ?>"><?php _e( 'Categoria:' ); ?></label>
		<?php $categories = get_categories(); ?>

		<select id="<?php echo $this->get_field_id( 'category' ); ?>" name="<?php echo $this->get_field_name( 'category' ); ?>" class="widefat categories" style="width:100%;">
		<option value="">Seleccionar</option>
		<?php foreach($categories as $category) { ?>
		<option value="<?php echo $category->term_id; ?>" <?php if ($category->term_id == $instance['category']) echo 'selected="selected"'; ?>><?php echo $category->name; ?></option>
		<?php } ?>
		</select>
		</p>

	<?php
	}

	// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['category'] = $new_instance['category'];
		return $instance;
	}
}

// Register and load the widget
function top_list_load_widget() {
	register_widget( 'top_list_widget' );
}
add_action( 'widgets_init', 'top_list_load_widget' );
?>
