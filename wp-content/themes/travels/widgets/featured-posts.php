<?php
// Creating the widget 
class featured_widget extends WP_Widget {

	function __construct() {
		parent::__construct(
		// Base ID of your widget
		'featured_widget',
		// Widget name will appear in UI
		__('Show Featured Posts', 'featured_widget_domain'),
		// Widget description
		array( 'description' => __( 'Show Featured Posts on Home Page', 'featured_widget_domain' ) ) 
		);
	}

	// Creating widget front-end
	// This is where the action happens
	public function widget( $args, $instance ) {

		echo $args['before_title'];
		$title = _e('Featured Hotels', 'travels');
		echo $args['after_title'];
		echo $args['before_widget'];

		if ( ! empty( $title ) )
			echo $args['before_title'] . $title . $args['after_title'];

		$posts = array(
			$instance['post_1'],
			$instance['post_2'],
			$instance['post_3'],
			$instance['post_4'],
			$instance['post_5'],
			$instance['post_6'],
			$instance['post_7'],
			$instance['post_8'],
			$instance['post_9'],
			$instance['post_10'],
			$instance['post_11'],
			$instance['post_12'],
			$instance['post_13'],
			$instance['post_14'],
		);
		$arg = array( 'post__in' => $posts, 'orderby' => 'post__in', 'order' => 'ASC', 'posts_per_page' => -1 );
		$the_query = new WP_Query( $arg );
		$i = 1; ?>

		<?php while ( $the_query->have_posts() ) {
			$the_query->the_post(); 
			$url = $instance['post_' .$i. '_url'];
			$location = $instance['post_' .$i. '_location'];
			$stars = $instance['post_' .$i. '_stars']; ?>
				<div class="tr-featured__item col-xs-12 col-sm-6 col-md-6 col-lg-4">
					<div class="tr-featured__item__img">
						<a title="Reservar en <?php the_title(); ?>" href="<?php if (!empty($url)) { echo $url; } else { the_permalink(); } ?>">
							<?=the_post_thumbnail('travel-featured-thumbnail', array('class' => 'img-responsive tr-home-featured')); ?>
						</a>
					</div>
					<div class="tr-featured__item__box">
						<a title="Reservar en <?php the_title(); ?>" href="<?php if (!empty($url)) { echo $url; } else { the_permalink(); } ?>">
							<h4><?php the_title(); ?></h4>
						</a>
						<p><?php if (!empty($location)) { echo $location; } else { echo 'Capital'; } ?></p>
						<span class="mt-sprite tr-featured__stars <?=$stars;?>-stars"></span>
						<a title="Reservar en <?php the_title(); ?>" href="<?php if (!empty($url)) { echo $url; } else { the_permalink(); } ?>">
							<span class="tr-featured__arrow">
								<img width="12px" src="<?=get_template_directory_uri(); ?>/img/arrow-right.png">
							</span>
						</a>
					</div>
				</div>
			<?php $i++;  ?>
		<?php } ?>

	<?php echo $args['after_widget'];
	}

	// Widget Backend
	public function form( $instance ) {

		if ( isset( $instance[ 'post_1_url' ] ) ) {
			$post_1_url = $instance[ 'post_1_url' ];
		} else {
			$post_1_url = '';
		}

		if ( isset( $instance[ 'post_2_url' ] ) ) {
			$post_2_url = $instance[ 'post_2_url' ];
		} else {
			$post_2_url = '';
		}

		if ( isset( $instance[ 'post_3_url' ] ) ) {
			$post_3_url = $instance[ 'post_3_url' ];
		} else {
			$post_3_url = '';
		}

		if ( isset( $instance[ 'post_4_url' ] ) ) {
			$post_4_url = $instance[ 'post_4_url' ];
		} else {
			$post_4_url = '';
		}

		if ( isset( $instance[ 'post_5_url' ] ) ) {
			$post_5_url = $instance[ 'post_5_url' ];
		} else {
			$post_5_url = '';
		}

		if ( isset( $instance[ 'post_6_url' ] ) ) {
			$post_6_url = $instance[ 'post_6_url' ];
		} else {
			$post_6_url = '';
		}

		if ( isset( $instance[ 'post_7_url' ] ) ) {
			$post_7_url = $instance[ 'post_7_url' ];
		} else {
			$post_7_url = '';
		}

		if ( isset( $instance[ 'post_8_url' ] ) ) {
			$post_8_url = $instance[ 'post_8_url' ];
		} else {
			$post_8_url = '';
		}

		if ( isset( $instance[ 'post_9_url' ] ) ) {
			$post_9_url = $instance[ 'post_9_url' ];
		} else {
			$post_9_url = '';
		}

		if ( isset( $instance[ 'post_10_url' ] ) ) {
			$post_10_url = $instance[ 'post_10_url' ];
		} else {
			$post_10_url = '';
		}

		if ( isset( $instance[ 'post_11_url' ] ) ) {
			$post_11_url = $instance[ 'post_11_url' ];
		} else {
			$post_11_url = '';
		}

		if ( isset( $instance[ 'post_12_url' ] ) ) {
			$post_12_url = $instance[ 'post_12_url' ];
		} else {
			$post_12_url = '';
		}

		if ( isset( $instance[ 'post_13_url' ] ) ) {
			$post_13_url = $instance[ 'post_13_url' ];
		} else {
			$post_13_url = '';
		}

		if ( isset( $instance[ 'post_14_url' ] ) ) {
			$post_14_url = $instance[ 'post_14_url' ];
		} else {
			$post_14_url = '';
		}

		if ( isset( $instance[ 'post_1_location' ] ) ) {
			$post_1_location = $instance[ 'post_1_location' ];
		} else {
			$post_1_location = '';
		}

		if ( isset( $instance[ 'post_2_location' ] ) ) {
			$post_2_location = $instance[ 'post_2_location' ];
		} else {
			$post_2_location = '';
		}

		if ( isset( $instance[ 'post_3_location' ] ) ) {
			$post_3_location = $instance[ 'post_3_location' ];
		} else {
			$post_3_location = '';
		}

		if ( isset( $instance[ 'post_4_location' ] ) ) {
			$post_4_location = $instance[ 'post_4_location' ];
		} else {
			$post_4_location = '';
		}

		if ( isset( $instance[ 'post_5_location' ] ) ) {
			$post_5_location = $instance[ 'post_5_location' ];
		} else {
			$post_5_location = '';
		}

		if ( isset( $instance[ 'post_6_location' ] ) ) {
			$post_6_location = $instance[ 'post_6_location' ];
		} else {
			$post_6_location = '';
		}

		if ( isset( $instance[ 'post_7_location' ] ) ) {
			$post_7_location = $instance[ 'post_7_location' ];
		} else {
			$post_7_location = '';
		}

		if ( isset( $instance[ 'post_8_location' ] ) ) {
			$post_8_location = $instance[ 'post_8_location' ];
		} else {
			$post_8_location = '';
		}

		if ( isset( $instance[ 'post_9_location' ] ) ) {
			$post_9_location = $instance[ 'post_9_location' ];
		} else {
			$post_9_location = '';
		}

		if ( isset( $instance[ 'post_10_location' ] ) ) {
			$post_10_location = $instance[ 'post_10_location' ];
		} else {
			$post_10_location = '';
		}

		if ( isset( $instance[ 'post_11_location' ] ) ) {
			$post_11_location = $instance[ 'post_10_location' ];
		} else {
			$post_11_location = '';
		}

		if ( isset( $instance[ 'post_12_location' ] ) ) {
			$post_12_location = $instance[ 'post_12_location' ];
		} else {
			$post_12_location = '';
		}

		if ( isset( $instance[ 'post_13_location' ] ) ) {
			$post_13_location = $instance[ 'post_13_location' ];
		} else {
			$post_13_location = '';
		}

		if ( isset( $instance[ 'post_14_location' ] ) ) {
			$post_14_location = $instance[ 'post_14_location' ];
		} else {
			$post_14_location = '';
		}

		?>

		<?php // 1 Post ?>

		<div class="featured_widget_box">
			<label for="<?php echo $this->get_field_id( 'post_1' ); ?>"><?php _e( 'Primer Hotel:' ); ?></label>
			<?php
			$featured_category_post = $GLOBALS['sites'][SC]['widgets']['featured_post_category'];
			$args = array( 'posts_per_page' => 99, 'category' => $featured_category_post, 'post_type' => 'post' );
			$hotels = get_posts( $args ); ?>

			<select id="<?php echo $this->get_field_id( 'post_1' ); ?>" name="<?php echo $this->get_field_name( 'post_1' ); ?>" class="widefat categories" style="width:100%;">
			<option value=""></option>
			<?php foreach($hotels as $hotel) { ?>
			<option value='<?php echo $hotel->ID; ?>' <?php if ($hotel->ID == $instance['post_1']) echo 'selected="selected"'; ?>><?php echo _e($hotel->post_title); ?></option>
			<?php } ?>
			</select>
			</p>

			<p>
			<label for="<?php echo $this->get_field_id( 'post_1_url' ); ?>"><?php _e( 'Hotel URL:' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'post_1_url' ); ?>" name="<?php echo $this->get_field_name( 'post_1_url' ); ?>" type="text" value="<?php echo esc_attr( $post_1_url ); ?>" />
			</p>

			<p>
			<label for="<?php echo $this->get_field_id( 'post_1_location' ); ?>"><?php _e( 'Ubicación:' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'post_1_location' ); ?>" name="<?php echo $this->get_field_name( 'post_1_location' ); ?>" type="text" value="<?php echo esc_attr( $post_1_location ); ?>" />
			</p>

			<select id="<?php echo $this->get_field_id( 'post_1_stars' ); ?>" name="<?php echo $this->get_field_name( 'post_1_stars' ); ?>" class="widefat categories" style="width:100%;">
				<option value="one" <?php if ('one' == $instance['post_1_stars']) echo 'selected="selected"'; ?>>1 Estrella</option>
				<option value="two" <?php if ('two' == $instance['post_1_stars']) echo 'selected="selected"'; ?>>2 Estrellas</option>
				<option value="three" <?php if ('three' == $instance['post_1_stars']) echo 'selected="selected"'; ?>>3 Estrellas</option>
				<option value="four" <?php if ('four' == $instance['post_1_stars']) echo 'selected="selected"'; ?>>4 Estrellas</option>
				<option value="five" <?php if ('five' == $instance['post_1_stars']) echo 'selected="selected"'; ?>>5 Estrellas</option>
			</select>
		</div>

		<?php // 2 Post ?>

		
		<div class="featured_widget_box">
			<label for="<?php echo $this->get_field_id( 'post_2' ); ?>"><?php _e( 'Segundo Hotel:' ); ?></label>
			<select id="<?php echo $this->get_field_id( 'post_2' ); ?>" name="<?php echo $this->get_field_name( 'post_2' ); ?>" class="widefat categories" style="width:100%;">
			<option value=""></option>
			<?php foreach($hotels as $hotel) { ?>
			<option value='<?php echo $hotel->ID; ?>' <?php if ($hotel->ID == $instance['post_2']) echo 'selected="selected"'; ?>><?php echo _e($hotel->post_title); ?></option>
			<?php } ?>
			</select>
			</p>

			<p>
			<label for="<?php echo $this->get_field_id( 'post_2_url' ); ?>"><?php _e( 'Hotel URL:' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'post_2_url' ); ?>" name="<?php echo $this->get_field_name( 'post_2_url' ); ?>" type="text" value="<?php echo esc_attr( $post_2_url ); ?>" />
			</p>

			<p>
			<label for="<?php echo $this->get_field_id( 'post_2_location' ); ?>"><?php _e( 'Ubicación:' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'post_2_location' ); ?>" name="<?php echo $this->get_field_name( 'post_2_location' ); ?>" type="text" value="<?php echo esc_attr( $post_2_location ); ?>" />
			</p>

			<select id="<?php echo $this->get_field_id( 'post_2_stars' ); ?>" name="<?php echo $this->get_field_name( 'post_2_stars' ); ?>" class="widefat categories" style="width:100%;">
				<option value="one" <?php if ('one' == $instance['post_2_stars']) echo 'selected="selected"'; ?>>1 Estrella</option>
				<option value="two" <?php if ('two' == $instance['post_2_stars']) echo 'selected="selected"'; ?>>2 Estrellas</option>
				<option value="three" <?php if ('three' == $instance['post_2_stars']) echo 'selected="selected"'; ?>>3 Estrellas</option>
				<option value="four" <?php if ('four' == $instance['post_2_stars']) echo 'selected="selected"'; ?>>4 Estrellas</option>
				<option value="five" <?php if ('five' == $instance['post_2_stars']) echo 'selected="selected"'; ?>>5 Estrellas</option>
			</select>
		</div>

		<?php // 3 Post ?>

		<div class="featured_widget_box">
			<label for="<?php echo $this->get_field_id( 'post_3' ); ?>"><?php _e( 'Tercer Hotel:' ); ?></label>
			<select id="<?php echo $this->get_field_id( 'post_3' ); ?>" name="<?php echo $this->get_field_name( 'post_3' ); ?>" class="widefat categories" style="width:100%;">
			<option value=""></option>
			<?php foreach($hotels as $hotel) { ?>
			<option value='<?php echo $hotel->ID; ?>' <?php if ($hotel->ID == $instance['post_3']) echo 'selected="selected"'; ?>><?php echo _e($hotel->post_title); ?></option>
			<?php } ?>
			</select>
			</p>

			<p>
			<label for="<?php echo $this->get_field_id( 'post_3_url' ); ?>"><?php _e( 'Hotel URL:' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'post_3_url' ); ?>" name="<?php echo $this->get_field_name( 'post_3_url' ); ?>" type="text" value="<?php echo esc_attr( $post_3_url ); ?>" />
			</p>

			<p>
			<label for="<?php echo $this->get_field_id( 'post_3_location' ); ?>"><?php _e( 'Ubicación:' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'post_3_location' ); ?>" name="<?php echo $this->get_field_name( 'post_3_location' ); ?>" type="text" value="<?php echo esc_attr( $post_3_location ); ?>" />
			</p>

			<select id="<?php echo $this->get_field_id( 'post_3_stars' ); ?>" name="<?php echo $this->get_field_name( 'post_3_stars' ); ?>" class="widefat categories" style="width:100%;">
				<option value="one" <?php if ('one' == $instance['post_3_stars']) echo 'selected="selected"'; ?>>1 Estrella</option>
				<option value="two" <?php if ('two' == $instance['post_3_stars']) echo 'selected="selected"'; ?>>2 Estrellas</option>
				<option value="three" <?php if ('three' == $instance['post_3_stars']) echo 'selected="selected"'; ?>>3 Estrellas</option>
				<option value="four" <?php if ('four' == $instance['post_3_stars']) echo 'selected="selected"'; ?>>4 Estrellas</option>
				<option value="five" <?php if ('five' == $instance['post_3_stars']) echo 'selected="selected"'; ?>>5 Estrellas</option>
			</select>
		</div>

		<?php // 4 Post ?>

		<div class="featured_widget_box">
			<label for="<?php echo $this->get_field_id( 'post_4' ); ?>"><?php _e( 'Cuarto Hotel:' ); ?></label>
			<select id="<?php echo $this->get_field_id( 'post_4' ); ?>" name="<?php echo $this->get_field_name( 'post_4' ); ?>" class="widefat categories" style="width:100%;">
			<option value=""></option>
			<?php foreach($hotels as $hotel) { ?>
				<option value='<?php echo $hotel->ID; ?>' <?php if ($hotel->ID == $instance['post_4']) echo 'selected="selected"'; ?>><?php echo _e($hotel->post_title); ?></option>
			<?php } ?>
			</select>
			</p>

			<p>
			<label for="<?php echo $this->get_field_id( 'post_4_url' ); ?>"><?php _e( 'Hotel URL:' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'post_4_url' ); ?>" name="<?php echo $this->get_field_name( 'post_4_url' ); ?>" type="text" value="<?php echo esc_attr( $post_4_url ); ?>" />
			</p>

			<p>
			<label for="<?php echo $this->get_field_id( 'post_4_location' ); ?>"><?php _e( 'Ubicación:' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'post_4_location' ); ?>" name="<?php echo $this->get_field_name( 'post_4_location' ); ?>" type="text" value="<?php echo esc_attr( $post_4_location ); ?>" />
			</p>

			<select id="<?php echo $this->get_field_id( 'post_4_stars' ); ?>" name="<?php echo $this->get_field_name( 'post_4_stars' ); ?>" class="widefat categories" style="width:100%;">
				<option value="one" <?php if ('one' == $instance['post_4_stars']) echo 'selected="selected"'; ?>>1 Estrella</option>
				<option value="two" <?php if ('two' == $instance['post_4_stars']) echo 'selected="selected"'; ?>>2 Estrellas</option>
				<option value="three" <?php if ('three' == $instance['post_4_stars']) echo 'selected="selected"'; ?>>3 Estrellas</option>
				<option value="four" <?php if ('four' == $instance['post_4_stars']) echo 'selected="selected"'; ?>>4 Estrellas</option>
				<option value="five" <?php if ('five' == $instance['post_4_stars']) echo 'selected="selected"'; ?>>5 Estrellas</option>
			</select>
		</div>

		<?php // 5 Post ?>

		<div class="featured_widget_box">		
			<label for="<?php echo $this->get_field_id( 'post_5' ); ?>"><?php _e( 'Quinto Hotel:' ); ?></label>
			<select id="<?php echo $this->get_field_id( 'post_5' ); ?>" name="<?php echo $this->get_field_name( 'post_5' ); ?>" class="widefat categories" style="width:100%;">
			<option value=""></option>
			<?php foreach($hotels as $hotel) { ?>
			<option value='<?php echo $hotel->ID; ?>' <?php if ($hotel->ID == $instance['post_5']) echo 'selected="selected"'; ?>><?php echo _e($hotel->post_title); ?></option>
			<?php } ?>
			</select>
			</p>

			<p>
			<label for="<?php echo $this->get_field_id( 'post_5_url' ); ?>"><?php _e( 'Hotel URL:' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'post_5_url' ); ?>" name="<?php echo $this->get_field_name( 'post_5_url' ); ?>" type="text" value="<?php echo esc_attr( $post_5_url ); ?>" />
			</p>

			<p>
			<label for="<?php echo $this->get_field_id( 'post_5_location' ); ?>"><?php _e( 'Ubicación:' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'post_5_location' ); ?>" name="<?php echo $this->get_field_name( 'post_5_location' ); ?>" type="text" value="<?php echo esc_attr( $post_5_location ); ?>" />
			</p>

			<select id="<?php echo $this->get_field_id( 'post_5_stars' ); ?>" name="<?php echo $this->get_field_name( 'post_5_stars' ); ?>" class="widefat categories" style="width:100%;">
				<option value="one" <?php if ('one' == $instance['post_5_stars']) echo 'selected="selected"'; ?>>1 Estrella</option>
				<option value="two" <?php if ('two' == $instance['post_5_stars']) echo 'selected="selected"'; ?>>2 Estrellas</option>
				<option value="three" <?php if ('three' == $instance['post_5_stars']) echo 'selected="selected"'; ?>>3 Estrellas</option>
				<option value="four" <?php if ('four' == $instance['post_5_stars']) echo 'selected="selected"'; ?>>4 Estrellas</option>
				<option value="five" <?php if ('five' == $instance['post_5_stars']) echo 'selected="selected"'; ?>>5 Estrellas</option>
			</select>
		</div>

		<?php // 6 Post ?>

		<div class="featured_widget_box">
			<label for="<?php echo $this->get_field_id( 'post_6' ); ?>"><?php _e( 'Sexto Hotel:' ); ?></label>
			<select id="<?php echo $this->get_field_id( 'post_6' ); ?>" name="<?php echo $this->get_field_name( 'post_6' ); ?>" class="widefat categories" style="width:100%;">
			<option value=""></option>
			<?php foreach($hotels as $hotel) { ?>
			<option value='<?php echo $hotel->ID; ?>' <?php if ($hotel->ID == $instance['post_6']) echo 'selected="selected"'; ?>><?php echo _e($hotel->post_title); ?></option>
			<?php } ?>
			</select>
			</p>

			<p>
			<label for="<?php echo $this->get_field_id( 'post_6_url' ); ?>"><?php _e( 'Hotel URL:' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'post_6_url' ); ?>" name="<?php echo $this->get_field_name( 'post_6_url' ); ?>" type="text" value="<?php echo esc_attr( $post_6_url ); ?>" />
			</p>

			<p>
			<label for="<?php echo $this->get_field_id( 'post_6_location' ); ?>"><?php _e( 'Ubicación:' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'post_6_location' ); ?>" name="<?php echo $this->get_field_name( 'post_6_location' ); ?>" type="text" value="<?php echo esc_attr( $post_6_location ); ?>" />
			</p>

			<select id="<?php echo $this->get_field_id( 'post_6_stars' ); ?>" name="<?php echo $this->get_field_name( 'post_6_stars' ); ?>" class="widefat categories" style="width:100%;">
				<option value="one" <?php if ('one' == $instance['post_6_stars']) echo 'selected="selected"'; ?>>1 Estrella</option>
				<option value="two" <?php if ('two' == $instance['post_6_stars']) echo 'selected="selected"'; ?>>2 Estrellas</option>
				<option value="three" <?php if ('three' == $instance['post_6_stars']) echo 'selected="selected"'; ?>>3 Estrellas</option>
				<option value="four" <?php if ('four' == $instance['post_6_stars']) echo 'selected="selected"'; ?>>4 Estrellas</option>
				<option value="five" <?php if ('five' == $instance['post_6_stars']) echo 'selected="selected"'; ?>>5 Estrellas</option>
			</select>
		</div>

		<?php // 7 Post ?>

		<div class="featured_widget_box">
			<label for="<?php echo $this->get_field_id( 'post_7' ); ?>"><?php _e( 'Séptimo Hotel:' ); ?></label>
			<select id="<?php echo $this->get_field_id( 'post_7' ); ?>" name="<?php echo $this->get_field_name( 'post_7' ); ?>" class="widefat categories" style="width:100%;">
			<option value=""></option>
			<?php foreach($hotels as $hotel) { ?>
			<option value='<?php echo $hotel->ID; ?>' <?php if ($hotel->ID == $instance['post_7']) echo 'selected="selected"'; ?>><?php echo _e($hotel->post_title); ?></option>
			<?php } ?>
			</select>
			</p>

			<p>
			<label for="<?php echo $this->get_field_id( 'post_7_url' ); ?>"><?php _e( 'Hotel URL:' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'post_7_url' ); ?>" name="<?php echo $this->get_field_name( 'post_7_url' ); ?>" type="text" value="<?php echo esc_attr( $post_7_url ); ?>" />
			</p>

			<p>
			<label for="<?php echo $this->get_field_id( 'post_7_location' ); ?>"><?php _e( 'Ubicación:' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'post_7_location' ); ?>" name="<?php echo $this->get_field_name( 'post_7_location' ); ?>" type="text" value="<?php echo esc_attr( $post_7_location ); ?>" />
			</p>

			<select id="<?php echo $this->get_field_id( 'post_7_stars' ); ?>" name="<?php echo $this->get_field_name( 'post_7_stars' ); ?>" class="widefat categories" style="width:100%;">
				<option value="one" <?php if ('one' == $instance['post_7_stars']) echo 'selected="selected"'; ?>>1 Estrella</option>
				<option value="two" <?php if ('two' == $instance['post_7_stars']) echo 'selected="selected"'; ?>>2 Estrellas</option>
				<option value="three" <?php if ('three' == $instance['post_7_stars']) echo 'selected="selected"'; ?>>3 Estrellas</option>
				<option value="four" <?php if ('four' == $instance['post_7_stars']) echo 'selected="selected"'; ?>>4 Estrellas</option>
				<option value="five" <?php if ('five' == $instance['post_7_stars']) echo 'selected="selected"'; ?>>5 Estrellas</option>
			</select>
		</div>

		<?php // 8 Post ?>

		<div class="featured_widget_box">
			<label for="<?php echo $this->get_field_id( 'post_8' ); ?>"><?php _e( 'Octavo Hotel:' ); ?></label>
			<select id="<?php echo $this->get_field_id( 'post_8' ); ?>" name="<?php echo $this->get_field_name( 'post_8' ); ?>" class="widefat categories" style="width:100%;">
			<option value=""></option>
			<?php foreach($hotels as $hotel) { ?>
			<option value='<?php echo $hotel->ID; ?>' <?php if ($hotel->ID == $instance['post_8']) echo 'selected="selected"'; ?>><?php echo _e($hotel->post_title); ?></option>
			<?php } ?>
			</select>
			</p>

			<p>
			<label for="<?php echo $this->get_field_id( 'post_8_url' ); ?>"><?php _e( 'Hotel URL:' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'post_8_url' ); ?>" name="<?php echo $this->get_field_name( 'post_8_url' ); ?>" type="text" value="<?php echo esc_attr( $post_8_url ); ?>" />
			</p>

			<p>
			<label for="<?php echo $this->get_field_id( 'post_8_location' ); ?>"><?php _e( 'Ubicación:' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'post_8_location' ); ?>" name="<?php echo $this->get_field_name( 'post_8_location' ); ?>" type="text" value="<?php echo esc_attr( $post_8_location ); ?>" />
			</p>

			<select id="<?php echo $this->get_field_id( 'post_8_stars' ); ?>" name="<?php echo $this->get_field_name( 'post_8_stars' ); ?>" class="widefat categories" style="width:100%;">
				<option value="one" <?php if ('one' == $instance['post_8_stars']) echo 'selected="selected"'; ?>>1 Estrella</option>
				<option value="two" <?php if ('two' == $instance['post_8_stars']) echo 'selected="selected"'; ?>>2 Estrellas</option>
				<option value="three" <?php if ('three' == $instance['post_8_stars']) echo 'selected="selected"'; ?>>3 Estrellas</option>
				<option value="four" <?php if ('four' == $instance['post_8_stars']) echo 'selected="selected"'; ?>>4 Estrellas</option>
				<option value="five" <?php if ('five' == $instance['post_8_stars']) echo 'selected="selected"'; ?>>5 Estrellas</option>
			</select>
		</div>

		<?php // 9 Post ?>

		<div class="featured_widget_box">
			<label for="<?php echo $this->get_field_id( 'post_9' ); ?>"><?php _e( 'Noveno Hotel:' ); ?></label>
			<select id="<?php echo $this->get_field_id( 'post_9' ); ?>" name="<?php echo $this->get_field_name( 'post_9' ); ?>" class="widefat categories" style="width:100%;">
			<option value=""></option>
			<?php foreach($hotels as $hotel) { ?>
			<option value='<?php echo $hotel->ID; ?>' <?php if ($hotel->ID == $instance['post_9']) echo 'selected="selected"'; ?>><?php echo _e($hotel->post_title); ?></option>
			<?php } ?>
			</select>
			</p>

			<p>
			<label for="<?php echo $this->get_field_id( 'post_9_url' ); ?>"><?php _e( 'Hotel URL:' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'post_9_url' ); ?>" name="<?php echo $this->get_field_name( 'post_9_url' ); ?>" type="text" value="<?php echo esc_attr( $post_9_url ); ?>" />
			</p>

			<p>
			<label for="<?php echo $this->get_field_id( 'post_9_location' ); ?>"><?php _e( 'Ubicación:' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'post_9_location' ); ?>" name="<?php echo $this->get_field_name( 'post_9_location' ); ?>" type="text" value="<?php echo esc_attr( $post_9_location ); ?>" />
			</p>

			<select id="<?php echo $this->get_field_id( 'post_9_stars' ); ?>" name="<?php echo $this->get_field_name( 'post_9_stars' ); ?>" class="widefat categories" style="width:100%;">
				<option value="one" <?php if ('one' == $instance['post_9_stars']) echo 'selected="selected"'; ?>>1 Estrella</option>
				<option value="two" <?php if ('two' == $instance['post_9_stars']) echo 'selected="selected"'; ?>>2 Estrellas</option>
				<option value="three" <?php if ('three' == $instance['post_9_stars']) echo 'selected="selected"'; ?>>3 Estrellas</option>
				<option value="four" <?php if ('four' == $instance['post_9_stars']) echo 'selected="selected"'; ?>>4 Estrellas</option>
				<option value="five" <?php if ('five' == $instance['post_9_stars']) echo 'selected="selected"'; ?>>5 Estrellas</option>
			</select>
		</div>

		<?php // 10 Post ?>

		<div class="featured_widget_box">
			<label for="<?php echo $this->get_field_id( 'post_10' ); ?>"><?php _e( 'Décimo Hotel:' ); ?></label>
			<select id="<?php echo $this->get_field_id( 'post_10' ); ?>" name="<?php echo $this->get_field_name( 'post_10' ); ?>" class="widefat categories" style="width:100%;">
			<option value=""></option>
			<?php foreach($hotels as $hotel) { ?>
			<option value='<?php echo $hotel->ID; ?>' <?php if ($hotel->ID == $instance['post_10']) echo 'selected="selected"'; ?>><?php echo _e($hotel->post_title); ?></option>
			<?php } ?>
			</select>
			</p>

			<p>
			<label for="<?php echo $this->get_field_id( 'post_10_url' ); ?>"><?php _e( 'Hotel URL:' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'post_10_url' ); ?>" name="<?php echo $this->get_field_name( 'post_10_url' ); ?>" type="text" value="<?php echo esc_attr( $post_10_url ); ?>" />
			</p>

			<p>
			<label for="<?php echo $this->get_field_id( 'post_10_location' ); ?>"><?php _e( 'Ubicación:' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'post_10_location' ); ?>" name="<?php echo $this->get_field_name( 'post_10_location' ); ?>" type="text" value="<?php echo esc_attr( $post_10_location ); ?>" />
			</p>

			<select id="<?php echo $this->get_field_id( 'post_10_stars' ); ?>" name="<?php echo $this->get_field_name( 'post_10_stars' ); ?>" class="widefat categories" style="width:100%;">
				<option value="one" <?php if ('one' == $instance['post_10_stars']) echo 'selected="selected"'; ?>>1 Estrella</option>
				<option value="two" <?php if ('two' == $instance['post_10_stars']) echo 'selected="selected"'; ?>>2 Estrellas</option>
				<option value="three" <?php if ('three' == $instance['post_10_stars']) echo 'selected="selected"'; ?>>3 Estrellas</option>
				<option value="four" <?php if ('four' == $instance['post_10_stars']) echo 'selected="selected"'; ?>>4 Estrellas</option>
				<option value="five" <?php if ('five' == $instance['post_10_stars']) echo 'selected="selected"'; ?>>5 Estrellas</option>
			</select>
		</div>

		<?php // 11 Post ?>

		<div class="featured_widget_box">
			<label for="<?php echo $this->get_field_id( 'post_11' ); ?>"><?php _e( 'Décimo Primer Hotel:' ); ?></label>
			<select id="<?php echo $this->get_field_id( 'post_11' ); ?>" name="<?php echo $this->get_field_name( 'post_11' ); ?>" class="widefat categories" style="width:100%;">
			<option value=""></option>
			<?php foreach($hotels as $hotel) { ?>
			<option value='<?php echo $hotel->ID; ?>' <?php if ($hotel->ID == $instance['post_11']) echo 'selected="selected"'; ?>><?php echo _e($hotel->post_title); ?></option>
			<?php } ?>
			</select>
			</p>

			<p>
			<label for="<?php echo $this->get_field_id( 'post_11_url' ); ?>"><?php _e( 'Hotel URL:' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'post_11_url' ); ?>" name="<?php echo $this->get_field_name( 'post_11_url' ); ?>" type="text" value="<?php echo esc_attr( $post_11_url ); ?>" />
			</p>

			<p>
			<label for="<?php echo $this->get_field_id( 'post_11_location' ); ?>"><?php _e( 'Ubicación:' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'post_11_location' ); ?>" name="<?php echo $this->get_field_name( 'post_11_location' ); ?>" type="text" value="<?php echo esc_attr( $post_11_location ); ?>" />
			</p>

			<select id="<?php echo $this->get_field_id( 'post_11_stars' ); ?>" name="<?php echo $this->get_field_name( 'post_11_stars' ); ?>" class="widefat categories" style="width:100%;">
				<option value="one" <?php if ('one' == $instance['post_11_stars']) echo 'selected="selected"'; ?>>1 Estrella</option>
				<option value="two" <?php if ('two' == $instance['post_11_stars']) echo 'selected="selected"'; ?>>2 Estrellas</option>
				<option value="three" <?php if ('three' == $instance['post_11_stars']) echo 'selected="selected"'; ?>>3 Estrellas</option>
				<option value="four" <?php if ('four' == $instance['post_11_stars']) echo 'selected="selected"'; ?>>4 Estrellas</option>
				<option value="five" <?php if ('five' == $instance['post_11_stars']) echo 'selected="selected"'; ?>>5 Estrellas</option>
			</select>
		</div>

		<?php // 12 Post ?>

		<div class="featured_widget_box">
			<label for="<?php echo $this->get_field_id( 'post_12' ); ?>"><?php _e( 'Décimo Segundo Hotel:' ); ?></label>
			<select id="<?php echo $this->get_field_id( 'post_12' ); ?>" name="<?php echo $this->get_field_name( 'post_12' ); ?>" class="widefat categories" style="width:100%;">
			<option value=""></option>
			<?php foreach($hotels as $hotel) { ?>
			<option value='<?php echo $hotel->ID; ?>' <?php if ($hotel->ID == $instance['post_12']) echo 'selected="selected"'; ?>><?php echo _e($hotel->post_title); ?></option>
			<?php } ?>
			</select>
			</p>

			<p>
			<label for="<?php echo $this->get_field_id( 'post_12_url' ); ?>"><?php _e( 'Hotel URL:' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'post_12_url' ); ?>" name="<?php echo $this->get_field_name( 'post_12_url' ); ?>" type="text" value="<?php echo esc_attr( $post_12_url ); ?>" />
			</p>

			<p>
			<label for="<?php echo $this->get_field_id( 'post_12_location' ); ?>"><?php _e( 'Ubicación:' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'post_12_location' ); ?>" name="<?php echo $this->get_field_name( 'post_12_location' ); ?>" type="text" value="<?php echo esc_attr( $post_12_location ); ?>" />
			</p>

			<select id="<?php echo $this->get_field_id( 'post_12_stars' ); ?>" name="<?php echo $this->get_field_name( 'post_12_stars' ); ?>" class="widefat categories" style="width:100%;">
				<option value="one" <?php if ('one' == $instance['post_12_stars']) echo 'selected="selected"'; ?>>1 Estrella</option>
				<option value="two" <?php if ('two' == $instance['post_12_stars']) echo 'selected="selected"'; ?>>2 Estrellas</option>
				<option value="three" <?php if ('three' == $instance['post_12_stars']) echo 'selected="selected"'; ?>>3 Estrellas</option>
				<option value="four" <?php if ('four' == $instance['post_12_stars']) echo 'selected="selected"'; ?>>4 Estrellas</option>
				<option value="five" <?php if ('five' == $instance['post_12_stars']) echo 'selected="selected"'; ?>>5 Estrellas</option>
			</select>
		</div>

		<?php // 13 Post ?>

		<div class="featured_widget_box">
			<label for="<?php echo $this->get_field_id( 'post_13' ); ?>"><?php _e( 'Décimo Tercer Hotel:' ); ?></label>
			<select id="<?php echo $this->get_field_id( 'post_13' ); ?>" name="<?php echo $this->get_field_name( 'post_13' ); ?>" class="widefat categories" style="width:100%;">
			<option value=""></option>
			<?php foreach($hotels as $hotel) { ?>
			<option value='<?php echo $hotel->ID; ?>' <?php if ($hotel->ID == $instance['post_13']) echo 'selected="selected"'; ?>><?php echo _e($hotel->post_title); ?></option>
			<?php } ?>
			</select>
			</p>

			<p>
			<label for="<?php echo $this->get_field_id( 'post_13_url' ); ?>"><?php _e( 'Hotel URL:' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'post_13_url' ); ?>" name="<?php echo $this->get_field_name( 'post_13_url' ); ?>" type="text" value="<?php echo esc_attr( $post_13_url ); ?>" />
			</p>

			<p>
			<label for="<?php echo $this->get_field_id( 'post_13_location' ); ?>"><?php _e( 'Ubicación:' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'post_13_location' ); ?>" name="<?php echo $this->get_field_name( 'post_13_location' ); ?>" type="text" value="<?php echo esc_attr( $post_13_location ); ?>" />
			</p>

			<select id="<?php echo $this->get_field_id( 'post_13_stars' ); ?>" name="<?php echo $this->get_field_name( 'post_13_stars' ); ?>" class="widefat categories" style="width:100%;">
				<option value="one" <?php if ('one' == $instance['post_13_stars']) echo 'selected="selected"'; ?>>1 Estrella</option>
				<option value="two" <?php if ('two' == $instance['post_13_stars']) echo 'selected="selected"'; ?>>2 Estrellas</option>
				<option value="three" <?php if ('three' == $instance['post_13_stars']) echo 'selected="selected"'; ?>>3 Estrellas</option>
				<option value="four" <?php if ('four' == $instance['post_13_stars']) echo 'selected="selected"'; ?>>4 Estrellas</option>
				<option value="five" <?php if ('five' == $instance['post_13_stars']) echo 'selected="selected"'; ?>>5 Estrellas</option>
			</select>
		</div>

		<?php // 14 Post ?>

		<div class="featured_widget_box">
			<label for="<?php echo $this->get_field_id( 'post_14' ); ?>"><?php _e( 'Décimo Cuarto Hotel:' ); ?></label>
			<select id="<?php echo $this->get_field_id( 'post_14' ); ?>" name="<?php echo $this->get_field_name( 'post_14' ); ?>" class="widefat categories" style="width:100%;">
			<option value=""></option>
			<?php foreach($hotels as $hotel) { ?>
			<option value='<?php echo $hotel->ID; ?>' <?php if ($hotel->ID == $instance['post_14']) echo 'selected="selected"'; ?>><?php echo _e($hotel->post_title); ?></option>
			<?php } ?>
			</select>
			</p>

			<p>
			<label for="<?php echo $this->get_field_id( 'post_14_url' ); ?>"><?php _e( 'Hotel URL:' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'post_14_url' ); ?>" name="<?php echo $this->get_field_name( 'post_14_url' ); ?>" type="text" value="<?php echo esc_attr( $post_14_url ); ?>" />
			</p>

			<p>
			<label for="<?php echo $this->get_field_id( 'post_14_location' ); ?>"><?php _e( 'Ubicación:' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'post_14_location' ); ?>" name="<?php echo $this->get_field_name( 'post_14_location' ); ?>" type="text" value="<?php echo esc_attr( $post_14_location ); ?>" />
			</p>

			<select id="<?php echo $this->get_field_id( 'post_14_stars' ); ?>" name="<?php echo $this->get_field_name( 'post_14_stars' ); ?>" class="widefat categories" style="width:100%;">
				<option value="one" <?php if ('one' == $instance['post_14_stars']) echo 'selected="selected"'; ?>>1 Estrella</option>
				<option value="two" <?php if ('two' == $instance['post_14_stars']) echo 'selected="selected"'; ?>>2 Estrellas</option>
				<option value="three" <?php if ('three' == $instance['post_14_stars']) echo 'selected="selected"'; ?>>3 Estrellas</option>
				<option value="four" <?php if ('four' == $instance['post_14_stars']) echo 'selected="selected"'; ?>>4 Estrellas</option>
				<option value="five" <?php if ('five' == $instance['post_14_stars']) echo 'selected="selected"'; ?>>5 Estrellas</option>
			</select>
		</div>
	<?php
	}

	// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['post_1'] = $new_instance['post_1'];
		$instance['post_1_url'] = $new_instance['post_1_url'];
		$instance['post_1_location'] = $new_instance['post_1_location'];
		$instance['post_1_stars'] = $new_instance['post_1_stars'];
		$instance['post_2'] = $new_instance['post_2'];
		$instance['post_2_url'] = $new_instance['post_2_url'];
		$instance['post_2_location'] = $new_instance['post_2_location'];
		$instance['post_2_stars'] = $new_instance['post_2_stars'];
		$instance['post_3'] = $new_instance['post_3'];
		$instance['post_3_url'] = $new_instance['post_3_url'];
		$instance['post_3_location'] = $new_instance['post_3_location'];
		$instance['post_3_stars'] = $new_instance['post_3_stars'];
		$instance['post_4'] = $new_instance['post_4'];
		$instance['post_4_url'] = $new_instance['post_4_url'];
		$instance['post_4_location'] = $new_instance['post_4_location'];
		$instance['post_4_stars'] = $new_instance['post_4_stars'];
		$instance['post_5'] = $new_instance['post_5'];
		$instance['post_5_url'] = $new_instance['post_5_url'];
		$instance['post_5_location'] = $new_instance['post_5_location'];
		$instance['post_5_stars'] = $new_instance['post_5_stars'];
		$instance['post_6'] = $new_instance['post_6'];
		$instance['post_6_url'] = $new_instance['post_6_url'];
		$instance['post_6_location'] = $new_instance['post_6_location'];
		$instance['post_6_stars'] = $new_instance['post_6_stars'];
		$instance['post_7'] = $new_instance['post_7'];
		$instance['post_7_url'] = $new_instance['post_7_url'];
		$instance['post_7_location'] = $new_instance['post_7_location'];
		$instance['post_7_stars'] = $new_instance['post_7_stars'];
		$instance['post_8'] = $new_instance['post_8'];
		$instance['post_8_url'] = $new_instance['post_8_url'];
		$instance['post_8_location'] = $new_instance['post_8_location'];
		$instance['post_8_stars'] = $new_instance['post_8_stars'];
		$instance['post_9'] = $new_instance['post_9'];
		$instance['post_9_url'] = $new_instance['post_9_url'];
		$instance['post_9_location'] = $new_instance['post_9_location'];
		$instance['post_9_stars'] = $new_instance['post_9_stars'];
		$instance['post_10'] = $new_instance['post_10'];
		$instance['post_10_url'] = $new_instance['post_10_url'];
		$instance['post_10_location'] = $new_instance['post_10_location'];
		$instance['post_10_stars'] = $new_instance['post_10_stars'];
		$instance['post_11'] = $new_instance['post_11'];
		$instance['post_11_url'] = $new_instance['post_11_url'];
		$instance['post_11_location'] = $new_instance['post_11_location'];
		$instance['post_11_stars'] = $new_instance['post_11_stars'];
		$instance['post_12'] = $new_instance['post_12'];
		$instance['post_12_url'] = $new_instance['post_12_url'];
		$instance['post_12_location'] = $new_instance['post_12_location'];
		$instance['post_12_stars'] = $new_instance['post_12_stars'];
		$instance['post_13'] = $new_instance['post_13'];
		$instance['post_13_url'] = $new_instance['post_13_url'];
		$instance['post_13_location'] = $new_instance['post_13_location'];
		$instance['post_13_stars'] = $new_instance['post_13_stars'];
		$instance['post_14'] = $new_instance['post_14'];
		$instance['post_14_url'] = $new_instance['post_14_url'];
		$instance['post_14_location'] = $new_instance['post_14_location'];
		$instance['post_14_stars'] = $new_instance['post_14_stars'];

		return $instance;
	}
}

// Register and load the widget
function featured_load_widget() {
	register_widget( 'featured_widget' );
}
add_action( 'widgets_init', 'featured_load_widget' );
?>
