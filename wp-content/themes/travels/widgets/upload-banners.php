<?php
// Creating the widget 
class upload_banner_widget extends WP_Widget {

	function __construct() {
		parent::__construct(
		// Base ID of your widget
		'upload_banner_widget',
		// Widget name will appear in UI
		__('Upload Banner on Sidebar', 'upload_banner_widget_domain'),
		// Widget description
		array( 'description' => __( 'Upload Banner on Sidebar', 'upload_banner_widget_domain' ) ) 
		);
	}

	// Creating widget front-end
	// This is where the action happens
	public function widget( $args, $instance ) {
		if (empty($instance['title_es']) || empty($instance['title_en']) || empty($instance['title_pt']))
			return;
		if (empty($instance['image_uri_es']) || empty($instance['image_uri_en']) || empty($instance['image_uri_pt']))
			return;
	?>
		<div class="tr-banner-widget">
			<div class="tr-banner-image">
				<?php $language = qtranxf_getLanguage(); ?>
				<a title="Ir a <?php echo $instance['title_' . $language]; ?>" href="<?php echo $instance['custom_link_' . $language]; ?>"><?php echo tr_get_image_id($instance['image_uri_' . $language]); ?></a>
				<a title="Ir a <?php echo $instance['title_' . $language]; ?>" href="<?php echo $instance['custom_link_' . $language]; ?>"><h5><?php echo $instance['title_' . $language]; ?></h5></a>
			</div>
		</div>

	<?php }

	// Widget Backend
	public function form( $instance ) {
		if ( isset( $instance[ 'title_es' ] ) ) {
			$title_es = $instance[ 'title_es' ];
		} else {
			$title_es = __( 'Título', 'upload_banner_widget_domain' );
		}
	
		if ( isset( $instance[ 'image_uri_es' ] ) ) {
			$img_url_es = $instance[ 'image_uri_es' ];
		} else {
			$img_url_es = __( 'Ruta de Imágen del Banner', 'upload_banner_widget_domain' );
		}

		if ( isset( $instance[ 'custom_link_es' ] ) ) {
			$custom_link_es = $instance[ 'custom_link_es' ];
		} else {
			$custom_link_es = __( 'Link Destino', 'upload_banner_widget_domain' );
		}

		if ( isset( $instance[ 'title_en' ] ) ) {
			$title_en = $instance[ 'title_en' ];
		} else {
			$title_en = __( 'Título', 'upload_banner_widget_domain' );
		}
	
		if ( isset( $instance[ 'image_uri_en' ] ) ) {
			$img_url_en = $instance[ 'image_uri_en' ];
		} else {
			$img_url_en = __( 'Ruta de Imágen del Banner', 'upload_banner_widget_domain' );
		}

		if ( isset( $instance[ 'custom_link_en' ] ) ) {
			$custom_link_en = $instance[ 'custom_link_en' ];
		} else {
			$custom_link_en = __( 'Link Destino', 'upload_banner_widget_domain' );
		}

		if ( isset( $instance[ 'title_pt' ] ) ) {
			$title_pt = $instance[ 'title_pt' ];
		} else {
			$title_pt = __( 'Título', 'upload_banner_widget_domain' );
		}
	
		if ( isset( $instance[ 'image_uri_pt' ] ) ) {
			$img_url_pt = $instance[ 'image_uri_pt' ];
		} else {
			$img_url_pt = __( 'Ruta de Imágen del Banner', 'upload_banner_widget_domain' );
		}

		if ( isset( $instance[ 'custom_link_pt' ] ) ) {
			$custom_link_pt = $instance[ 'custom_link_pt' ];
		} else {
			$custom_link_pt = __( 'Link Destino', 'upload_banner_widget_domain' );
		}

		// removed the for loop, you can create new instances of the widget instead.
		?>
		<div class="tr-banner-widget-admin-warning">
			<p>Seleccionar siempre la opción "Tamaño Completo" al insertar la imágen.</p>
		</div>
		<div class="tr-banner-widget-admin">
			<h3>Español</h3>
			<p>
				<label for="<?php echo $this->get_field_id('title_es'); ?>">Titulo</label><br />
				<input type="text" name="<?php echo $this->get_field_name('title_es'); ?>" id="<?php echo $this->get_field_id('title_es'); ?>" value="<?php echo esc_attr( $title_es ); ?>" class="widefat" />
			</p>
			<p>
				<label for="<?php echo $this->get_field_id('image_uri_es'); ?>">Imágen</label><br />
				<input type="text" class="img" name="<?php echo $this->get_field_name('image_uri_es'); ?>" id="<?php echo $this->get_field_id('image_uri_es'); ?>" value="<?php echo $img_url_es; ?>" />
				<input type="button" class="select-img" value="Seleccionar Imágen" />
			</p>
			<p>
				<label for="<?php echo $this->get_field_id('custom_link_es'); ?>">URL Destino</label><br />
				<input type="text" class="cs-link" name="<?php echo $this->get_field_name('custom_link_es'); ?>" id="<?php echo $this->get_field_id('custom_link_es'); ?>" value="<?php echo $custom_link_es; ?>" />
			</p>
		</div>

		<div class="tr-banner-widget-admin">
			<h3>Ingles</h3>
			<p>
				<label for="<?php echo $this->get_field_id('title_en'); ?>">Titulo</label><br />
				<input type="text" name="<?php echo $this->get_field_name('title_en'); ?>" id="<?php echo $this->get_field_id('title_en'); ?>" value="<?php echo esc_attr( $title_en ); ?>" class="widefat" />
			</p>
			<p>
				<label for="<?php echo $this->get_field_id('image_uri_en'); ?>">Imágen</label><br />
				<input type="text" class="img" name="<?php echo $this->get_field_name('image_uri_en'); ?>" id="<?php echo $this->get_field_id('image_uri_en'); ?>" value="<?php echo $img_url_en; ?>" />
				<input type="button" class="select-img" value="Seleccionar Imágen" />
			</p>
			<p>
				<label for="<?php echo $this->get_field_id('custom_link_en'); ?>">URL Destino</label><br />
				<input type="text" class="cs-link" name="<?php echo $this->get_field_name('custom_link_en'); ?>" id="<?php echo $this->get_field_id('custom_link_en'); ?>" value="<?php echo $custom_link_en; ?>" />
			</p>
		</div>

		<div class="tr-banner-widget-admin">
			<h3>Portugues</h3>
			<p>
				<label for="<?php echo $this->get_field_id('title_pt'); ?>">Titulo</label><br />
				<input type="text" name="<?php echo $this->get_field_name('title_pt'); ?>" id="<?php echo $this->get_field_id('title_pt'); ?>" value="<?php echo esc_attr( $title_pt ); ?>" class="widefat" />
			</p>
			<p>
				<label for="<?php echo $this->get_field_id('image_uri_pt'); ?>">Imágen</label><br />
				<input type="text" class="img" name="<?php echo $this->get_field_name('image_uri_pt'); ?>" id="<?php echo $this->get_field_id('image_uri_pt'); ?>" value="<?php echo $img_url_pt; ?>" />
				<input type="button" class="select-img" value="Seleccionar Imágen" />
			</p>
			<p>
				<label for="<?php echo $this->get_field_id('custom_link_pt'); ?>">URL Destino</label><br />
				<input type="text" class="cs-link" name="<?php echo $this->get_field_name('custom_link_pt'); ?>" id="<?php echo $this->get_field_id('custom_link_pt'); ?>" value="<?php echo $custom_link_pt; ?>" />
			</p>
		</div>
		<?php
	}

	// Updating widget replacing old instances with new.
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title_es'] = ( ! empty( $new_instance['title_es'] ) ) ? strip_tags( $new_instance['title_es'] ) : '';
		$instance['image_uri_es'] = $new_instance['image_uri_es'];
		$instance['custom_link_es'] = $new_instance['custom_link_es'];

		$instance['title_en'] = ( ! empty( $new_instance['title_en'] ) ) ? strip_tags( $new_instance['title_en'] ) : '';
		$instance['image_uri_en'] = $new_instance['image_uri_en'];
		$instance['custom_link_en'] = $new_instance['custom_link_en'];

		$instance['title_pt'] = ( ! empty( $new_instance['title_pt'] ) ) ? strip_tags( $new_instance['title_pt'] ) : '';
		$instance['image_uri_pt'] = $new_instance['image_uri_pt'];
		$instance['custom_link_pt'] = $new_instance['custom_link_pt'];

		return $instance;
	}
}

// Register and load the widget.
function upload_banner_load_widget() {
	register_widget( 'upload_banner_widget' );
}

add_action( 'widgets_init', 'upload_banner_load_widget' );

//Queue up the necessary js
function tr_upload_image_enqueue() {
	wp_enqueue_style('thickbox');
	wp_enqueue_script('media-upload');
	wp_enqueue_script('thickbox');
	//Moved the js to an external file, you may want to change the path
	wp_enqueue_script('tr_upload_image_js', get_stylesheet_directory_uri() . '/js/vendor/travel-upload-media.js', null, null, true);
}

function tr_get_image_id( $image_url = '') {

	$attachment = getBannerAttachment($image_url);
	if (empty($attachment)) {
		return getDefualtImg();
	}

	$src = wp_get_attachment_image_src($attachment[0], 'full');
	$full_path = $src[0];	

	if (is_gif($full_path)) {
		$img_uri = wp_get_attachment_image_src($attachment[0], 'full');
		$image_thumb = wp_get_attachment_image($attachment[0], 'full', false, array('src' => set_schema($img_uri[0]), 'class' => 'img-responsive hidden-sm'));
		return $image_thumb;
	}
	$img_uri = wp_get_attachment_image_src($attachment[0], 'travel-banner-widget-thumbnail');
	$image_thumb = wp_get_attachment_image($attachment[0], 'travel-banner-widget-thumbnail', false, array('src' => set_schema($img_uri[0]), 'class' => 'img-responsive hidden-sm'));
	return $image_thumb;
}

function getBannerAttachment($image_url = '') {

	global $wpdb;
	$prefix = $wpdb->prefix;
	$attachment = $wpdb->get_col($wpdb->prepare( "SELECT ID FROM " . $prefix . "posts" . " WHERE guid='%s';", $image_url ));
	if (empty($attachment)) {
		return false;
	}
	return $attachment;
}

function getDefualtImg() {
	$path = '';
	$path = buildDefaultImgSrc();
	$img = <<<HTML
		<img src='{$path}' class='img-responsive hidden-sm' />
HTML;
	return $img;
}


function buildDefaultImgSrc() {
	$file = $GLOBALS['sites'][SC]['no_img_thumb_src'];
	$sc = SC;
	if (empty($file) || empty($sc))
		return false;

	$path = '';
	$path = get_bloginfo('template_url') . '/img/' . $sc . '/' . $file;
	return $path;
}

function is_gif($src = null) {
	if (empty($src))
		return;
	$src = pathinfo($src);

	if ($src['extension'] == 'gif') {
		return true;
	}
	return false;
}

function set_schema($full_path = null) {
	if (empty($full_path))
		return;

	if (is_ssl()) {
		$full_path = set_url_scheme($full_path, 'https');
		return $full_path;
	}
	return $full_path;
}

add_action('admin_enqueue_scripts', 'tr_upload_image_enqueue');
?>
