<?php
// Creating the widget 
class recommended_widget extends WP_Widget {

	function __construct() {
		parent::__construct(
		// Base ID of your widget
		'recommended_widget',
		// Widget name will appear in UI
		__('Show Recommended Posts', 'recommended_widget_domain'),
		// Widget description
		array( 'description' => __( 'Show Posts on Home Page', 'recommended_widget_domain' ), ) 
		);
	}

	// Creating widget front-end
	// This is where the action happens
	public function widget( $args, $instance ) {
		$title = apply_filters( 'widget_title', $instance['title'] );
		// before and after widget arguments are defined by themes
		echo $args['before_widget'];
		if ( ! empty( $title ) )
			//echo $args['before_title'] . $title . $args['after_title'];

		$posts = array(
			$instance['post_1'],
			$instance['post_2'],
			$instance['post_3']
		);

		$arg = array( 'post__in' => $posts, 'orderby' => 'post__in', 'order' => 'ASC' );
		$the_query = new WP_Query( $arg );
		$url = '';
		$stars = '';
		$i = 1;

		while ( $the_query->have_posts() ) {
			$the_query->the_post();
			$url = $instance['post_' .$i. '_url'];
			$location = $instance['post_' .$i. '_location'];
			$stars = $instance['post_' .$i. '_stars']; 
			?>

			<div class="col-sm-4 col-md-4 col-lg-4 tr-recomended-widget-box">
				<a title="Reservar en <?php the_title(); ?>" href="<?php if (!empty($url)) { echo $url; } else { the_permalink(); } ?>"><?php echo the_post_thumbnail('travel-widget-thumbnail', array('class' => 'tr-home-recomended img-responsive')); ?></a>
				<div class="tr-recomended-header">
					<a href="<?php if (!empty($url)) { echo $url; } else { the_permalink(); } ?>"><h3><?php the_title(); ?></h3></a>
					<p><?php if (!empty($location)) { echo $location; } else { echo 'Capital'; } ?></p>
					<span class="mt-sprite stars <?=$stars;?>-stars"></span>
					<div class="tr-recomended-border"></div>
				</div>
				<p class="tr-recomended-text"><?php echo the_excerpt_max_charlength(150); ?></p>
				<a title="Reservar en <?php the_title(); ?>" class="tr-recomended-button-link" href="<?php if (!empty($url)) { echo $url; } else { the_permalink(); } ?>"><div class="tr-recomended-button"><span class="tr-recomended-link"><?php _e( 'See Prices', 'travels' ) ?></span><span class="tr-recomended-arrow"><img width="16px" src="<?php echo get_template_directory_uri(); ?>/img/arrow-right.png"></span></div></a>
			</div>
		<?php $i++;?>
		<?php } ?>
		<?php unset($i); ?>
	<?php echo $args['after_widget'];
	}

	// Widget Backend
	public function form( $instance ) {

		if ( isset( $instance[ 'title' ] ) ) {
			$title = $instance[ 'title' ];
		} else {
			$title = __( 'New title', 'recommended_widget_domain' );
		}

		if ( isset( $instance[ 'post_1' ] ) ) {
			$post_1 = $instance[ 'post_1' ];
		} else {
			$post_1 = __( '', 'recommended_widget_domain' );
		}

		if ( isset( $instance[ 'post_2' ] ) ) {
			$post_2 = $instance[ 'post_2' ];
		} else {
			$post_2 = __( '', 'recommended_widget_domain' );
		}

		if ( isset( $instance[ 'post_3' ] ) ) {
			$post_3 = $instance[ 'post_3' ];
		} else {
			$post_3 = __( '', 'recommended_widget_domain' );
		}

		if ( isset( $instance[ 'post_1_url' ] ) ) {
			$post_1_url = $instance[ 'post_1_url' ];
		} else {
			$post_1_url = __( '', 'recommended_widget_domain' );
		}

		if ( isset( $instance[ 'post_2_url' ] ) ) {
			$post_2_url = $instance[ 'post_2_url' ];
		} else {
			$post_2_url = __( '', 'recommended_widget_domain' );
		}

		if ( isset( $instance[ 'post_3_url' ] ) ) {
			$post_3_url = $instance[ 'post_3_url' ];
		} else {
			$post_3_url = __( '', 'recommended_widget_domain' );
		}

		if ( isset( $instance[ 'post_1_location' ] ) ) {
			$post_1_location = $instance[ 'post_1_location' ];
		} else {
			$post_1_location = __( '', 'recommended_widget_domain' );
		}

		if ( isset( $instance[ 'post_2_location' ] ) ) {
			$post_2_location = $instance[ 'post_2_location' ];
		} else {
			$post_2_location = __( '', 'recommended_widget_domain' );
		}

		if ( isset( $instance[ 'post_3_location' ] ) ) {
			$post_3_location = $instance[ 'post_3_location' ];
		} else {
			$post_3_location = __( '', 'recommended_widget_domain' );
		}

		if ( isset( $instance[ 'post_1_stars' ] ) ) {
			$post_1_stars = $instance[ 'post_1_stars' ];
		} else {
			$post_1_stars = __( '', 'recommended_widget_domain' );
		}

		if ( isset( $instance[ 'post_2_stars' ] ) ) {
			$post_2_stars = $instance[ 'post_2_stars' ];
		} else {
			$post_2_stars = __( '', 'recommended_widget_domain' );
		}

		if ( isset( $instance[ 'post_3_stars' ] ) ) {
			$post_3_stars = $instance[ 'post_3_stars' ];
		} else {
			$post_3_stars = __( '', 'recommended_widget_domain' );
		}

		?>

		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Título:' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>

		<?php //First Post ?>

		<div class="featured_widget_box">
			<p>
				<label for="<?php echo $this->get_field_id( 'post_1' ); ?>"><?php _e( 'Primer Hotel:' ); ?></label>
				<?php
				$recommended_category_post = $GLOBALS['sites'][SC]['widgets']['recommended_post_category'];
				$args = array( 'posts_per_page' => 999, 'category' => $recommended_category_post, 'post_type' => 'post' );
				$hotels = get_posts( $args ); ?>

				<select id="<?php echo $this->get_field_id( 'post_1' ); ?>" name="<?php echo $this->get_field_name( 'post_1' ); ?>" class="widefat categories" style="width:100%;">
					<?php foreach($hotels as $hotel) { ?>
						<option value='<?php echo $hotel->ID; ?>' <?php if ($hotel->ID == $instance['post_1']) echo 'selected="selected"'; ?>><?php echo _e($hotel->post_title); ?></option>
					<?php } ?>
				</select>
			</p>
			<p>
				<label for="<?php echo $this->get_field_id( 'post_1_url' ); ?>"><?php _e( 'Hotel URL:' ); ?></label>
				<input class="widefat" id="<?php echo $this->get_field_id( 'post_1_url' ); ?>" name="<?php echo $this->get_field_name( 'post_1_url' ); ?>" type="text" value="<?php echo esc_attr( $post_1_url ); ?>" />
			</p>
			<p>
				<label for="<?php echo $this->get_field_id( 'post_1_location' ); ?>"><?php _e( 'Ubicación:' ); ?></label>
				<input class="widefat" id="<?php echo $this->get_field_id( 'post_1_location' ); ?>" name="<?php echo $this->get_field_name( 'post_1_location' ); ?>" type="text" value="<?php echo esc_attr( $post_1_location ); ?>" />
			</p>
			<select id="<?php echo $this->get_field_id( 'post_1_stars' ); ?>" name="<?php echo $this->get_field_name( 'post_1_stars' ); ?>" class="widefat categories" style="width:100%;">
				<option value="one" <?php if ('one' == $instance['post_1_stars']) echo 'selected="selected"'; ?>>1 Estrella</option>
				<option value="two" <?php if ('two' == $instance['post_1_stars']) echo 'selected="selected"'; ?>>2 Estrellas</option>
				<option value="three" <?php if ('three' == $instance['post_1_stars']) echo 'selected="selected"'; ?>>3 Estrellas</option>
				<option value="four" <?php if ('four' == $instance['post_1_stars']) echo 'selected="selected"'; ?>>4 Estrellas</option>
				<option value="five" <?php if ('five' == $instance['post_1_stars']) echo 'selected="selected"'; ?>>5 Estrellas</option>
			</select>
		</div>

		<?php //Second Post ?>

		<div class="featured_widget_box">
			<label for="<?php echo $this->get_field_id( 'post_2' ); ?>"><?php _e( 'Segundo Hotel:' ); ?></label>
			<select id="<?php echo $this->get_field_id( 'post_2' ); ?>" name="<?php echo $this->get_field_name( 'post_2' ); ?>" class="widefat categories" style="width:100%;">
			<?php foreach($hotels as $hotel) { ?>
			<option value='<?php echo $hotel->ID; ?>' <?php if ($hotel->ID == $instance['post_2']) echo 'selected="selected"'; ?>><?php echo _e($hotel->post_title); ?></option>
			<?php } ?>
			</select>
			</p>

			<p>
			<label for="<?php echo $this->get_field_id( 'post_2_url' ); ?>"><?php _e( 'Hotel URL:' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'post_2_url' ); ?>" name="<?php echo $this->get_field_name( 'post_2_url' ); ?>" type="text" value="<?php echo esc_attr( $post_2_url ); ?>" />
			</p>

			<p>
			<label for="<?php echo $this->get_field_id( 'post_2_location' ); ?>"><?php _e( 'Ubicación:' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'post_2_location' ); ?>" name="<?php echo $this->get_field_name( 'post_2_location' ); ?>" type="text" value="<?php echo esc_attr( $post_2_location ); ?>" />
			</p>
			<select id="<?php echo $this->get_field_id( 'post_2_stars' ); ?>" name="<?php echo $this->get_field_name( 'post_2_stars' ); ?>" class="widefat categories" style="width:100%;">
				<option value="one" <?php if ('one' == $instance['post_2_stars']) echo 'selected="selected"'; ?>>1 Estrella</option>
				<option value="two" <?php if ('two' == $instance['post_2_stars']) echo 'selected="selected"'; ?>>2 Estrellas</option>
				<option value="three" <?php if ('three' == $instance['post_2_stars']) echo 'selected="selected"'; ?>>3 Estrellas</option>
				<option value="four" <?php if ('four' == $instance['post_2_stars']) echo 'selected="selected"'; ?>>4 Estrellas</option>
				<option value="five" <?php if ('five' == $instance['post_2_stars']) echo 'selected="selected"'; ?>>5 Estrellas</option>
			</select>
		</div>

		<?php //Three Post ?>

		<div class="featured_widget_box">
			<label for="<?php echo $this->get_field_id( 'post_3' ); ?>"><?php _e( 'Tercer Hotel:' ); ?></label>
			<select id="<?php echo $this->get_field_id( 'post_3' ); ?>" name="<?php echo $this->get_field_name( 'post_3' ); ?>" class="widefat categories" style="width:100%;">
			<?php foreach($hotels as $hotel) { ?>
			<option value='<?php echo $hotel->ID; ?>'<?php if ($hotel->ID == $instance['post_3']) echo 'selected="selected"'; ?>><?php echo _e($hotel->post_title); ?></option>
			<?php } ?>
			</select>
			</p>

			<p>
			<label for="<?php echo $this->get_field_id( 'post_3_url' ); ?>"><?php _e( 'Hotel URL:' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'post_3_url' ); ?>" name="<?php echo $this->get_field_name( 'post_3_url' ); ?>" type="text" value="<?php echo esc_attr( $post_3_url ); ?>" />
			</p>

			<p>
			<label for="<?php echo $this->get_field_id( 'post_3_location' ); ?>"><?php _e( 'Ubicación:' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'post_3_location' ); ?>" name="<?php echo $this->get_field_name( 'post_3_location' ); ?>" type="text" value="<?php echo esc_attr( $post_3_location ); ?>" />
			</p>
			<select id="<?php echo $this->get_field_id( 'post_3_stars' ); ?>" name="<?php echo $this->get_field_name( 'post_3_stars' ); ?>" class="widefat categories" style="width:100%;">
				<option value="one" <?php if ('one' == $instance['post_3_stars']) echo 'selected="selected"'; ?>>1 Estrella</option>
				<option value="two" <?php if ('two' == $instance['post_3_stars']) echo 'selected="selected"'; ?>>2 Estrellas</option>
				<option value="three" <?php if ('three' == $instance['post_3_stars']) echo 'selected="selected"'; ?>>3 Estrellas</option>
				<option value="four" <?php if ('four' == $instance['post_3_stars']) echo 'selected="selected"'; ?>>4 Estrellas</option>
				<option value="five" <?php if ('five' == $instance['post_3_stars']) echo 'selected="selected"'; ?>>5 Estrellas</option>
			</select>
		</div>
	<?php
	}

	// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['post_1'] = $new_instance['post_1'];
		$instance['post_1_url'] = $new_instance['post_1_url'];
		$instance['post_1_location'] = $new_instance['post_1_location'];
		$instance['post_1_stars'] = $new_instance['post_1_stars'];
		$instance['post_2'] = $new_instance['post_2'];
		$instance['post_2_url'] = $new_instance['post_2_url'];
		$instance['post_2_location'] = $new_instance['post_2_location'];
		$instance['post_2_stars'] = $new_instance['post_2_stars'];
		$instance['post_3'] = $new_instance['post_3'];
		$instance['post_3_url'] = $new_instance['post_3_url'];
		$instance['post_3_location'] = $new_instance['post_3_location'];
		$instance['post_3_stars'] = $new_instance['post_3_stars'];
		return $instance;
	}
}

// Register and load the widget
function recommended_load_widget() {
	register_widget( 'recommended_widget' );
}
add_action( 'widgets_init', 'recommended_load_widget' );
?>
