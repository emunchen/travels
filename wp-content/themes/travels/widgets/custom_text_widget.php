<?php

class custom_text_widget extends WP_Widget {

	function __construct() {
		parent::__construct(
		// Base ID of your widget
		'custom_text_widget',
		// Widget name will appear in UI
		__('Show HTML on specific Posts', 'travel'),
		// Widget description
		array( 'description' => __( 'Print custom HTML on specific Posts.', 'travel' ) )
		);
	}

	// Creating widget front-end
	// This is where the action happens
	public function widget( $args, $instance ) { ?>

		<?php
			if (empty($instance['title']) || empty($instance['title']) || empty($instance['title']))
			return;
		?>
		<?php if (is_single(array(871, 10814, 879, 882, 891, 10781))): ?>
			<div class="tr-banner-widget">
				<h4 style="text-align: center;"><?php echo $instance['title']; ?></h4>
				<div style="background-color: #FFF;">
					<?php echo $instance['content']; ?>
				</div>
			</div>
		<?php endif; ?>
<?php	}

	// Widget Backend
	public function form( $instance ) {
		if ( isset( $instance[ 'title' ] ) ) {
			$title = $instance[ 'title' ];
		} else {
			$title = __( 'New title', 'travel' );
		}
		if ( isset( $instance[ 'content' ] ) ) {
			$content = $instance[ 'content' ];
		} else {
			$content = __( 'Texto o Contenido HTML', 'travel' );
		}
		?>

		<p>
		<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Título:' ); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>

		<p>
		<label for="<?php echo $this->get_field_id( 'content' ); ?>"><?php _e( 'Texto:' ); ?></label>
		<textarea rows="6" class="widefat" id="<?php echo $this->get_field_id( 'content' ); ?>" name="<?php echo $this->get_field_name( 'content' ); ?>">
			<?php echo $instance['content']; ?>
		</textarea>
		</p>
	<?php
	}

	// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['content'] = $new_instance['content'];
		return $instance;
	}
}

// Register and load the widget
function custom_text_widget() {
	register_widget( 'custom_text_widget' );
}
add_action( 'widgets_init', 'custom_text_widget' );
?>
