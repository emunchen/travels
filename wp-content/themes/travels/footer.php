<footer>
	<?php if ($GLOBALS['sites'][SC]['widgets']['main_footer']): ?>
		<div class="tr-footer-top container">
			<div class="footer-widget">
				<?php $titles = $GLOBALS['sites'][SC]['widgets']['top_footer']['titles']; ?>
				<h4><?php _e( $titles['menu-footer-one'], 'travels' ) ?></h4>
				<?php wp_nav_menu( array(
					'theme_location'	=> 'menu-footer-one-' . qtranxf_getLanguage(),
					'sort_column'		=> 'menu_order',
					'container_class'	=> 'menu-footer',
					'container'			=> 'div'
				)); ?>
			</div>
			<div class="footer-widget">
				<h4><?php _e( $titles['menu-footer-two'], 'travels' ) ?></h4>
				<?php wp_nav_menu( array(
					'theme_location'	=> 'menu-footer-two-' . qtranxf_getLanguage(),
					'sort_column'		=> 'menu_order',
					'container_class'	=> 'menu-footer',
					'container'			=> 'div'
				)); ?>
			</div>
			<div class="footer-widget">
				<h4><?php _e( $titles['menu-footer-three'], 'travels' ) ?></h4>
				<?php wp_nav_menu( array(
					'theme_location'	=> 'menu-footer-three-' . qtranxf_getLanguage(),
					'sort_column'		=> 'menu_order',
					'container_class'	=> 'menu-footer',
					'container'			=> 'div'
				)); ?>
			</div>
			<div class="footer-widget">
				<h4><?php _e( $titles['menu-footer-four'], 'travels' ) ?></h4>
				<?php wp_nav_menu( array(
					'theme_location'	=> 'menu-footer-four-' . qtranxf_getLanguage(),
					'sort_column'		=> 'menu_order',
					'container_class'	=> 'menu-footer',
					'container'			=> 'div'
				)); ?>
			</div>
			<div class="footer-widget">
				<h4><?php _e( $titles['menu-footer-five'], 'travels' ) ?></h4>
				<?php wp_nav_menu( array(
					'theme_location'	=> 'menu-footer-five-' . qtranxf_getLanguage(),
					'sort_column'		=> 'menu_order',
					'container_class'	=> 'menu-footer',
					'container'			=> 'div'
				)); ?>
			</div>
		</div>
	<?php endif; ?>
	<?php if ($GLOBALS['sites'][SC]['widgets']['middle_footer']): ?>
	<div class="tr-footer-middle container">
		<?php tr_footer_init(); ?>
	</div>
	<?php endif; ?>
	<?php if ($GLOBALS['sites'][SC]['widgets']['bottom_footer']): ?>
	<div class="tr-footer-bottom">
		<div class="container">
			<div class="col-sm-3 col-md-3 col-lg-3">
				<p><a href="/<?=qtranxf_getLanguage(); ?>/contacto/"><?php _e( 'Contact Us', 'travels' ) ?></a></p>	
			</div>
			<div class="col-sm-3 col-md-6 col-lg-6">
				<p><a href="/<?=qtranxf_getLanguage(); ?>/declaracion-de-privacidad/"><?php _e( 'Legal Advice', 'travels' ) ?></a></p>
			</div>
			<div class="col-sm-3 col-md-3 col-lg-3">
				<p><a href="/<?=qtranxf_getLanguage(); ?>/terminos-de-uso/"><?php _e( 'Terms and Conditions', 'travels' ) ?></a></p>
			</div>
		</div>
	<div>
	<?php endif; ?>
</footer>
<div id="loading-modal" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="false">
  <div class="modal-dialog modal-xs">
    <div class="modal-content">
		<div class="modal-body">
			<h3 class="modal-first-title"><?php _e( 'Looking for the best prices ...', 'travels' ) ?></h3>
			<h3 class="modal-second-title" style="display:none;"><?php _e( 'Obtaining Deals ...', 'travels' ) ?></h3>
			<div class="progress progress-striped active">
				<div class="progress-bar bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
			</div>
		</div>
    </div>
  </div>
</div>
<div id="error-modal" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3 class="modal-title"><?php _e( 'Oops! Something went wrong. You may be able to try again.', 'travels' ) ?></h3>
      </div>
      <div class="modal-body">
        <p><?php _e( 'Please Contact us via Email - contacto@mendoza.travel', 'travels' ) ?></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php _e( 'Close', 'travels' ) ?></button>
      </div>
    </div>
  </div>
</div>
<?=wp_footer() ?>
<?=get_analytics() ?>
<?=get_fb_remarketing() ?>
<?=socials_scripts() ?>
<link rel="stylesheet" href="<?php echo get_bloginfo('template_url'); ?>/css/swipebox.min.css">
<link rel="stylesheet" href="<?php echo get_bloginfo('template_url'); ?>/css/validationEngine.jquery.css">
</body>
</html>
