<?php get_header(); ?>
<div class="tr-custom-category tr-category-slider-bus-tour">
	<div class="container">
		<div class="<?=defineCols(); ?>">
			<div id="outter-div">
				<div id="aligned">
					<h1><?php single_cat_title( '', true ); ?></h1>
				</div>
			</div>
		</div>
		<?php get_contact_form(); ?>
	</div>
</div>
<div class="tr-custom-category container">
	<div class="tr-category-main col-xs-12 col-sm-9 col-md-9 col-lg-9">
		<?php if ( have_posts() ) : ?>
			<?php while ( have_posts() ) : the_post(); ?>
				<div class="tr-custom-category-item col-xs-12 col-sm-6 col-md-4 col-lg-4">
					<a title="<?php _e( 'More Info', 'travels' ) ?> <?php the_title(); ?>" href="<?php the_permalink(); ?>"><?php echo the_post_thumbnail('travel-widget-thumbnail', array('src' => setImageScheme('travel-widget-thumbnail'),'class' => 'tr-home-recomended img-responsive')); ?></a>
					<div class="tr-recomended-header">
						<a href="<?php the_permalink(); ?>"><h3><?php the_title(); ?></h3></a>
					</div>
					<p class="tr-recomended-text"><?php echo the_excerpt_max_charlength(150); ?></p>
					<a title="<?php _e( 'More Info', 'travels' ) ?> <?php the_title(); ?>" class="tr-recomended-button-link" href="<?php the_permalink(); ?>"><div class="tr-recomended-button"><span class="tr-recomended-link"><?php _e( 'More Info', 'travels' ) ?></span><span class="tr-recomended-arrow"><img width="16px" src="<?php echo get_template_directory_uri(); ?>/img/arrow-right.png"></span></div></a>
				</div>
			<?php endwhile; ?>
			<?php tr_pagination(); ?>
		<?php endif; ?>
	</div>
	<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
		<?php get_sidebar( 'category' ); ?>
	</div>
</div>
<div id="custom-contact-modal" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="false">
  <div class="modal-dialog modal-xs">
    <div class="modal-content">
		<div class="modal-body">
			<button type="button" class="close modal-email-close" data-dismiss="modal" aria-hidden="true" style="display:none;">&times;</button>
			<h3 class="custom-contact-modal-first-title"><?php _e( 'Sending Inquiry ...', 'travels' ) ?></h3>
			<h3 class="custom-contact-modal-second-title" style="display:none;"><?php _e( 'Thank You! Please check your email in few minutes.', 'travels' ) ?></h3>
			<h5 style="margin-top: 20px;" class="custom-contact-modal-second-title">
				Puedes recibir más ofertas en Facebook:
			</h5>
			<div style='height: 32px;width: 148px;display: inline-block;overflow: hidden;color: #fff;'>
				<div style='background-color: #fff;pointer-events:none;position:absolute;width:148px;z-index:2;line-height:36px;'>
					<a class="btn btn-primary" href="#">
					<i class="fa fa-facebook-official fa-lg"></i> Recibir Ofertas</a>
				</div>
				<div style="margin-top: 7px;" class="fb-send-to-messenger"
					messenger_app_id="<?=$GLOBALS['sites'][SC]['facebook']['appID']?>"
					page_id="<?=$GLOBALS['sites'][SC]['facebook']['pageID']?>"
					data-ref="PASS_THROUGH_PARAM"
					color="blue"
					size="xlarge">
				</div>
			</div>
			<div class="progress progress-striped active">
				<div class="progress-bar bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
			</div>
		</div>
		<div class="modal-footer modal-email" style="display:none;">
        	<button type="button" class="btn btn-default" data-dismiss="modal"><?php _e( 'Close', 'travels' ) ?></button>
      </div>
    </div>
  </div>
</div>
<div id="custom-contact-error-modal" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3 class="modal-title"><?php _e( 'Oops! Something went wrong. You may be able to try again.', 'travels' ) ?></h3>
      </div>
      <div class="modal-body">
        <p><?php _e( 'Please Contact us via Email - contacto@mendoza.travel', 'travels' ) ?></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php _e( 'Close', 'travels' ) ?></button>
      </div>
    </div>
  </div>
</div>
<?php get_footer(); ?>