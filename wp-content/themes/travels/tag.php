<?php get_header(); ?>

	<div class="container tr-category-container">

			<?php if ( function_exists('yoast_breadcrumb') ): ?>

				<?=google_ads_unit(); ?>

				<?php yoast_breadcrumb('<p id="breadcrumbs">','</p>'); ?>

			<?php endif; ?>

			<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">

				<?php get_sidebar( 'category' ); ?>

			</div>

			<div class="tr-category-main col-xs-12 col-sm-8 col-md-8 col-lg-8">

				<h1><?php single_cat_title( $prefix = '', $display = true ); ?></h1>

				<div class="col-xs-12 col-md-12">

					<?=google_ads('atf-category-right-content', 'ad-margin-bottom ad-margin-top', 'auto'); ?>

				</div>

				<?php if ( have_posts() ) : ?>

					<?php while ( have_posts() ) : the_post(); ?>

						<div class="tr-post-cat-box col-xs-12 col-md-12">

							<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

								<div class="tr-category-img col-xs-12 col-sm-5 col-md-5 col-lg-5">

									<?php if ( has_post_thumbnail() ) { ?>

										<a href="<?=custom_category_link(); ?>" title="<?php the_title_attribute(); ?>" >
											<?php the_post_thumbnail('travel-category-thumbnail', array( 'src' => setImageScheme('travel-widget-thumbnail'), 'class' => 'img-responsive' )); ?>
										</a>

									<?php } else { ?>

										<a href="<?=custom_category_link(); ?>"><img width="277px" height="200px" src="<?=get_bloginfo( 'stylesheet_directory' ) ?>/img/no-image-available-277x277.png" class="img-responsive" /></a>

									<?php } ?>

								</div>

								<div class="tr-category-info col-xs-12 col-sm-6 col-md-7 col-lg-7">

									<a href="<?php the_permalink(); ?>"><h2><?php the_title(); ?></h2></a>

									<div class="hidden-sm"><?php the_excerpt(); ?></div>

									<div class="tr-cat-view-more-button"><a href="<?php the_permalink(); ?>" title="Leer Más sobre <?php the_title_attribute(); ?>" class="btn btn-warning">Más Info</a></div>

								</div>

							</article>

						</div>

					<?php endwhile; ?>

					<div class="col-xs-12 col-md-12">

							<?=google_ads('btf-category-right-content', 'ad-margin-bottom ad-margin-top', 'auto'); ?>

					</div>

					<?php tr_pagination(); ?>

				<?php endif; ?>

			</div>

	</div>

<?php get_footer(); ?>