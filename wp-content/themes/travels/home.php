<?php get_header(); ?>
      <div class="container tr-home-form-slider">
		<div class="slider">
			<div class="tr-home-form col-sm-12 col-md-6 col-lg-5">
				<div class="row">
					<div class="col-sm-12 col-md-12 col-lg-12">
						<h1><?php _e( 'Best prices for hotels in ', 'travels' ) ?> <strong><?=$GLOBALS['sites'][SC]['site_display'];?></strong></h1>
					</div>
				</div>
				<div class="tr-form col-md-12">
					<?=displayForm('home'); ?>
				</div>
			</div>
			<div class="tr-corousel col-sm-12 col-md-6 col-lg-7 hidden-sm hidden-xs">
				<div id="banner-fade">
					<ul class="bjqs">
						<?php travel_get_post_gellery(); ?>
					</ul>
				</div>
			</div>
		</div>
    </div>
	<div class="container tr-recomended-container">
		<div class="tr-recomended-widget col-xs-12 col-sm-12 col-md-9 col-lg-9">
			<?php dynamic_sidebar ('Home Center'); ?>
			<div class="tr-featured col-md-12 col-lg-12 clearfix">
				<?php dynamic_sidebar('Home Middle'); ?>
			</div>
		</div>
		<div id="tr-sidebar" class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
			<?php dynamic_sidebar('Home Right Sidebar'); ?>
		</div>
	</div>
	<div class="tr-certificates">
		<div class="container">
			<div class="col-sm-2 col-md-2 col-lg-2">
				<div class="tr-certificates-logo"><img src="<?=get_bloginfo('template_url'); ?>/img/positive-cert.png" class="img-responsive" width="68px" /></div>
			</div>
			<div class="col-sm-3 col-md-2 col-lg-2">
				<div class="tr-certificates-logo truste-cert"><img src="<?=get_bloginfo('template_url'); ?>/img/truste.png" class="img-responsive" width="150px" /></div>
			</div>
			<div class="col-sm-2 col-md-2 col-lg-2">
				<div class="tr-certificates-logo site-lock"><a href="#" onclick="window.open('https://www.sitelock.com/verify.php?site=mendoza.travel','SiteLock','width=600,height=600,left=160,top=170');" ><img src="<?=get_bloginfo('template_url'); ?>/img/site-lock-cert.png" class="img-responsive" width="90px" /></a></div>
			</div>
			<div class="col-sm-6 col-md-6 col-lg-6">
			</div>
		</div>
	</div>
<?php get_footer(); ?>