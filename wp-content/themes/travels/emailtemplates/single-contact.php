<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
	<title>Formulario de contacto desde Mendoza.travel</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width">
</head>
<body>
	<h4>Contacto desde %%SITE%%</h4>
	<p><strong>Nombre: </strong>%%NAME%%</p>
	<p><strong>Correo: </strong>%%MAIL%%</p>
	<p><strong>Teléfono: </strong>%%PHONE%%</p>
	<p><strong>Fecha: </strong>%%DATE%%</p>
	<p><strong>Mensaje: </strong></p>
	<p>%%MSG%%</p>
</body>
</html>