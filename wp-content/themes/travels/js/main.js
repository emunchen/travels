$( document ).ready(function() {
	$( '#passengers' ).on( 'click', function() {
		$( '.passengers-modal' ).show();
	});

	$( '#pax-button' ).on( 'click', function() {

		$('.passengers-modal').hide();
		var count = 
		Number($( '#Group1_Adults' ).val()) +
		Number($( '#Group1_Children' ).val()) +
		Number($( '#Group2_Adults' ).val()) +
		Number($( '#Group2_Children' ).val()) +
		Number($( '#Group3_Adults' ).val()) +
		Number($( '#Group3_Children' ).val()) +
		Number($( '#Group4_Adults' ).val()) +
		Number($( '#Group4_Children' ).val());
		$('#passengers').val(count + ' Pasajeros');
	});

	$('#rooms').change(function() {

		if ($(this).val() == '1' ) {
			$( '#room-2' ).hide();
			$( '#room-3' ).hide();
			$( '#room-4' ).hide();
		}

		if ($(this).val() == '2' ) {
			$( '#room-2' ).show();
			$( '#room-3' ).hide();
			$( '#room-4' ).hide();
		}

		if ($(this).val() == '3' ) {
			$( '#room-2' ).show();
			$( '#room-3' ).show();
			$( '#room-4' ).hide();
		}

		if ($(this).val() == '4' ) {
			$( '#room-2' ).show();
			$( '#room-3' ).show();
			$( '#room-4' ).show();
		}
	});

	var $from = $('#from'),	$to = $('#to');

	$from.add($to).change(function () {
		var dayFrom = $from.datepicker('getDate');
		var dayTo = $to.datepicker('getDate');

		if (dayFrom && dayTo) {
			var days = calcDaysBetween(dayFrom, dayTo);
			if (days < 0) {
				$( '#Nights' ).val(3);
			} else {
				$( '#Nights' ).val(days);
			}
		}
	});

	function calcDaysBetween(startDate, endDate) {
		return (endDate - startDate) / (1000 * 60 * 60 * 24);
	}
});

/*** Send Search form  ***/
$('#tr-home-form').submit(function(event) {
	event.preventDefault();
	$.ajax({
        data: $(this).serialize(),
        type: 'POST',
        url: $(this).attr('action'),
		beforeSend: function () {
			$('#loading-modal').modal({
				keyboard: false,
				backdrop: 'static',
				show: true
			});
		},
		statusCode: {
			500: function() {
				$('#loading-modal').modal('hide');
				$('#error-modal').modal({
					show: true
				});
			}
		},
		success: function(response) {
			$( '.modal-first-title' ).hide();
			$( '.modal-second-title' ).show();
			window.location = response;
		}
	});
});

/*** Send Custom Contact form  ***/
$('#tr-custom-contact-form').submit(function(event) {
	event.preventDefault();
	$.ajax({
        data: $(this).serialize(),
        type: 'POST',
        url: $(this).attr('action'),
		beforeSend: function () {
			if (!$( '#tr-custom-contact-form' ).validationEngine( 'validate' )) {
					return false;
				} else {
				$( '#custom-contact-modal' ).modal({
					keyboard: false,
					backdrop: 'static',
					show: true
				});
			}
		},
		statusCode: {
			500: function() {
				$( '#custom-contact-modal' ).modal( 'hide' );
				$( '#custom-contact-error-modal' ).modal({
					show: true
				});
			}
		},
		success: function(response) {
			$( '.custom-contact-modal-first-title' ).hide();
			$( '.custom-contact-modal-second-title' ).show();
			$( '.progress' ).hide();
			$( '.modal-email-close' ).show();
			$( '.modal-email' ).show();
			if ($( '#track' ).val() === 'True') {
				fb_trackConv('191094691252807');
				ads_trackConv('1069898183');
				_gaq.push(['_trackEvent', 'local-tours', 'subscription', 'main-landing',, false]);
				window._fbq.push(['track', 'Lead']);
			}
			clear_input();
			$('#tr-phone').css('background-color', ''); 
			$('#tr-email').css('background-color', ''); 
			$('.tr-check-email').hide();
			$('.tr-check').hide();
		}
	});
});


function getSearchParameters() {
      var prmstr = window.location.search.substr(1);
      return prmstr != null && prmstr != "" ? transformToAssocArray(prmstr) : {};
}

function transformToAssocArray( prmstr ) {
    var params = {};
    var prmarr = prmstr.split("&");
    for ( var i = 0; i < prmarr.length; i++) {
        var tmparr = prmarr[i].split("=");
        params[tmparr[0]] = tmparr[1];
    }
    return params;
}
var params = getSearchParameters();
$.each( params, function( i, val ) {

	if (!$.isEmptyObject(getCookie(i))) {
		$('#tr-' + i).val(val);
	} else {
		if (!$.isEmptyObject(i)) {
			$('#tr-' + i).val(val);
			setCookie(i, val , 10);
		}
	}
});

var tr_utms = {
	'utm_source': '',
	'utm_content': '',
	'utm_term': '',
	'utm_campaign': '',
	'utm_medium': ''
}

if ($.isEmptyObject(params)) {
	$.each( tr_utms, function( i, val ) {
		if (getCookie(i)) {
			$('#tr-' + i).val(getCookie(i));
		}
	});
}

function isEmpty(str) {
    return (!str || 0 === str.length);
}

function getCookie(name) {
	var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+d.toUTCString();
    var domain = window.location.host;
    document.cookie = cname + "=" + cvalue + "; " + expires + ";domain=." + domain + ";path=/";
}

/*** Init validation engine  ***/
$("#tr-custom-contact-form").validationEngine({
	promptPosition : "bottomLeft",
	showOneMessage: true,
	scroll:false
});

function clear_input() {
$(':input','#tr-custom-contact-form')
  .not(':button, :submit, :reset, :hidden')
  .val('')
  .removeAttr('checked')
  .removeAttr('selected')
}

$('#booking-send').on('click', function() {

	var site = $('#site').val();

	if (site == 'BuenosAires') {
		var res = site.split('Buenos');
		var res = 'Buenos ' + res[1];
	} else {
		var res = 'Mendoza';
	}

	if ($('#ss').val() == '') {
		$('#ss').val(res);
	}

});

$('#category-send').on('click', function() {

	var site = $('#site').val();

	if (site == 'BuenosAires') {
		var res = site.split('Buenos');
		var res = 'Buenos ' + res[1];
	} else {
		var res = 'Mendoza';
	}

	if ($('#ss').val() == '') {
		$('#ss').val(res);
	}

});

function setBar() {
	var progress = setInterval(function() {
		var $bar = $('.bar');
		if ($bar.width() > 500) {
			clearInterval(progress);
			$('.progress').removeClass('active');
			$( '.modal-first-title' ).hide();
			$( '.modal-second-title' ).show();
			$bar.width(0);
		} else {
			$bar.width($bar.width() + 50);
		}
		$bar.text($bar.width() / 5 + "%");
	}, 800);
}

/*** Set FB pixel conversion  ***/
function fb_trackConv(fb_conversion_id) {
	var image = new Image(1,1); 
	image.src = "https://www.facebook.com/tr/?id=" + fb_conversion_id + "&ev=PageView&cd[value]=0.00&cd[currency]=ARS&noscript=1"; 	
} 

/*** Set Adwords pixel conversion  ***/
function ads_trackConv(ads_conversion_id) {
	var image = new Image(1,1); 
	image.src = "http://www.googleadservices.com/pagead/conversion/" + ads_conversion_id + "/?label=ynWkCMmpnFkQx7OV_gM&guid=ON&script=0"; 	
}