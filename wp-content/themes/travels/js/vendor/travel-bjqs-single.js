jQuery(document).ready(function($) {
	$('#banner-fade').bjqs({
		animtype      : 'slide',
		height        : 462,
		width         : 730,
		responsive    : true,
		randomstart   : true,
		showmarkers   : false,
		animspeed     : 3300,
		nexttext      : '<img src="/wp-content/themes/travels/img/arrow-right.png" />',
		prevtext      : '<img src="/wp-content/themes/travels/img/arrow-left.png" />'
	});
});
