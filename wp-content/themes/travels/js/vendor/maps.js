function initialize() {
	var myLatlng = new google.maps.LatLng(-32.891129,-68.842838);
	var mapOptions = {
		zoom: 17,
		center: myLatlng,
		mapTypeId: google.maps.MapTypeId.HYBRID
	};

	var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

	var contentString = '<div class="ca-window-map"><img src="/wp-content/themes/travels/img/logo.png" width="140" style="float:left; margin:0 11px 0px 0;"><br /><p>Rivadavia 244, Ciudad, Mendoza, Argentina</p><p>+54 261 4380505</p></div>';

	var infowindow = new google.maps.InfoWindow({
		content: contentString
	});

	var marker = new google.maps.Marker({
		position: myLatlng,
		map: map,
		title: 'Cumbres Andinas'
	});
	infowindow.open(map,marker);
}

function loadScript() {
	var script = document.createElement('script');
	script.type = 'text/javascript';
	script.src = 'https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&' + 'callback=initialize';
	document.body.appendChild(script);
}
window.onload = loadScript;