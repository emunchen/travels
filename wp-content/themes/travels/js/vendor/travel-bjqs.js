jQuery(document).ready(function($) {
	var width = $(window).width();
	var slider_width, slider_height;

	if (width > 1200 ) {

		slider_width	=	665;
		slider_height	=	357;

	} else {

		slider_width	=	470;
		slider_height	=	357;
	}

	$('#banner-fade').bjqs({
		animtype      : 'slide',
		height        : slider_height,
		width         : slider_width,
		responsive    : false,
		randomstart   : true,
		showmarkers   : false,
		animspeed     : 3300,
		nexttext      : '<img src="/wp-content/themes/travels/img/arrow-right.png" />',
		prevtext      : '<img src="/wp-content/themes/travels/img/arrow-left.png" />'
	});
});
