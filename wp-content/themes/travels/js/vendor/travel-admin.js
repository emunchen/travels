jQuery(document).ready(function ($) {


	$('#travels_form_title_es').show();
	$('#travels_email_subject_es').show();

	$('#travels_form_title_pt').hide();
	$('#travels_form_title_en').hide();
	$('#travels_email_subject_pt').hide();
	$('#travels_email_subject_en').hide();

	$('li[lang="es"]').click(function() {
		$('#travels_form_title_es').show();
		$('#travels_email_subject_es').show();

		$('#travels_form_title_pt').hide();
		$('#travels_form_title_en').hide();
		$('#travels_email_subject_pt').hide();
		$('#travels_email_subject_en').hide();
	});

	$('li[lang="pt"]').click(function() {
		$('#travels_form_title_pt').show();
		$('#travels_email_subject_pt').show();

		$('#travels_form_title_es').hide();
		$('#travels_form_title_en').hide();
		$('#travels_email_subject_es').hide();
		$('#travels_email_subject_en').hide();
	});

	$('li[lang="en"]').click(function() {
		$('#travels_form_title_en').show();
		$('#travels_email_subject_en').show();

		$('#travels_form_title_es').hide();
		$('#travels_form_title_pt').hide();
		$('#travels_email_subject_es').hide();
		$('#travels_email_subject_pt').hide();
	});
});