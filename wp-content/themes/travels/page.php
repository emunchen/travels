<?php get_header(); ?>

<div class="tr-category-container container">

	<?php if ( function_exists('yoast_breadcrumb') ): ?>

		<?=google_ads_unit(); ?>

		<?php yoast_breadcrumb('<p id="breadcrumbs">','</p>'); ?>

	<?php endif; ?>

	<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
		<?php get_sidebar( 'category' ); ?>
	</div>

	<?php while ( have_posts() ) : the_post(); ?>

		<div class="tr-single-main col-xs-12 col-sm-8 col-md-8 col-lg-8">

			<?=google_ads('atf-single-right-content', 'ad-margin-bottom', 'auto'); ?>

			<div class="tr-gallery-single">

				<div class="tr-single-title transparent tr-relative-title"><h1><?php the_title(); ?></h1></div>

					<?php if ( has_post_thumbnail() ): ?>
						<div class="tr-swipebox-gallery">
							<?=setSwipeboxGallery($post->ID); ?>
						</div>
					<?php endif; ?>

				</div>

			<div class="tr-single-content">
				<div class="social">
					<span class="google">
						<?=gl_html();?>
					</span>
					<span class="facebook">
						<?=fb_html();?>
					</span>
					<span class="twitter">
						<?=tw_html();?>
					</span>
					
				</div>
				<?php the_content(); ?>
				<?=google_ads('btf-single-right-content', null, 'auto'); ?>

			</div>

		</div>

	<?php endwhile; ?>

</div>

<?php get_footer(); ?>