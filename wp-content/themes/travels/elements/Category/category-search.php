<?php

/**
 * Create Sidebar Travel Form.
 *
 **/

?>
	<h4><?php _e( 'Search Hotels', 'travels' ) ?></h4>
	<form action="<?php echo get_bloginfo('template_url'); ?>/pxsol/post.php" type="POST" id="tr-home-form">
		<div class="row clearfix">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
				<div class="form-group">
					<label for="from"><?php _e( 'Check In', 'travels' ) ?></label>
					<input type="text" name="Start" id="from" class="form-control" placeholder="dd/mm/aaaa">
				</div>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
				<div class="form-group">
					<label for="to"><?php _e( 'Check Out', 'travels' ) ?></label>
					<input type="text" name="End" id="to" class="form-control" placeholder="dd/mm/aaaa">
				</div>
			</div>
			<input name="Currency" type="hidden" id="currency" value="ARS" />
			<input name="Nights" type="hidden" id="Nights" size="3" value="" />
			<input name="Pos" type="hidden" id="Pos" value="<?=$GLOBALS['sites'][SC]['site_display']; ?> Travel">
			<input name="Lng" type="hidden" id="Lng" value="">
			<input name="SubType" type="hidden" id="SubType" value="">
			<input name="Channel" type="hidden" id="Channel" value="2">
			<input name="RateType" type="hidden" id="RateType" value="auto">
			<input name="ReturnUrl" type="hidden" id="ReturnUrl" value="<?=$GLOBALS['sites'][SC]['pxsol_return_url']; ?>">
			<input name="FromUrl" type="hidden" id="FromUrl" value="http://reservas.mendoza.travel/widget/search_ota.php?pos=<?=$GLOBALS['sites'][SC]['site_display']; ?> Travel">
			<input name="Type" type="hidden" id="Type" value="Hotel">
			<input name="ListID" type="hidden" id="ListID" value="<?=$GLOBALS['sites'][SC]['pxsol_ListID']; ?>">
		</div>
		<div class="row clearfix passengers-fix">
			<div class="col-xs-8 col-sm-12 col-md-12 col-lg-12">
				<div class="form-group">
					<label for="exampleInputEmail1"><?php _e( 'Passengers', 'travels' ) ?></label>
					<input type="text" name="passengers" id="passengers" class="form-control" placeholder="<?php printf( __( '%d Passengers', 'travels' ), '2' ); ?>" value="<?php printf( __( '%d Passengers', 'travels' ), '2' ); ?>">
				</div>
			</div>
			<div class="passengers-modal">
				<table border="0" cellspacing="0" cellpadding="3" class="modal-table">
						<tr>
							<th>
								<select name="Groups" id="rooms" class="distribution_rooms form-control">
									<option value="1"><?php printf( _n( '%d Room', '%d Rooms', '1', 'travels' ), '1' ); ?></option>
									<option value="2"><?php printf( _n( '%d Room', '%d Rooms', '2', 'travels' ), '2' ); ?></option>
									<option value="3"><?php printf( _n( '%d Room', '%d Rooms', '3', 'travels' ), '3' ); ?></option>
									<option value="4"><?php printf( _n( '%d Room', '%d Rooms', '4', 'travels' ), '4' ); ?></option>
								</select>
							</th>
							<th><strong><?php _e( 'Adults', 'travels' ) ?></strong></th>
							<th><strong><?php _e( 'Childrens', 'travels' ) ?></strong></th>
						</tr>
						<tr id="room-1">
							<td valign="top" class="distribution_room_title">
								<p><strong><?php printf( __( 'Room %d', 'travels' ), '1' ); ?></strong></p>
							</td>
							<td valign="top">
								<select name="Group1_Adults" id="Group1_Adults" class="distribution_adults form-control">
								<option value="0">0</option>
								<option value="1">1</option>
								<option value="2" selected="selected">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
								</select>
							</td>
							<td valign="top">
								<select name="Group1_Children" id="Group1_Children" class="distribution_kids form-control">
									<option value="0" selected="selected">0</option>
									<option value="1">1</option>
									<option value="2">2</option>
								</select>
							</td>
						</tr>
						<tr id="room-2">
							<td valign="top" class="distribution_room_title">
								<p><strong><?php printf( __( 'Room %d', 'travels' ), '2' ); ?></strong></p>
							</td>
							<td valign="top">
								<select name="Group2_Adults" id="Group2_Adults" class="distribution_adults form-control">
								<option value="0" selected="selected">0</option>
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
								</select>
							</td>
							<td valign="top">
								<select name="Group2_Children" id="Group2_Children" class="distribution_kids form-control">
									<option value="0" selected="selected">0</option>
									<option value="1">1</option>
									<option value="2">2</option>
								</select>
							</td>
						</tr>
						<tr id="room-3">
							<td valign="top" class="distribution_room_title">
								<p><strong><?php printf( __( 'Room %d', 'travels' ), '3' ); ?></strong></p>
							</td>
							<td valign="top">
								<select name="Group3_Adults" id="Group3_Adults" class="distribution_adults form-control">
								<option value="0" selected="selected">0</option>
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
								</select>
							</td>
							<td valign="top">
								<select name="Group3_Children" id="Group3_Children" class="distribution_kids form-control">
									<option value="0" selected="selected">0</option>
									<option value="1">1</option>
									<option value="2">2</option>
								</select>
							</td>
						</tr>
						<tr id="room-4">
							<td valign="top" class="distribution_room_title">
								<p><strong><?php printf( __( 'Room %d', 'travels' ), '4' ); ?></strong></p>
							</td>
							<td valign="top">
								<select name="Group4_Adults" id="Group4_Adults" class="distribution_adults form-control">
									<option value="0" selected="selected">0</option>
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
								</select>
							</td>
							<td valign="top">
								<select name="Group4_Children" id="Group4_Children" class="distribution_kids form-control">
									<option value="0" selected="selected">0</option>
									<option value="1">1</option>
									<option value="2">2</option>
								</select>
							</td>
						</tr>
				</table>
				<div class="form-group pax-button">
					<div class="col-sm-12">
						<a href="javascript:void(0);" id="pax-button" class="btn btn-warning btn-md"><?php _e( 'Acept', 'travels' ) ?></a>
					</div>
				</div>
			</div>
		</div>
		<div class="row clearfix">
			<div class="col-sm-7 col-md-10 col-lg-10">
				<div class="form-group tr-home-submit">
					<button type="submit" class="btn btn-warning btn-lg"><?php _e( 'Search', 'travels' ) ?></button>
				</div>
			</div>
		</div>
	</form>