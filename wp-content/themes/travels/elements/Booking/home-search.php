<?php

/**
 * Home Booking Form Template.
 *
 **/

	$bookingConf = $GLOBALS['sites'][SC]['plugins']['booking'];
?>
	<form action='<?=$bookingConf['resultpage'];?>' type='GET' id='booking-form' style='margin-top:58px;' target='_blank'>
		<input type='hidden' id='checkin_monthday' name='checkin_monthday' value='<?=date("d", strtotime("tomorrow"));?>' />
		<input type='hidden' id='checkin_year_month' name='checkin_year_month' value='<?=date("Y-m", strtotime("tomorrow"));?>' />
		<input type='hidden' id='checkout_monthday' name='checkout_monthday' value='<?=date("d", strtotime("+ 2 days"));?>' />
		<input type='hidden' id='checkout_year_month' name='checkout_year_month' value='<?=date("Y-m", strtotime("+ 2 days"));?>' />
		<input type='hidden' name='error_url' value='<?=$bookingConf['errorurl']?><?=$bookingConf['aid']?>' />
		<input type='hidden' name='si' value='<?=$bookingConf['si']?>' />
		<input type='hidden' name='label' value='wp-searchbox-widget-<?=$bookingConf['aid']?>' />
		<input type='hidden' name='aid' value='<?=$bookingConf['aid']?>' />
		<input type='hidden' name='utm_campaign' value='search_box' />
		<input type='hidden' name='utm_medium' value='sp' />
		<input type='hidden' name='utm_term' value='wp-searchbox-widget-<?=$bookingConf['aid']?>' />
		<input type='hidden' name='do_availability_check' value='on' />
		<input type='hidden' id='site' value='<?=$GLOBALS['sites'][SC]['site_display'];?>' />
		<input type='hidden' name='ss' id='ss' value='' />
		<div class="row clearfix">
			<div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
				<div class="form-group">
					<label for="from"><?php _e( 'Check In', 'travels' ); ?></label>
					<div class="input-group input-group-md">
						<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
						<input type="text" name="checkin" id="from" class="form-control" placeholder="dd/mm/aaaa">
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
				<div class="form-group">
					<label for="from"><?php _e( 'Check Out', 'travels' ); ?></label>
					<div class="input-group input-group-md">
						<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
						<input type="text" name="checkout" id="to" class="form-control" placeholder="dd/mm/aaaa">
					</div>
				</div>
			</div>
		</div>
		<div class="row clearfix">
			<div class="col-sm-10">
				<div class="form-group booking-home-submit">
					<button id="booking-send" type="submit" class="btn btn-warning btn-lg"><?php _e( 'Search', 'travels' ); ?></button>
				</div>
			</div>
		</div>
	</form>