<?php

/**
 * Set PHP sessions.
 *
 *
 * PHP version 5
 *
 * @package Referral
 * @author Emanuel Pécora
 * @copyright  Summit S.A.
 */

/**
 * Call session_start() if session is not set
 * @param void
 * @return void
 */

function StartSession() {
	ini_set('session.gc_maxlifetime', 30 * 60 * 60 * 24);
	ini_set('session.cookie_lifetime', 30 * 60 * 60 * 24);

	session_start();
	$_SESSION['referrals'] = array(
		'utm_source'	=> tr_get_source(),
		'utm_medium'	=> tr_get_medium(),
		'utm_term'		=> tr_get_term(),
		'utm_campaign'	=> tr_get_campaign(),
		'utm_content'	=> tr_get_content(),
		'org_referer'	=> tr_get_org_referer()
	);
}

/**
 * Destroy session
 * @param void
 * @return void
 */

function EndSession() {
	session_destroy ();
}

/**
 * Get referer
 * @param void
 * @return array
 */

function tr_get_org_referer() {

	if (empty($_SESSION['referrals']['org_referer']))
		if(isset($_SERVER['HTTP_REFERER']))
			return $_SERVER['HTTP_REFERER'];

	if (!empty($_SESSION['referrals']['org_referer']))
		return $_SESSION['referrals']['org_referer'];
}

/**
 * Get source
 * @param void
 * @return array
 */

function tr_get_source() {

	if (empty($_SESSION['referrals']['utm_source']))
		if(isset($_GET['utm_source']))
			return $_GET['utm_source'];

	if (!empty($_SESSION['referrals']['utm_source']))
		return $_SESSION['referrals']['utm_source'];
}

/**
 * Get medium
 * @param void
 * @return array
 */

function tr_get_medium() {

	if (empty($_SESSION['referrals']['utm_medium']))
		if(isset($_GET['utm_medium']))
			return $_GET['utm_medium'];

	if (!empty($_SESSION['referrals']['utm_medium']))
		return $_SESSION['referrals']['utm_medium'];
}

/**
 * Get term
 * @param void
 * @return array
 */

function tr_get_term() {

	if (empty($_SESSION['referrals']['utm_term']))
		if(isset($_GET['utm_term']))
			return $_GET['utm_term'];

	if (!empty($_SESSION['referrals']['utm_term']))
		return $_SESSION['referrals']['utm_term'];
}

/**
 * Get campaign
 * @param void
 * @return array
 */

function tr_get_campaign() {

	if (empty($_SESSION['referrals']['utm_campaign']))
		if(isset($_GET['utm_campaign']))
			return $_GET['utm_campaign'];

	if (!empty($_SESSION['referrals']['utm_campaign']))
		return $_SESSION['referrals']['utm_campaign'];
}

/**
 * Get content
 * @param void
 * @return array
 */

function tr_get_content() {

	if (empty($_SESSION['referrals']['utm_content']))
		if(isset($_GET['utm_content']))
			return $_GET['utm_content'];

	if (!empty($_SESSION['referrals']['utm_content']))
		return $_SESSION['referrals']['utm_content'];
}

/**
 * Create hidden inputs
 * @param void
 * @return html
 */

function referrals_inputs() {

	$inputs = '';
	$referrals = array(
		'utm_source',
		'utm_medium',
		'utm_term',
		'utm_campaign',
		'utm_content',
		'org_referer'
	);
	foreach($referrals as $referral) {
		$inputs .= '<input id=\'tr-' .$referral . '\' type=\'hidden\' name=\'' .$referral . '\' value=\'\'/>';
	}
	return $inputs;
}
?>