<?php
header($_SERVER["SERVER_PROTOCOL"] . ' 200 Ok');
if (!$_POST) {
	exit;
}

$subscriber = $_POST;

require_once('../../../../../wp-load.php');

$email_config = $GLOBALS['sites'][SC]['email'];

// Get categories from single post.
$single_cat = get_categories_ids($subscriber['id']);

// Get template from specific site.
$template = tr_get_template($single_cat);

// Replace all variables on template.
$message = replace_template_variables($template['value'], $subscriber);

$email = get_post_meta( $subscriber['id'], 'travels_email_contact', true);

if (!empty($email)) {
	$mainSubject = get_email_subject($subscriber['id'], $subscriber['language'], $template['subject']);
}

if (empty($email)) {
	// It's a Category.
	$email = get_tax_meta($subscriber['id'], 'travels_email_recipients');
	if (!empty($email)) {
		$email = explode(',', $email);
	}
	$mainSubject = get_tax_meta($subscriber['id'], 'travels_cat_subject_' . $subscriber['language']);
}

$bcc = get_post_meta( $subscriber['id'], 'travels_email_subject_CC', true);
if (empty($bcc)) {
	$bcc = get_tax_meta($subscriber['id'], 'travels_cc_recipients');
	if (!empty($bcc)) {
		$bcc = explode(',', $bcc);
	}
}

default_sender($subscriber['email'], $subscriber['name'], $email, $mainSubject, $message, $bcc);

$required = array(
	'name',
	'email',
	'location_by_phone',
	'phone',
	'message',
	'org_referer',
	'utm_source',
	'utm_campaign',
	'utm_term'
);

foreach($required as $field) {
	if (isset($subscriber[$field])) {
		$save[$field] = $subscriber[$field];
	}
}

$dateTime = new DateTime(date('Y-m-d H:i'), new DateTimeZone('GMT'));
$dateTime->setTimezone(new DateTimeZone('America/Argentina/Mendoza'));

$crm_config = $GLOBALS['sites'][SC]['crm'];
$save['created'] = $dateTime->format('Y-m-d H:i');
$save['landing'] = $subscriber['title'];
$save['site'] = $GLOBALS['sites'][SC]['fancy_name'];
$save['post_id'] = $subscriber['id'];
$save['site_id'] = $crm_config['site_id'];

if (empty($subscriber['utm_source']))
	$save['utm_source'] = 'seo';

$db = new wpdb($crm_config['db_user'], $crm_config['db_password'], $crm_config['db_name'], $crm_config['db_host']);
$db->insert( 'subscribers', $save, array('%s', '%s', '%s', '%s', '%s', '%s', '%s' , '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s'));
if (!empty($db->last_error)) {
	write_log($subscriber['name']);
	write_log($db->last_error);
}
$provider = get_post_meta($subscriber['id'], 'travels_provider', true);

if (empty($provider)) {
	$provider = get_category_provider($subscriber['id']);
}

if (!empty($provider)) {

	$lead = array(
		'subscriber_id' => $db->insert_id,
		'provider_id' => $provider,
		'status_id' => 1,
		'created' => $dateTime->format('Y-m-d H:i'),
		'modified' => $dateTime->format('Y-m-d H:i')
	);
	$db->insert('leads', $lead, array('%s', '%s', '%s', '%s', '%s', '%s'));
	if (!empty($db->last_error)) {
		write_log($subscriber['name']);
		write_log($db->last_error);
	}
}

/**
 *
 * Get the first category ID from post.
 *
 **/

function get_categories_ids($post_id = null, $index = 0) {

	if (empty($post_id))
		return;

	$categories = get_the_category($post_id);
	if (empty($categories)) {
		return $post_id;
	}

	return $categories[$index]->term_id;
}

/**
 *
 * Get email subject post meta
 *
 **/

function get_email_subject($post_id = null, $language = null, $template_subject = null) {

	$subject = get_post_meta( $post_id, 'travels_email_subject_' . $language, true );

	if (!empty($subject)) {
		return $subject;
	}

	if (!empty($template_subject)) {
		return $template_subject;
	}

	return false;
}

/**
 *
 * Replace template variables with subscriber data.
 *
 **/

function replace_template_variables($message = null, $data = array()) {

	if (empty($message))
		return;

	$data['date'] = date('d/m/Y H:i:s');
	$data['site'] = $GLOBALS['sites'][SC]['fancy_name'];

	$variables = array(
		'%%NAME%%' => $data['name'],
		'%%MAIL%%' => $data['email'],
		'%%PHONE%%' => $data['phone'],
		'%%MSG%%' => $data['message'],
		'%%DATE%%' => $data['date'],
		'%%SITE%%' => $data['site']
	);

	foreach ($variables as $key => $value) {
		$message = str_replace($key, $value, $message);
	}

	return $message;
}

/**
 *
 * Get template from CRM.
 *
 **/

function tr_get_template($category_id = null) {

	if (empty($category_id))
		return;

	$crm_config = $GLOBALS['sites'][SC]['crm'];
	$db = new wpdb($crm_config['db_user'], $crm_config['db_password'], $crm_config['db_name'], $crm_config['db_host']);

	$template = $db->get_row( "SELECT * FROM  `templates` WHERE  `site_id` = " . $crm_config['site_id'] . " AND  `wpcategory_id` = " . $category_id . " ", ARRAY_A );
	if (empty($template)) {
		$template = $db->get_row( "SELECT * FROM  `templates` WHERE  `site_id` = " . $crm_config['site_id'] . " AND  `default` = 1", ARRAY_A );
	}
	return $template;
}

function get_category_provider($category_id = null) {
	if (empty($category_id)) {
		return;
	}

	$providers = array(
		'56' => 4,
		'57' => 3,
		'41' => 2,
		'161' => 4
	);

	if (isset($providers[$category_id])) {
		return $providers[$category_id];
	}
	return false;
}

?>