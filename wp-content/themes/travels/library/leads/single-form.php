<link rel="stylesheet" href="<?=get_stylesheet_directory_uri()?>/js/vendor/intl-tel/css/intlTelInput.css">
<div class="tr-custom-form single-form col-sm-12 col-md-9 col-lg-9">
	<div class="row single-form-title">
		<div class="form-header col-sm-12 col-md-12 col-lg-12">
			<h2>
				<?php if (get_form_title()): ?>

					<?=get_form_title(); ?>

				<?php else: ?>

					<?php _e( 'Contact ', 'travels' ); the_title(); ?>

				<?php endif; ?>
			</h2>
		</div>
	</div>
	<div class="row">
		<div class="tr-form col-md-12">
			<form action="<?php bloginfo('template_url'); ?>/library/leads/add.php" type="POST" id="tr-custom-contact-form">
				<div class="row clearfix">
					<div class="col-xs-12 col-md-12 col-lg-12">
						<div class="form-group">
							<label for="email"><?php _e( 'E-Mail', 'travels' ) ?> <span class="tr-custom-required">* <?php _e( 'Required', 'travels' ) ?></span></label>
							<div class="input-group input-group-md">
								<i class="fa fa-cog fa-spin fa-2x fa-fw margin-bottom tr-phone-spin tr-cog-email" style="display:none;"></i>
								<i class="fa fa-check fa-2x tr-phone-spin tr-check-email" style="display:none;"></i>
								<span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
								<input autocomplete="off" id="tr-email" type="email" name="email" class="form-control validate[required, custom[email]]" placeholder="<?php _e( 'E-Mail', 'travels' ) ?>">
							</div>
							<label for="name"><?php _e( 'Name', 'travels' ) ?> <span class="tr-custom-required">* <?php _e( 'Required', 'travels' ) ?></span></label>
							<div class="input-group input-group-md">
								<span class="input-group-addon"><i class="fa fa-user"></i></span>
								<input autocomplete="off" type="text" name="name" class="form-control validate[required]" placeholder="<?php _e( 'Name', 'travels' ) ?>">
							</div>
							<label for="phone"><?php _e( 'Phone', 'travels' ) ?> <span class="tr-custom-required">* <?php _e( 'Required', 'travels' ) ?></span></label>
							<div class="input-group input-group-md" style="width: 100%;">
								<input autocomplete="off" id="tr-phone" type="tel" name="phone" class="form-control validate[required]">
								<i class="fa fa-cog fa-spin fa-2x fa-fw margin-bottom tr-phone-spin tr-cog" style="display:none;"></i>
								<i class="fa fa-check fa-2x tr-phone-spin tr-check" style="display:none;"></i>
							</div>
							<label for="message"><?php _e( 'Message', 'travels' ) ?> <span class="tr-custom-required">* <?php _e( 'Required', 'travels' ) ?></span></label>
							<div class="input-group input-group-md">
								<span class="input-group-addon"><i class="fa fa-pencil"></i></span>
								<textarea rows="4" cols="50" name="message" class="form-control"></textarea>
							</div>
							<div class="tr-submit">
								<button id="tr-send" type="submit" disabled="disabled" class="btn btn-warning btn-lg"><?php _e( 'Check Prices', 'travels' ) ?></button>
								<input type="hidden" name="title" value="<?php the_title(); ?>" />
								<input type="hidden" name="id" value="<?=getTravelID()?>" />
								<input type="hidden" name="track" id="track" value="<?=track_form()?>" />
								<input type="hidden" name="location_by_phone" id="geophone" value="" />
								<input type="hidden" name="language" value="<?=qtrans_getLanguage();?>" />
								<?=referrals_inputs(); ?>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<script src="<?=get_stylesheet_directory_uri()?>/js/vendor/intl-tel/js/intlTelInput.min.js"></script>
<script> 
	$("#tr-phone").intlTelInput({
		initialCountry: '',
		excludeCountries: ['pm', 'mf', 'cf', 'ba', 'cd', 'st', 'ae'],
		preferredCountries: ['ar','br', 'us'],
		utilsScript: '/wp-content/themes/travels/js/vendor/intl-tel/js/utils.js' // just for formatting/placeholders etc
	});
</script>
<?php
$messages = array(
	'es' => array(
		'wrong_number'	=> 'Teléfono incorrecto. Debes escribir el número con código de área completo. Ejemplo: 011 15-2345-6789',
		'wrong_email'	=> 'Correo electrónico incorrecto. Ejemplo: ejemplo@me.com'
	),
	'pt' => array(
		'wrong_number'	=> 'Número de telefone errado. Você deve digitar o número com código de área completa. Exemplo: (11) 96123-4567',
		'wrong_email'	=> 'E-mail errado. Exemplo: exemplo@me.com'
	),
	'en' => array(
		'wrong_number'	=> 'Wrong phone number. You must enter the number with telephone area code. Example: (201) 555-5555',
		'wrong_email'	=> 'Wrong E-mail. Example: example@me.com'
	)
);

$wrong_number = $messages[qtranxf_getLanguage()]['wrong_number'];
$wrong_email = $messages[qtranxf_getLanguage()]['wrong_email'];

$url = get_template_directory_uri () . '/library/email-validate.php';
$script = <<<JS
	<script>
		var geoPhone = $('#geophone');
		var phone = $('#tr-phone');
		var submitButon = $('#tr-send');
		phone.focusout(function() {
			$('.tr-cog').hide();
			$('.tr-check').hide();
			phone.validationEngine('hide');
			var phoneValue =  $(this).val();
			var countryData = phone.intlTelInput('getSelectedCountryData');
			$.ajax({
				url: '{$url}' + '?phone=' + countryData.dialCode + phoneValue,
				beforeSend: function( xhr ) {
					$('.tr-cog').show();
				}
			})
			.done(function( data ) {
				phone.css('background-color', '#bbe4b0');
				submitButon.prop('disabled', false);
				$('.tr-cog').hide();
				$('.tr-check').show(1000);
				phone.val(data.verified.replace('+', ''));
				geoPhone.val(data.location);
			})
			.fail(function( data ) {
				phone.css('background-color', '#f3afaf');
				$('.tr-cog').hide();
				phone.validationEngine('showPrompt', '{$wrong_number}', '', 'topLeft', true)
			})
		});
	</script>
	<script>
		var email = $('#tr-email');
		var submitButon = $('#tr-send');
		email.focusout(function() {
			$('.tr-cog-email').hide();
			$('.tr-check-email').hide();
			email.validationEngine('hide');
			var emailValue =  $(this).val();
			$.ajax({
				url: '{$url}' + '?email=' + emailValue,
				beforeSend: function( xhr ) {
					$('.tr-cog-email').show();
				}
			})
			.done(function( data ) {
				email.css('background-color', '#bbe4b0');
				$('.tr-cog-email').hide();
				$('.tr-check-email').show(1000);
			})
			.fail(function( data ) {
				email.css('background-color', '#f3afaf');
				$('.tr-cog-email').hide();
				email.validationEngine('showPrompt', '{$wrong_email}', '', 'topLeft', true)
			})
		});
	</script>
JS;
?>
<?=$script?>
<div id="custom-contact-modal" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="false">
  <div class="modal-dialog modal-xs">
    <div class="modal-content">
		<div class="modal-body">
			<button type="button" class="close modal-email-close" data-dismiss="modal" aria-hidden="true" style="display:none;">&times;</button>
			<h3 class="custom-contact-modal-first-title"><?php _e( 'Sending Inquiry ...', 'travels' ) ?></h3>
			<h3 class="custom-contact-modal-second-title" style="display:none;"><?php _e( 'Your message has been sent. Thank you!', 'travels' ) ?></h3>
			<h5 style="margin-top: 20px;" class="custom-contact-modal-second-title">
				Puedes recibir más ofertas en Facebook:
			</h5>
			<div style='height: 32px;width: 148px;display: inline-block;overflow: hidden;color: #fff;'>
				<div style='background-color: #fff;pointer-events:none;position:absolute;width:148px;z-index:2;line-height:36px;'>
					<a class="btn btn-primary" href="#">
					<i class="fa fa-facebook-official fa-lg"></i> Recibir Ofertas</a>
			</div>
				<div style="margin-top: 7px;" class="fb-send-to-messenger"
					messenger_app_id="<?=$GLOBALS['sites'][SC]['facebook']['appID']?>"
					page_id="<?=$GLOBALS['sites'][SC]['facebook']['pageID']?>"
					data-ref="PASS_THROUGH_PARAM"
					color="blue"
					size="xlarge">
				</div>
			</div>
			<div class="progress progress-striped active">
				<div class="progress-bar bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
			</div>
		</div>
		<div class="modal-footer modal-email" style="display:none;">
        	<button type="button" class="btn btn-danger" data-dismiss="modal"><?php _e( 'Close', 'travels' ) ?></button>
      </div>
    </div>
  </div>
</div>
<div id="custom-contact-error-modal" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3 class="modal-title"><?php _e( 'Oops! Something went wrong. You may be able to try again.', 'travels' ) ?></h3>
      </div>
      <div class="modal-body">
        <p><?php _e( 'Please Contact us via Email - contacto@mendoza.travel', 'travels' ) ?></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php _e( 'Close', 'travels' ) ?></button>
      </div>
    </div>
  </div>
</div>