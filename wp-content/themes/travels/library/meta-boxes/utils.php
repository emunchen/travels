<?php

/**
 *
 * Get Provider list from CRM.
 *
 **/

function get_providers() {

	$crm_config = $GLOBALS['sites'][SC]['crm'];
	$db = new wpdb($crm_config['db_user'], $crm_config['db_password'], $crm_config['db_name'], $crm_config['db_host']);
	$providers = $db->get_results('SELECT * FROM providers', ARRAY_A);
	return $providers;
}

function get_provider($id = null) {

	if (empty($id)) {
		return;
	}

	$crm_config = $GLOBALS['sites'][SC]['crm'];
	$db = new wpdb($crm_config['db_user'], $crm_config['db_password'], $crm_config['db_name'], $crm_config['db_host']);
	$provider = $db->get_row('SELECT * FROM providers WHERE id = '. $id, ARRAY_A);
	return $provider;
}


function get_providers_list($object) {

	$provider_selected = '';

	$providers = get_providers();
	$selected = get_post_meta($object->ID, 'travels_provider', true);
	$provider_selected = get_provider($selected);

	$input = '<select name="travels_provider" class="form-control parsley-validated" id="PagesSiteId">';

	if ($provider_selected) {
		$input .= '<option value="' . $provider_selected['id'] . '">' . $provider_selected['name'] . '</option>';
	}

	if (empty($provider_selected)) {
		$input .= '<option value="">Seleccionar</option>';	
	}

	foreach ($providers as $provider) {
		$input .= '<option value="' . $provider['id'] . '">' . $provider['name'] . '</option>';
	}
	$input .= '<option value="">Ninguno</option>';
	$input .= '</select>';
	return $input;
}

?>