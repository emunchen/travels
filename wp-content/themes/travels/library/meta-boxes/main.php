<?php

/**
 *
 * Include some utils functions.
 *
 **/

$utils_lib = dirname(__FILE__) . '/library/meta-boxes/utils.php';


include('utils.php');

/**
 *
 * Setup meta boxes on post editor.
 *
 **/

add_action( 'load-post.php', 'travels_email_contact_post_setup' );
add_action( 'load-post-new.php', 'travels_email_contact_post_setup' );

/**
 *
 * Add meta boxes to Wordpress hook.
 *
 **/

function travels_email_contact_post_setup() {

	/* Add meta boxes on the 'add_meta_boxes' hook. */
	add_action( 'add_meta_boxes', 'travels_add_post_meta_boxes' );

	/* Save post meta on the 'save_post' hook. */
	add_action( 'save_post', 'travels_save_post_meta_boxes', 10, 2 );
}

/**
 *
 * Create one or more meta boxes to be displayed on the post editor screen.
 *
 **/

function travels_add_post_meta_boxes() {

	//Adding email contact

	add_meta_box(
		'travels_email_contact',					// Unique ID
		esc_html__( 'Email', 'travels' ),	// Title
		'travels_email_contact_meta_box',			// Callback function
		'post',										// Admin page (or post type)
		'side',										// Context
		'default'									// Priority
	);

	//Adding form title ES

	add_meta_box(
		'travels_form_title_es',					// Unique ID
		esc_html__( 'Título de Formulario (Español)', 'travels' ),		// Title
		'travels_form_title_meta_box_es',			// Callback function
		'post',										// Admin page (or post type)
		'side',										// Context
		'default'									// Priority
	);

	//Adding form title PT

	add_meta_box(
		'travels_form_title_pt',					// Unique ID
		esc_html__( 'Título de Formulario (Portugues)', 'travels' ),		// Title
		'travels_form_title_meta_box_pt',			// Callback function
		'post',										// Admin page (or post type)
		'side',										// Context
		'default'									// Priority
	);

	//Adding form title EN

	add_meta_box(
		'travels_form_title_en',					// Unique ID
		esc_html__( 'Título de Formulario (Inglés)', 'travels' ),		// Title
		'travels_form_title_meta_box_en',			// Callback function
		'post',										// Admin page (or post type)
		'side',										// Context
		'default'									// Priority
	);

	//Adding email subject ES

	add_meta_box(
		'travels_email_subject_es',					// Unique ID
		esc_html__( 'Asunto del Correo (Español)', 'travels' ),	// Title
		'travels_email_subject_meta_box_es',		// Callback function
		'post',										// Admin page (or post type)
		'side',										// Context
		'default'									// Priority
	);

	//Adding email subject PT

	add_meta_box(
		'travels_email_subject_pt',					// Unique ID
		esc_html__( 'Asunto del Correo (Portugues)', 'travels' ),	// Title
		'travels_email_subject_meta_box_pt',		// Callback function
		'post',										// Admin page (or post type)
		'side',										// Context
		'default'									// Priority
	);

	//Adding email subject EN

	add_meta_box(
		'travels_email_subject_en',					// Unique ID
		esc_html__( 'Asunto del Correo (Inglés)', 'travels' ),	// Title
		'travels_email_subject_meta_box_en',		// Callback function
		'post',										// Admin page (or post type)
		'side',										// Context
		'default'									// Priority
	);

	//Adding Booking Hotel ID

	add_meta_box(
		'travels_booking_hotel_ID',					// Unique ID
		esc_html__( 'ID de Hotel en Booking', 'travels' ),	// Title
		'travels_booking_hotel_ID_meta_box',		// Callback function
		'post',										// Admin page (or post type)
		'side',										// Context
		'default'									// Priority
	);

	//Adding Booking Hotel ID

	add_meta_box(
		'travels_despegar_hotel_ID',				// Unique ID
		esc_html__( 'ID de Hotel en Despegar', 'travels' ),	// Title
		'travels_despegar_hotel_ID_meta_box',		// Callback function
		'post',										// Admin page (or post type)
		'side',										// Context
		'default'									// Priority
	);

	//Adding CC email field.

	add_meta_box(
		'travels_email_subject_CC',					// Unique ID
		esc_html__( 'Enviar Copia a: ', 'travels' ),	// Title
		'travels_email_subject_CC_meta_box',		// Callback function
		'post',										// Admin page (or post type)
		'side',										// Context
		'default'									// Priority
	);

	//Adding Provider list.

	add_meta_box(
		'travels_provider',							// Unique ID
		esc_html__( 'Seleccionar Proveedor', 'travels' ),	// Title
		'travels_provider_meta_box',				// Callback function
		'post',										// Admin page (or post type)
		'side',										// Context
		'default'									// Priority
	);
}

/**
 *
 * Display the post meta box.
 *
 **/

function travels_email_contact_meta_box( $object, $box ) { ?>

	<?php wp_nonce_field( 'tr_email_contact', 'travel_email_contact_nonce' ); ?>
	<p>
		<label for='smashing-post-class'><?php _e( 'Email del restaurant, bodega, etc', 'travels' ); ?></label>
		<br />
		<input class='widefat' type='text' name='travels_email_contact' id='travels_email_contact' value="<?php echo esc_attr( get_post_meta( $object->ID, 'travels_email_contact', true ) ); ?>" size="30" />
	</p>

<?php }

function travels_form_title_meta_box_es( $object, $box ) { ?>

	

		<?php wp_nonce_field( 'tr_form_title_es', 'travel_form_title_nonce_es' ); ?>
		<p>
			<label for='smashing-post-class'><?php _e( 'Título de Formulario', 'travels' ); ?></label>
			<br />
			<input class='widefat' type='text' name='travels_form_title_es' id='travels_form_title_es' value="<?php echo esc_attr( get_post_meta( $object->ID, 'travels_form_title_es', true ) ); ?>" size="30" />
		</p>

	

<?php }

function travels_form_title_meta_box_pt( $object, $box ) { ?>

	<?php wp_nonce_field( 'tr_form_title_pt', 'travel_form_title_nonce_pt' ); ?>
	<p>
		<label for='smashing-post-class'><?php _e( 'Título de Formulario', 'travels' ); ?></label>
		<br />
		<input class='widefat' type='text' name='travels_form_title_pt' id='travels_form_title_pt' value="<?php echo esc_attr( get_post_meta( $object->ID, 'travels_form_title_pt', true ) ); ?>" size="30" />
	</p>

<?php }

function travels_form_title_meta_box_en( $object, $box ) { ?>

	<?php wp_nonce_field( 'tr_form_title_en', 'travel_form_title_nonce_en' ); ?>
	<p>
		<label for='smashing-post-class'><?php _e( 'Título de Formulario', 'travels' ); ?></label>
		<br />
		<input class='widefat' type='text' name='travels_form_title_en' id='travels_form_title_en' value="<?php echo esc_attr( get_post_meta( $object->ID, 'travels_form_title_en', true ) ); ?>" size="30" />
	</p>

<?php }

function travels_email_subject_meta_box_es( $object, $box ) { ?>

	<?php wp_nonce_field( 'tr_email_subject_es', 'travel_email_subject_nonce_es' ); ?>
	<p>
		<label for='smashing-post-class'><?php _e( 'Asunto del correo', 'travels' ); ?></label>
		<br />
		<input class='widefat' type='text' name='travels_email_subject_es' id='travels_email_subject_es' value="<?php echo esc_attr( get_post_meta( $object->ID, 'travels_email_subject_es', true ) ); ?>" size="30" />
	</p>	

<?php }

function travels_email_subject_meta_box_pt( $object, $box ) { ?>

	<?php wp_nonce_field( 'tr_email_subject_pt', 'travel_email_subject_nonce_pt' ); ?>
	<p>
		<label for='smashing-post-class'><?php _e( 'Asunto del correo', 'travels' ); ?></label>
		<br />
		<input class='widefat' type='text' name='travels_email_subject_pt' id='travels_email_subject_pt' value="<?php echo esc_attr( get_post_meta( $object->ID, 'travels_email_subject_pt', true ) ); ?>" size="30" />
	</p>	

<?php }

function travels_email_subject_meta_box_en( $object, $box ) { ?>

	<?php wp_nonce_field( 'tr_email_subject_en', 'travel_email_subject_nonce_en' ); ?>
	<p>
		<label for='smashing-post-class'><?php _e( 'Asunto del correo', 'travels' ); ?></label>
		<br />
		<input class='widefat' type='text' name='travels_email_subject_en' id='travels_email_subject_en' value="<?php echo esc_attr( get_post_meta( $object->ID, 'travels_email_subject_en', true ) ); ?>" size="30" />
	</p>	

<?php }


function travels_booking_hotel_ID_meta_box( $object, $box ) { ?>

	<?php wp_nonce_field( 'travels_booking_hotel_ID', 'travels_booking_hotel_ID_nonce' ); ?>
	<p>
		<label for='smashing-post-class'><?php _e( 'ID del Hotel en Booking', 'travels' ); ?></label>
		<br />
		<input class='widefat' type='text' name='travels_booking_hotel_ID' id='travels_booking_hotel_ID' value="<?php echo esc_attr( get_post_meta( $object->ID, 'travels_booking_hotel_ID', true ) ); ?>" size="30" />
	</p>

<?php }

function travels_despegar_hotel_ID_meta_box( $object, $box ) { ?>

	<?php wp_nonce_field( 'travels_despegar_hotel_ID', 'travels_despegar_hotel_ID_nonce' ); ?>
	<p>
		<label for='smashing-post-class'><?php _e( 'ID del Hotel en Despegar', 'travels' ); ?></label>
		<br />
		<input class='widefat' type='text' name='travels_despegar_hotel_ID' id='travels_despegar_hotel_ID' value="<?php echo esc_attr( get_post_meta( $object->ID, 'travels_despegar_hotel_ID', true ) ); ?>" size="30" />
	</p>

<?php }

function travels_email_subject_CC_meta_box( $object, $box ) { ?>

	<?php wp_nonce_field( 'travels_email_subject_CC', 'travels_email_subject_CC_nonce' ); ?>
	<p>
		<label for='smashing-post-class'><?php _e( 'Enviar Copia a :', 'travels' ); ?></label>
		<br />
		<input class='widefat' type='text' name='travels_email_subject_CC' id='travels_email_subject_CC' value="<?php echo esc_attr( get_post_meta( $object->ID, 'travels_email_subject_CC', true ) ); ?>" size="30" />
	</p>

<?php }

function travels_provider_meta_box( $object, $box ) { ?>

	<?php wp_nonce_field( 'travels_provider', 'travels_provider_nonce' ); ?>
	<p>
		<label for='smashing-post-class'><?php _e( 'Seleccionar Proveedor', 'travels' ); ?></label>
		<br />
		<?=get_providers_list($object); ?>
	</p>

<?php }

/**
 *
 * Save custom meta boxes.
 *
 **/

function travels_save_post_meta_boxes( $post_id, $post ) {

	/* Verify the nonce before proceeding. */
	if ( !isset( $_POST['travel_email_contact_nonce'] ) || !wp_verify_nonce( $_POST['travel_email_contact_nonce'], 'tr_email_contact' ) )
		return $post_id;

	if ( !isset( $_POST['travel_form_title_nonce_es'] ) || !wp_verify_nonce( $_POST['travel_form_title_nonce_es'], 'tr_form_title_es' ) )
		return $post_id;

	if ( !isset( $_POST['travel_email_subject_nonce_es'] ) || !wp_verify_nonce( $_POST['travel_email_subject_nonce_es'], 'tr_email_subject_es' ) )
		return $post_id;

	if ( !isset( $_POST['travel_form_title_nonce_pt'] ) || !wp_verify_nonce( $_POST['travel_form_title_nonce_pt'], 'tr_form_title_pt' ) )
		return $post_id;

	if ( !isset( $_POST['travel_email_subject_nonce_pt'] ) || !wp_verify_nonce( $_POST['travel_email_subject_nonce_pt'], 'tr_email_subject_pt' ) )
		return $post_id;

	if ( !isset( $_POST['travel_form_title_nonce_en'] ) || !wp_verify_nonce( $_POST['travel_form_title_nonce_en'], 'tr_form_title_en' ) )
		return $post_id;

	if ( !isset( $_POST['travel_email_subject_nonce_en'] ) || !wp_verify_nonce( $_POST['travel_email_subject_nonce_en'], 'tr_email_subject_en' ) )
		return $post_id;

	if ( !isset( $_POST['travels_booking_hotel_ID_nonce'] ) || !wp_verify_nonce( $_POST['travels_booking_hotel_ID_nonce'], 'travels_booking_hotel_ID' ) )
		return $post_id;

	if ( !isset( $_POST['travels_despegar_hotel_ID_nonce'] ) || !wp_verify_nonce( $_POST['travels_despegar_hotel_ID_nonce'], 'travels_despegar_hotel_ID' ) )
		return $post_id;

	if ( !isset( $_POST['travels_email_subject_CC_nonce'] ) || !wp_verify_nonce( $_POST['travels_email_subject_CC_nonce'], 'travels_email_subject_CC' ) )
		return $post_id;

	if ( !isset( $_POST['travels_provider_nonce'] ) || !wp_verify_nonce( $_POST['travels_provider_nonce'], 'travels_provider' ) )
		return $post_id;

	/* Get the post type object. */
	$post_type = get_post_type_object( $post->post_type );

	/* Check if the current user has permission to edit the post. */
	if ( !current_user_can( $post_type->cap->edit_post, $post_id ) )
		return $post_id;

	/* Get the posted data */
	if (isset($_POST['travels_email_contact']))
		$email_contact_meta_value = $_POST['travels_email_contact'];

	if (isset($_POST['travels_form_title_es']))
		$form_title_meta_value_es = $_POST['travels_form_title_es'];

	if (isset($_POST['travels_email_subject_es']))
		$form_title_meta_value_es = $_POST['travels_email_subject_es'];

	if (isset($_POST['travels_form_title_pt']))
		$form_title_meta_value_pt = $_POST['travels_form_title_pt'];

	if (isset($_POST['travels_email_subject_pt']))
		$form_title_meta_value_pt = $_POST['travels_email_subject_pt'];

	if (isset($_POST['travels_form_title_en']))
		$form_title_meta_value_en = $_POST['travels_form_title_en'];

	if (isset($_POST['travels_email_subject_en']))
		$form_title_meta_value_en = $_POST['travels_email_subject_en'];

	if (isset($_POST['travels_booking_hotel_ID']))
		$bookingID = $_POST['travels_booking_hotel_ID'];

	if (isset($_POST['travels_despegar_hotel_ID']))
		$despegarID = $_POST['travels_despegar_hotel_ID'];

	if (isset($_POST['travels_provider_nonce']))
		$despegarID = $_POST['travels_provider_nonce'];

	/* Get the meta keys. */
	$email_contact_meta_key = 'travels_email_contact';
	$form_title_meta_key_es = 'travels_form_title_es';
	$email_subject_meta_key_es = 'travels_email_subject_es';

	$form_title_meta_key_pt = 'travels_form_title_pt';
	$email_subject_meta_key_pt = 'travels_email_subject_pt';

	$form_title_meta_key_en = 'travels_form_title_en';
	$email_subject_meta_key_en = 'travels_email_subject_en';

	$bookingID_meta_key = 'travels_booking_hotel_ID';
	$despegarID_meta_key = 'travels_despegar_hotel_ID';

	$travels_email_subject_CC_meta_key = 'travels_email_subject_CC';
	$travels_provider_meta_key = 'travels_provider';

	// If this is an autosave, our form has not been submitted,
	//     so we don't want to do anything.
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) 
		return $post_id;

	// Sanitize user input.
	$email_contact_meta_value = sanitize_text_field( $_POST['travels_email_contact'] );

	$form_title_meta_value_es = sanitize_text_field( $_POST['travels_form_title_es'] );
	$email_subject_meta_value_es = sanitize_text_field( $_POST['travels_email_subject_es'] );

	$form_title_meta_value_pt = sanitize_text_field( $_POST['travels_form_title_pt'] );
	$email_subject_meta_value_pt = sanitize_text_field( $_POST['travels_email_subject_pt'] );

	$form_title_meta_value_en = sanitize_text_field( $_POST['travels_form_title_en'] );
	$email_subject_meta_value_en = sanitize_text_field( $_POST['travels_email_subject_en'] );

	$bookingID = sanitize_text_field( $_POST['travels_booking_hotel_ID'] );
	$despegarID = sanitize_text_field( $_POST['travels_despegar_hotel_ID'] );

	$travels_email_subject_CC = sanitize_text_field( $_POST['travels_email_subject_CC'] );
	$travels_provider = sanitize_text_field( $_POST['travels_provider'] );

	// Update the meta field in the database.
	update_post_meta( $post_id, $email_contact_meta_key, $email_contact_meta_value );

	// Update the meta field in the database.
	update_post_meta( $post_id, $form_title_meta_key_es, $form_title_meta_value_es );
	// Update the meta field in the database.
	update_post_meta( $post_id, $email_subject_meta_key_es, $email_subject_meta_value_es );

	// Update the meta field in the database.
	update_post_meta( $post_id, $form_title_meta_key_pt, $form_title_meta_value_pt );
	// Update the meta field in the database.
	update_post_meta( $post_id, $email_subject_meta_key_pt, $email_subject_meta_value_pt );

	// Update the meta field in the database.
	update_post_meta( $post_id, $form_title_meta_key_en, $form_title_meta_value_en );
	// Update the meta field in the database.
	update_post_meta( $post_id, $email_subject_meta_key_en, $email_subject_meta_value_en );

	update_post_meta( $post_id, $bookingID_meta_key, $bookingID );

	update_post_meta( $post_id, $despegarID_meta_key, $despegarID );

	update_post_meta( $post_id, $travels_email_subject_CC_meta_key, $travels_email_subject_CC );

	update_post_meta( $post_id, $travels_provider_meta_key, $travels_provider );

}
?>