<?php

function getsinglebottomJs() {

	if (is_home())
		return;

	$url = $GLOBALS['sites'][SC]['plugins']['despegar']['url'];
	$destiny = $GLOBALS['sites'][SC]['plugins']['despegar']['destiny'];
	$country = $GLOBALS['sites'][SC]['plugins']['despegar']['country'];
	$affiliate = $GLOBALS['sites'][SC]['plugins']['despegar']['affiliate'];
	$autocomplete = $GLOBALS['sites'][SC]['plugins']['despegar']['autocomplete'];

	$script = <<<JS
	<script>
		$( document ).ready(function() {
			//Define datepicker rules
			$(function() {
				$( '#start-single-bottom' ).datepicker({
					defaultDate: '+1d',
					minDate:'+1d',
					changeMonth: true,
					numberOfMonths: 2,
					onClose: function( selectedDate ) {
						$( '#end-single-bottom' ).datepicker( 'option', 'minDate', selectedDate );
					},
					onSelect: function(dateText, inst) {}
				});
				$( '#end-single-bottom' ).datepicker({
					defaultDate: '+1d',
					changeMonth: true,
					minDate:'+1d',
					numberOfMonths: 2,
					onClose: function( selectedDate ) {
						$( '#start-single-bottom' ).datepicker( 'option', 'maxDate', selectedDate );
					},
					onSelect: function(dateText, inst) {}
				});
			});

			$('#single-bottom-submit').on('click', function() {

				if ($("#hotelID").val() == '') {
					var location = '{$url}' + '{$destiny}' + '/' + getDateFrom() + '/' + getDateTo() + '/2/&country={$country}&affiliate=' + '{$affiliate}';
				} else {
					var location = '{$url}' + 'Details' + '/' + $("#hotelID").val() + '/' + getDateFrom() + '/' + getDateTo() + '/2/&country={$country}&affiliate=' + '{$affiliate}';
				}
				window.open(location);

			});

			function getDateTo() {

				if ($('#end-single-bottom').datepicker('getDate') == null) {
					var dateTo = new Date();
					dayTo = dateTo.getDate() + 2;
				} else {
					var dateTo = $('#end-single-bottom').datepicker('getDate');
					dayTo = dateTo.getDate();
				}

				dayTo = is_one_digit(dayTo);
				monthTo = dateTo.getMonth() + 1,
				monthTo = is_one_digit(monthTo);
				yearTo =  dateTo.getFullYear();
				dateTo = yearTo + '-' + monthTo + '-' + dayTo;
				return dateTo;
			}

			function getDateFrom() {

				if ($('#start-single-bottom').datepicker('getDate') == null) {
					var dateFrom = new Date();
				} else {
					var dateFrom = $('#start-single-bottom').datepicker('getDate');
				}
				
				dayFrom  = dateFrom.getDate(),
				dayFrom = is_one_digit(dayFrom);
				monthFrom = dateFrom.getMonth() + 1,
				monthFrom = is_one_digit(monthFrom);
				yearFrom =  dateFrom.getFullYear();
				dateFrom = yearFrom + '-' + monthFrom + '-' + dayFrom;
				return dateFrom;
			}

			function is_one_digit(date) {

				if (date.toString().length == '1') {
					date = '0' + date;
					return date; 
				}
				return date;
			}
		});
	</script>
JS;

	echo $script;
}

?>