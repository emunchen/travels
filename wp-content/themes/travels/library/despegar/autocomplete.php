<?php

	// Point to where you downloaded the phar
	include('../httpful/httpful.phar');

	/** Main subscriber logic. **/

	require_once(__DIR__ . '/../../../../../wp-load.php');

	$query = beforeRequest();

	//debug(urlencode($query)); die;

	if ( false === ( $response = get_transient( $query . '_despegar_query' ) ) ) :

		$response = autocomplete($query);
		set_transient( $query . '_despegar_query', $response, 60*60*5000*5000*50000000 );

	endif;

	if (empty($response->body)) {
		$no_results = array('No hay Resultados');
		echo json_encode($no_results);
		exit();
	}

	$hotels = array();

	$i = 0;

	foreach($response->body as $item) {
		$hotels[$i]['value'] = $item->description;
		$hotels[$i]['label'] = $item->description;
		$hotels[$i]['id'] = $item->hotel_id;
		$i++;
	}
	echo json_encode($hotels);
	exit();

	function autocomplete($query) {

		$query = str_replace(' ', '%20', $query);
		
		// And you're ready to go!
		$response = \Httpful\Request::get('https://api.despegar.com/v3/autocomplete?query=' . $query . '&product=HOTELS&locale=es_AR&hotel_result=10')
			->addHeader('X-ApiKey', 'c3fb2c48aaa2474aae206b4bf47d6507')
			->send();
		return $response;
	}


/**
 * Do some actions before send request to class.
 *
 * @param void
 * @return array $request
 */

	function beforeRequest() {

		/** Terminates execution of the script if Method is not GET. **/
		if ($_SERVER['REQUEST_METHOD'] != 'GET')
			exit(1);

		$data = $_GET['query'];
		return $data;
	}