<?php

/**
 * Despegar Category Search Form.
 *
 **/

?>
	<h4><?php _e( 'Search Hotels', 'travels' ) ?></h4>
	<div>
		<input type='hidden' id='hotelID' name='hotelID' value='' />
		<div class="row clearfix">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
				<div class="form-group">
					<label for="from"><?php _e( 'Check In', 'travels' ); ?></label>
					<div class="input-group input-group-md">
						<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
						<input type="text" name="checkin" id="start-category-search" class="form-control" placeholder="dd/mm/aaaa">
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
				<div class="form-group">
					<label for="from"><?php _e( 'Check Out', 'travels' ); ?></label>
					<div class="input-group input-group-md">
						<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
						<input type="text" name="checkout" id="end-category-search" class="form-control" placeholder="dd/mm/aaaa">
					</div>
				</div>
			</div>
		</div>
		<div class="row clearfix">
			<div class="col-sm-10">
				<div class="form-group booking-home-submit">
					<button id="category-search-submit" type="submit" class="btn btn-warning btn-lg"><?php _e( 'Search', 'travels' ); ?></button>
				</div>
			</div>
		</div>
	</div>