<?php

/**
 * Single Bottom Booking Form Template.
 *
 **/
	global $post;

	if (!get_post_meta( $post->ID, 'travels_despegar_hotel_ID', true ) )
		return;
?>
<div class='booking-single-bottom-form form-inline'>
	<h4><?php _e( 'See Price', 'travels' ) ?></h4>
	<input type='hidden' id='hotelID' name='hotelID' value='<?=get_post_meta( $post->ID, 'travels_despegar_hotel_ID', true ); ?>' />
	<div class="row clearfix">
		<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
			<div class="form-group">
				<label for="from"><?php _e( 'Check In', 'travels' ); ?></label>
				<div class="input-group input-group-md">
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
					<input type="text" name="checkin" id="start-single-bottom" class="form-control validate[required] from" placeholder="dd/mm/aaaa">
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
			<div class="form-group">
				<label for="from"><?php _e( 'Check Out', 'travels' ); ?></label>
				<div class="input-group input-group-md">
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>	
					<input type="text" name="checkout" id="end-single-bottom" class="form-control validate[required] to" placeholder="dd/mm/aaaa">
				</div>
			</div>
		</div>
		<div class="form-group booking-home-submit" style='margin-left:20px;'>
			<button id="single-bottom-submit" type="submit" class="btn btn-warning btn-lg"><?php _e( 'Book', 'travels' ); ?></button>
		</div>
	</div>
</div>