<?php

/**
 * Despegar Home Search Form.
 *
 **/

	if (!is_despegar_enabled())
		return;

?>
	<div style="margin-top: 58px;">
		<input type='hidden' id='hotelID' name='hotelID' value='' />
		<div class="row clearfix">
			<div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
				<div class="form-group">
					<label for="from"><?php _e( 'Check In', 'travels' ); ?></label>
					<div class="input-group input-group-md">
						<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
						<input type="text" name="checkin" id="start-home-search" class="form-control validate[required] from" placeholder="dd/mm/aaaa">
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
				<div class="form-group">
					<label for="from"><?php _e( 'Check Out', 'travels' ); ?></label>
					<div class="input-group input-group-md">
						<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
						<input type="text" name="checkout" id="end-home-search" class="form-control validate[required] to" placeholder="dd/mm/aaaa">
					</div>
				</div>
			</div>
		</div>
		<div class="row clearfix">
			<div class="col-sm-10">
				<div class="form-group booking-home-submit">
					<button id="home-search-submit" type="submit" class="btn btn-warning btn-lg"><?php _e( 'Search', 'travels' ); ?></button>
				</div>
			</div>
		</div>
	</div>