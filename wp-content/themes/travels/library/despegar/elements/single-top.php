<?php

/**
 * Despegar Single Top Form.
 *
 **/

	global $post;

	if (!get_post_meta( $post->ID, 'travels_despegar_hotel_ID', true ) )
		return;
?>
	<div>
		<h4><?php _e( 'See Price', 'travels' ) ?></h4>
		<input type='hidden' id='hotelID' name='hotelID' value='<?=get_post_meta( $post->ID, 'travels_despegar_hotel_ID', true ); ?>' />
		<div class="row clearfix">
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
				<div class="form-group">
					<label for="from"><?php _e( 'Check In', 'travels' ); ?></label>
					<div class="input-group input-group-md">
						<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
						<input type="text" name="checkin" id="start-single-top" class="form-control validate[required] from" placeholder="dd/mm/aaaa">
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
				<div class="form-group">
					<label for="from"><?php _e( 'Check Out', 'travels' ); ?></label>
					<div class="input-group input-group-md">
						<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
						<input type="text" name="checkout" id="end-single-top" class="form-control validate[required] to" placeholder="dd/mm/aaaa">
					</div>
				</div>
			</div>
		</div>
		<div class="row clearfix">
			<div class="col-sm-10">
				<div class="form-group booking-home-submit">
					<button id="single-top-submit" type="submit" class="btn btn-warning btn-lg"><?php _e( 'Search', 'travels' ); ?></button>
				</div>
			</div>
		</div>
	</div>