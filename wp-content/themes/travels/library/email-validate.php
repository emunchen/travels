<?php

require_once('../../../../wp-load.php');
require_once( dirname(__FILE__) . '/api-cumbres/src/Lead/Lead.php');

$settings = array(
	'url'		=> API_URL,
	'version'	=> API_VERSION,
	'appID'		=> API_APPID,
	'secret'	=> API_SECRET
);

if (isset($_GET['email'])) {
	if (empty($_GET['email'])) {
		status_header(404);
		return;
	}
	$api = new Lead($settings);
	$response = $api->validateEmail($_GET['email']);
	if ($response->code != 200) {
		status_header(404);
	}
	header('Content-Type: application/json');
	echo json_encode($response->body);
}

if (isset($_GET['phone'])) {
	$api = new Lead($settings);
	$response = $api->validatePhone($_GET['phone']);
	if ($response->code != 200) {
		status_header(404);
	}
	header('Content-Type: application/json');
	echo json_encode($response->body);
}
