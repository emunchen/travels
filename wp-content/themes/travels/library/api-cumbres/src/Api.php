<?php

include('httpful.phar');

/**
 * Api Contector.
 */

class Api {

/**
 * Default settings.
 */
	public $settings = array(
		'url'		=> '',
		'version'	=> '',
		'appID'		=> '',
		'secret'	=> ''
	);

/**
 * Constructor.
 */
	public function __construct(array $settings = array()) {
		if (!empty($settings)) {
			$this->settings = array_merge($this->settings, $settings);
			$data = array(
				'appID'		=> $this->settings['appID'],
				'secret'	=> $this->settings['secret']
			);
			$response = $this->post('auth', $data, false);
			if ($response->code != 200) {
				return;
			}
			if (empty($response->body['access_token'])) {
				return;
			}
			$this->settings['bearer'] = $response->body['access_token'];
		}
	}

/**
 * Issue a GET request.
 */
	public function get($uri = null) {

		$uri = $this->__buildUri($uri, true);
		$response = \Httpful\Request::get($uri)
		->Authorization('Bearer ' . $this->settings['bearer'])
		->send();
		return $this->__parseResponse($response);
	}

/**
 * Issue a POST request.
 */
	public function post($uri = null, $data = null, $version = true) {

		$uri = $this->__buildUri($uri, true, $version);
		$params = $this->__buildParams();
		$response = \Httpful\Request::post($uri)
		->Authorization('Bearer ' . $this->settings['bearer'])
		->sendsJson()
		->body($data)
		->send();
		return $this->__parseResponse($response);
	}

/**
 * Issue a PUT request.
 */
	public function put($uri = null, $data = null) {

		$uri = $this->__buildUri($uri, true);
		$params = $this->__buildParams();
		$response = \Httpful\Request::put($uri)
		->Authorization('Bearer ' . $this->settings['bearer'])
		->sendsJson()
		->body($data)
		->send();
		return $this->__parseResponse($response);
	}

/**
 * Construct a valid URI.
 *
 * @param array $uri
 */

	private function __buildUri($uri = null, $key = false, $version = true) {
		$params = array('url', 'version');
		foreach ($params as $param) {
			if (!array_key_exists($param, $this->settings)) {
				throw new InternalErrorException();
			}
		}

		if ($version) {
			$fullUri = $this->settings['url'] . '/' . $this->settings['version'] . '/';
		} else {
			$fullUri = $this->settings['url'] . '/';
		}

		if (!empty($uri)) {
			$fullUri .= $uri;
		}

		return $fullUri;
	}

/**
 * Parse params to send through socket
 *
 * @param array $params.
 */
	private function __buildParams($params = array()) {

		$_params = array();

		if(!empty($params)) {
			$_params = array_merge($_params, $params);
		}

		return $_params;
	}
/**
 * Parse a Httpful::response.
 *
 * @param $response object.
 */
	private function __parseResponse($response) {
		$response->code = (int)$response->code;
		$response->body = json_decode(json_encode($response->body), true);
		return $response;
	}
}
?>