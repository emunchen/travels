<?php

require_once( dirname(__FILE__) . '/../Api.php');

class Lead extends Api {

/**
 * Add Subscriber data.
 *
 * @return array Subscriber
 */
	public function addSubscriber($data = array()) {
		if (empty($data)) {
			throw new Exception('Empty data Request');
		}

		$response = $this->post('subscribers', $data);
		return $response;
	}

/**
 * Get Subscriber data.
 *
 * @return array Subscriber
 */
	public function getSubscriber($id = null) {
		if (empty($id)) {
			throw new Exception('Empty id Request');
		}

		$response = $this->get('subscribers/' . $id);
		return $response;
	}

/**
 * Add Lead data.
 *
 * @return array Lead
 */
	public function addLead($data = array()) {
		if (empty($data)) {
			throw new Exception('Empty data Request');
		}

		$response = $this->post('leads', $data);
		return $response;
	}

/**
 * Validate email
 *
 * @return string $email
 */
	public function validateEmail($email = '') {

		if (empty($email)) {
			throw new Exception('Empty email');
		}

		$response = $this->get('subscribers/validate_email/' . $email);
		return $response;
	}

/**
 * Validate phone
 *
 * @return string $phone
 */
	public function validatePhone($phone = '') {

		if (empty($phone)) {
			throw new Exception('Empty phone');
		}

		$response = $this->get('subscribers/validate_phone/+' . $phone);
		return $response;
	}

/**
 * Retrieve a list of public employees types.
 *
 * @return array
 */
	public function getPublicEmployees() {

		$response = $this->get('public_employees');
		$public_employees = $response->body['public_employees'];

		if ($response->code != 200) {
			throw new Exception('Error Processing Request');
		}

		$_public_employees = array();
		foreach ($public_employees as $p) {
			$_public_employees[$p['id']] = $p['name'];
		}

		krsort($_public_employees);
		return $_public_employees;
	}

/**
 * Retrieve a list of employment contract type.
 *
 * @return array
 */
	public function getContractTypes() {

		$response = $this->get('contract_types');
		$contract_types = $response->body['contract_types'];

		if ($response->code != 200) {
			throw new Exception('Error Processing Request');
		}

		$_contract_types = array();
		foreach ($contract_types as $c) {
			$_contract_types[$c['id']] = $c['name'];
		}

		return $_contract_types;
	}

/**
 * Retrieve a list of all branches offices by state
 *
 * @return array
 */
	public function getBranchesOfficesByState($providerId) {
		if (empty($providerId)) {
			throw new BadRequestException();
		}

		$response = $this->get('branches/state/' . $providerId);
		if ($response->code !== 200) {
			throw new InternalErrorException();
		}

		$branchOffices = $response->body;
		return $this->__group_by($branchOffices['branches'], 'city.state_id');
	}

/**
 * Retrieve a list of all branches offices grouped by city.
 *
 * @param  array $branchesByState Array of branches by state
 * @return array
 */
	public function formatBranchesByCity($providerId, $branchesByState = array()) {
		if (empty($providerId)) {
			throw new BadRequestException();
		}
		if (empty($branchesByState)) {
			$branchesByState = $this->getBranchesOfficesByState($providerId);
		}
		if (empty($branchesByState)) {
			return array();
		}

		$branchesByCity = array();
		foreach ($branchesByState as $stateId => $branches) {
			foreach ($branches as $branch) {
				$branchesByCity[$branch['city_id']][] = $branch;
			}
		}

		return $branchesByCity;
	}

/**
 * Retrieve a list of cities available for branches based on braches by state.
 *
 * @param  array $branchesByState Array of branches by state
 * @return array
 */
	public function getAvailableCities($providerId, $branchesByState = array()) {
		if (empty($providerId)) {
			throw new BadRequestException();
		}
		if (empty($branchesByState)) {
			$branchesByState = $this->getBranchesOfficesByState($providerId);
		}
		if (empty($branchesByState)) {
			return array();
		}

		$cities = array();
		foreach ($branchesByState as $stateId => $branches) {
			foreach ($branches as $branch) {
				$cities[$branch['city']['state_id']][$branch['city']['id']] = $branch['city'];
			}
		}
		return $cities;
	}

/**
 * Retrive providers phone according to provider scope.
 *
 * @param  int $providerId The id of the provider.
 * @return string
 */
	public function getScopes($providerId) {
		if (empty($providerId)) {
			return array();
		}
		$response = $this->get('scopes/_slug/desktop');
		if (!empty($response->body['settings'])) {
			$scope = unserialize($response->body['settings']);
			if (!empty($scope['phone'])) {
				return $this->getProvider($providerId, 'phone');
			}
		}
	}

/**
 * Retrieve a set of all branches offices for a provider.
 *
 * @param  int $providerId The id of the provider
 * @return array
 */
	public function getBranchesOffices($providerId) {
		if (empty($providerId)) {
			throw new InternalErrorException();
		}

		$response = $this->get('branches/provider/' . $providerId);
		if ($response->code !== 200) {
			throw new InternalErrorException();
		}

		$branchOffices = $response->body;
		return $branchOffices['branches'];
	}

/**
 * Get the subscriber name.
 *
 * @return string
 */
	public function getSubscriberName() {
		if (!isset($_SESSION['settings']))
			return;

		$settings = $_SESSION['settings'];
		return $settings['suscriber']['name'];
	}

/**
 * Get the subscriber state.
 *
 * @return string
 */
	public function getSubscriberState() {
		if (!isset($_SESSION['settings']))
			return;

		$settings = $_SESSION['settings'];
		return $settings['state_id'];
	}

/**
 * Get the lead id.
 *
 * @return string
 */
	public function getLeadId() {
		if (!isset($_SESSION['settings']))
			return;

		$settings = $_SESSION['settings'];
		return $settings['id'];
	}

/**
 * Get a provider with all its fields or one specified.
 *
 * @param int $id The id of the provider
 * @param string $field A specific field to retrieve
 * @return mixed
 */
	public function getProvider($id, $field = '') {
		if (empty($id)) {
			return;
		}

		$response = $this->get('providers/' . $id);
		if ($response->code !== 200) {
			return array();
		}
		$provider = $response->body;
		if (!empty($field)) {
			return $provider[$field];
		}
		return $provider;
	}

/**
 * Get all providers list.
 *
 * @return mixed
 */
	public function getProviders() {

		$response = $this->get('providers');
		if ($response->code !== 200) {
			return array();
		}
		$providers = $response->body;
		return $providers;
	}

/**
 * Get a provider with all its fields searching by Slug.
 *
 * @param string $slug A provider slug.
 * @return mixed
 */

	public function getProviderBySlug($slug = '') {
		if (empty($slug)) {
			return;
		}
		$response = $this->get('providers/slug/' . $slug);
		if ($response->code !== 200) {
			return array();
		}
		$provider = $response->body;
		return $provider;
	}

/**
 * Utility to group an array by a field and use it as key.
 *
 * @param  array $group Array of elements
 * @param  string $index Field
 * @return array
 */
	private function __group_by($group = array(), $index = null) {
		$result = array();
		if (strpos($index, '.')) {
			$index = explode('.', $index);
		}
		foreach ($group as $data) {
			$id =  (is_array($index)) ? $data[$index[0]][$index[1]] : $data[$index];
			if (isset($result[$id])) {
				$result[$id][] = $data;
			} else {
				$result[$id] = array($data);
			}
		}
		return $result;
	}
}
?>