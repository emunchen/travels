<?php

function gethomeJs() {

	if (!is_home())
		return;

	$script = <<<JS
	<script>
		$( document ).ready(function() {
			//Define datepicker rules
			$(function() {
				$( '#home-from' ).datepicker({
					defaultDate: '+1d',
					minDate:'+1d',
					changeMonth: true,
					numberOfMonths: 2,
					onClose: function( selectedDate ) {
						$( '#home-to' ).datepicker( 'option', 'minDate', selectedDate );
					},
					onSelect: function(dateText, inst) {
						var dateFrom = $('#home-from').datepicker('getDate'),
						dayFrom  = dateFrom.getDate(),
						monthFrom = dateFrom.getMonth() + 1,
						yearFrom =  dateFrom.getFullYear();
						$( '#checkin_monthday' ).val(dayFrom);
						$( '#checkin_year_month' ).val(yearFrom + '-' + monthFrom);
					}
				});
				$( '#home-to' ).datepicker({
					defaultDate: '+1d',
					changeMonth: true,
					minDate:'+1d',
					numberOfMonths: 2,
					onClose: function( selectedDate ) {
						$( '#home-from' ).datepicker( 'option', 'maxDate', selectedDate );
					},
					onSelect: function(dateText, inst) { 
						var dateTo = $( '#home-to' ).datepicker('getDate'),
						dayTo  = dateTo.getDate(),  
						monthTo = dateTo.getMonth() + 1,              
						yearTo =  dateTo.getFullYear();
						$( '#checkout_monthday' ).val(dayTo);
						$( '#checkout_year_month' ).val(yearTo + '-' + monthTo);
					}
				});
			});

		});
	</script>
JS;

	echo $script;
}

?>