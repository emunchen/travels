<?php

function getsinglebottomJs() {

	if (is_home())
		return;

	$script = <<<JS
	<script>
		$( document ).ready(function() {
			//Define datepicker rules
			$(function() {
				$( '#from-bottom' ).datepicker({
					defaultDate: '+1d',
					minDate:'+1d',
					changeMonth: true,
					numberOfMonths: 2,
					onClose: function( selectedDate ) {
						$( '#to-bottom' ).datepicker( 'option', 'minDate', selectedDate );
					},
					onSelect: function(dateText, inst) {
						var dateFrom_single = $('#from-bottom').datepicker('getDate'),
						dayFrom_single  = dateFrom_single.getDate(),
						monthFrom_single = dateFrom_single.getMonth() + 1,
						yearFrom_single =  dateFrom_single.getFullYear();
						$('#checkin_monthday_single').val(dayFrom_single);
						$('#checkin_year_month_single').val(yearFrom_single + '-' + monthFrom_single);
					}
				});
				$( '#to-bottom' ).datepicker({
					defaultDate: '+1d',
					changeMonth: true,
					minDate:'+1d',
					numberOfMonths: 2,
					onClose: function( selectedDate ) {
						$( '#from-bottom' ).datepicker( 'option', 'maxDate', selectedDate );
					},
					onSelect: function(dateText, inst) { 
						var dateTo_single = $('#to-bottom').datepicker('getDate'),
						dayTo_single  = dateTo_single.getDate(),  
						monthTo_single = dateTo_single.getMonth() + 1,              
						yearTo_single =  dateTo_single.getFullYear();
						$('#checkout_monthday_single').val(dayTo_single);
						$('#checkout_year_month_single').val(yearTo_single + '-' + monthTo_single);
					}
				});
			});
		});
	</script>
JS;

	echo $script;
}

?>