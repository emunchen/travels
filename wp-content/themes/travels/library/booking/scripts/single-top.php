<?php

function getsingletopJs() {

	if (is_home())
		return;

	$script = <<<JS
	<script>
		$( document ).ready(function() {
			//Define datepicker rules
			$(function() {
				$( '#from-top' ).datepicker({
					defaultDate: '+1d',
					minDate:'+1d',
					changeMonth: true,
					numberOfMonths: 2,
					onClose: function( selectedDate ) {
						$( '#to-top' ).datepicker( 'option', 'minDate', selectedDate );
					},
					onSelect: function(dateText, inst) {
						var dateFrom_single_top = $('#from-top').datepicker('getDate'),
						dayFrom_single_top  = dateFrom_single_top.getDate(),
						monthFrom_single_top = dateFrom_single_top.getMonth() + 1,
						yearFrom_single_top =  dateFrom_single_top.getFullYear();
						$('#checkin_monthday_single_top').val(dayFrom_single_top);
						$('#checkin_year_month_single_top').val(yearFrom_single_top + '-' + monthFrom_single_top);
					}
				});
				$( '#to-top' ).datepicker({
					defaultDate: '+1d',
					changeMonth: true,
					minDate:'+1d',
					numberOfMonths: 2,
					onClose: function( selectedDate ) {
						$( '#from-top' ).datepicker( 'option', 'maxDate', selectedDate );
					},
					onSelect: function(dateText, inst) { 
						var dateTo_single_top = $('#to-top').datepicker('getDate'),
						dayTo_single_top  = dateTo_single_top.getDate(),  
						monthTo_single_top = dateTo_single_top.getMonth() + 1,              
						yearTo_single_top =  dateTo_single_top.getFullYear();
						$('#checkout_monthday_single_top').val(dayTo_single_top);
						$('#checkout_year_month_single_top').val(yearTo_single_top + '-' + monthTo_single_top);
					}
				});
			});
		});
	</script>
JS;

	echo $script;
}

?>