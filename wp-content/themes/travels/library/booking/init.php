<?php
/**
 * Init Booking plugin.
 *
 */

	require_once(__DIR__ . '/../../../../../wp-load.php');

	require('scripts/single-top.php');
	require('scripts/single-bottom.php');
	require('scripts/category-search.php');
	require('scripts/home-search.php');

/**
 *
 *	Define form dispatcher. Support Booking Forms.
 *
 **/

	function bookingdispatcherForm($type = 'home') {

		$forms = array(
			'home'			=> 'home-search.php',
			'category'		=> 'category-search.php',
			'single-top'	=> 'single-top.php',
			'single-bottom'	=> 'single-bottom.php'
		);

		$formPath;
		$formPath = dirname(__FILE__) . '/elements/' . $forms[$type];
		if (!empty($forms[$type])) {
			require($formPath);
		}

		$type = str_replace('-', '', $type);
		add_action('wp_footer', 'get' . $type . 'Js', 100);
	}

/**
 *
 *	Check if plugin is enabled.
 *
 **/

	function is_booking_enabled() {
		if (isset($GLOBALS['sites'][SC]['plugins']['booking']['enabled']))
			$enabled = $GLOBALS['sites'][SC]['plugins']['booking']['enabled'];

		if (!$enabled)
			return false;

		return true;
	}
?>