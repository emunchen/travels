<?php

/**
 * Single Bottom Booking Form Template.
 *
 **/
	global $post;
	$bookingConf = $GLOBALS['sites'][SC]['plugins']['booking'];

	if (!get_post_meta( $post->ID, 'travels_booking_hotel_ID', true ))
		return;
?>
<div class='booking-single-bottom-form'>
	<h4><?php _e( 'See Price and Availability', 'travels' ) ?></h4>
	<form action='<?=get_post_meta( $post->ID, 'travels_booking_hotel_ID', true ); ?>' type='GET' id='booking-form' class='form-inline' target='_blank'>
		<input type='hidden' id='checkin_monthday_single' name='checkin_monthday' value='<?=date("d", strtotime("tomorrow"));?>' />
		<input type='hidden' id='checkin_year_month_single' name='checkin_year_month' value='<?=date("Y-m", strtotime("tomorrow"));?>' />
		<input type='hidden' id='checkout_monthday_single' name='checkout_monthday' value='<?=date("d", strtotime("+ 2 days"));?>' />
		<input type='hidden' id='checkout_year_month_single' name='checkout_year_month' value='<?=date("Y-m", strtotime("+ 2 days"));?>' />
		<input type='hidden' name='error_url' value='<?=$bookingConf['errorurl'];?><?=$bookingConf['aid']; ?>' />
		<input type='hidden' name='si' value='<?=$bookingConf['si']; ?>' />
		<input type='hidden' name='label' value='wp-searchbox-widget-<?=$bookingConf['aid']; ?>' />
		<input type='hidden' name='aid' value='<?=$bookingConf['aid']; ?>' />
		<input type='hidden' name='dest_id' value='<?=$bookingConf['dest_id']?>' />
		<input type='hidden' name='utm_campaign' value='search_box' />
		<input type='hidden' name='utm_medium' value='sp' />
		<input type='hidden' name='utm_term' value='wp-searchbox-widget-<?=$bookingConf['aid']; ?>' />
		<input type='hidden' name='do_availability_check' value='on' />
		<input type='hidden' id='site_single' value='<?=$GLOBALS['sites'][SC]['site_display'];?>' />
			<div class="form-group">
				<label for="from"><?php _e( 'Check In', 'travels' ); ?></label>
				<input type="text" name="checkin" id="from-bottom" class="form-control" placeholder="dd/mm/aaaa">
			</div>
			<div class="form-group">
				<label for="from"><?php _e( 'Check Out', 'travels' ); ?></label>
				<input type="text" name="checkout" id="to-bottom" class="form-control" placeholder="dd/mm/aaaa">
			</div>
			<div class="form-group booking-home-submit" style='margin-left:20px;'>
				<button id="booking-hotel-bottom" type="submit" class="btn btn-warning btn-lg"><?php _e( 'Book', 'travels' ); ?></button>
			</div>
	</form>
</div>