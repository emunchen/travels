<?php

/**
 * Register Menus
 *
 */

register_nav_menu( 'primary', 'Header Menu' );
register_nav_menu( 'secondary', 'Footer Menu' );

/**
 * Init Footer Menu
 *
 */

function tr_footer_init() {

	$bottom_footer = $GLOBALS['sites'][SC]['widgets']['bottom_footer'];

	if ($bottom_footer) {
		wp_nav_menu( array(
			'theme_location'	=> 'secondary',
			'menu'				=> 'Footer Menu',
			'container'			=> 'div',
			'container_class'	=> 'navbar-collapse collapse',
			'menu_class'		=> 'nav navbar-nav tr-navbar-nav-footer'
		));
	}		
}

/**
 * Create custom log.
 *
 */

if (!function_exists('write_log')) {
	function write_log($log) {
		if ( is_array($log ) || is_object($log)) {
			error_log(print_r($log, true));
		} else {
			error_log($log);
		}
	}
}

/**
 * Register our sidebars and widgetized areas.
 *
 */

function tr_widgets_init() {

	register_sidebar( array(
		'name' => 'Home Center',
		'id' => 'home_center_1',
		'before_widget' => '<div>',
		'after_widget' => '</div>',
		'before_title' => '<h2 class="tr-recomended-title">',
		'after_title' => '</h2>'
		)
	);

	register_sidebar( array(
		'name' => 'Home Middle',
		'id' => 'home_middle_1',
		'before_widget' => '<div>',
		'after_widget' => '</div>',
		'before_title' => '<h2 class="tr-featured-title">',
		'after_title' => '</h2>'
		)
	);

	register_sidebar( array(
		'name' => 'Footer Top',
		'id' => 'footer_top',
		'before_widget' => '<div class="footer-widget">',
		'after_widget' => '</div>',
		'before_title' => '<h4>',
		'after_title' => '</h4>'
		)
	);

	register_sidebar( array(
		'name' => 'Home Right Sidebar',
		'id' => 'home',
		'before_widget' => '<div>',
		'after_widget' => '</div>',
		'before_title' => '<h4>',
		'after_title' => '</h4>'
		)
	);

	register_sidebar( array(
		'name' => 'Category Left Sidebar',
		'id' => 'category',
		'before_widget' => '<div>',
		'after_widget' => '</div>',
		'before_title' => '<h4>',
		'after_title' => '</h4>'
		)
	);
}

add_action( 'widgets_init', 'tr_widgets_init' );

/*
 *
 * Include widgets
 *
 */

require_once('widgets/recommended-posts.php');
require_once('widgets/featured-posts.php');
require_once('widgets/top-list.php');
require_once('widgets/upload-banners.php');
require_once('widgets/custom_text_widget.php');

/*
 * Remove Unnecesary tags.
 */

function remove_header_info() {
	remove_action('wp_head', 'feed_links', 2);  
	remove_action('wp_head', 'feed_links_extra', 3);
	remove_action('wp_head', 'wp_generator');
	remove_action('wp_head', 'wlwmanifest_link');
	remove_action('wp_head', 'rsd_link');
	remove_action('wp_head', 'wp_shortlink_wp_head');
}

add_action('init', 'remove_header_info');

/**
 * Control Excerpt Length using Filters.
 *
 */

function custom_excerpt_length( $length ) {
	return 25;
}

add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

/**
 * Enable post thumbnails
 *
 */

add_theme_support( 'post-thumbnails' );

/**
 * Add new post thumbnail sizes
 *
 */

add_image_size( 'travel-widget-thumbnail', 324, 190, true );
add_image_size( 'travel-featured-thumbnail', 330, 200, true );
add_image_size( 'travel-gallery-thumbnail', 665, 352, true );
add_image_size( 'travel-banner-widget-thumbnail', 350, 90, true );
add_image_size( 'travel-category-thumbnail', 355, 200, true );
add_image_size( 'travel-single-gallery', 801, 563, true );

/**
 * Get featured images to display on gallery
 *
 */

function travel_get_post_gellery() {

	$cat = '';
	$cat = $GLOBALS['sites'][SC]['featured_category'];

	if(empty($cat))
		return;

	//get by  id: 6 (Destacados)
	$args = array( 'posts_per_page' => 10, 'category' => $cat );
	$posts = get_posts( $args );

	if (empty($posts))
		return;

	foreach ( $posts as $post ) :
		echo '<a href="' . get_permalink($post->ID) . '"><li>'. get_the_post_thumbnail($post->ID, 'travel-gallery-thumbnail', array('title' => __($post->post_title ), 'class' => 'tr-featured-img-fix')); '</li></a>';
	endforeach;
}

/**
 *
 * Register scripts
 *
 */

function travel_register_load_script() {

	wp_register_script( 'travel-jquery', get_stylesheet_directory_uri() . '/js/vendor/jquery-1.10.1.min.js', array(), false, false);
	wp_register_script( 'travel-bjqs-core', get_stylesheet_directory_uri() . '/js/vendor/bjqs-1.3.min.js', array( 'travel-jquery' ), false, true );
	wp_register_script( 'travel-bjqs', get_stylesheet_directory_uri() . '/js/vendor/travel-bjqs.js', array( 'travel-jquery' ), false, true);
	wp_register_script( 'travel-bjqs-single', get_stylesheet_directory_uri() . '/js/vendor/travel-bjqs-single.js', array( 'travel-jquery' ), false, true);
	wp_register_script( 'travel-bootstrap', get_stylesheet_directory_uri() . '/js/vendor/bootstrap.min.js', array( 'travel-jquery' ), false, true);
	wp_register_script( 'travel-main', get_stylesheet_directory_uri() . '/js/main.js', array( 'travel-jquery', 'travel-validation-engine' ), false, true);
	wp_register_script( 'travel-jquery-ui', get_stylesheet_directory_uri() . '/js/vendor/jquery-ui-1.10.4.custom.min.js', array( 'travel-jquery' ), false, true);
	wp_register_script( 'jquery.ui.datepicker-es', get_stylesheet_directory_uri() . '/js/vendor/jquery.ui.datepicker-es.js', array( 'travel-jquery', 'travel-jquery-ui' ), false, true);
	wp_register_script( 'jquery.ui.datepicker-en', get_stylesheet_directory_uri() . '/js/vendor/jquery.ui.datepicker-en.js', array( 'travel-jquery', 'travel-jquery-ui' ), false, true);
	wp_register_script( 'jquery.ui.datepicker-pt', get_stylesheet_directory_uri() . '/js/vendor/jquery.ui.datepicker-pt.js', array( 'travel-jquery', 'travel-jquery-ui' ), false, true);
	wp_register_script( 'jquery.validation-engine-en', get_stylesheet_directory_uri() . '/js/vendor/jquery.validationEngine-en.js', array( 'travel-jquery', 'travel-validation-engine' ), false, true);
	wp_register_script( 'jquery.validation-engine-es', get_stylesheet_directory_uri() . '/js/vendor/jquery.validationEngine-es.js', array( 'travel-jquery', 'travel-validation-engine' ), false, true);
	wp_register_script( 'jquery.validation-engine-pt', get_stylesheet_directory_uri() . '/js/vendor/jquery.validationEngine-pt_BR.js', array( 'travel-jquery', 'travel-validation-engine' ), false, true);
	wp_register_script( 'travel-validation-engine', get_stylesheet_directory_uri() . '/js/vendor/jquery.validationEngine.js', array(), false, true);
	wp_register_script( 'travel-google-maps', get_stylesheet_directory_uri() . '/js/vendor/maps.js', array(), false, true);
	wp_register_script( 'travel-swipebox', get_stylesheet_directory_uri() . '/js/vendor/jquery.swipebox.min.js', array('travel-jquery'), false, true);
	wp_register_script( 'travel-swipebox-init', get_stylesheet_directory_uri() . '/js/vendor/swipebox.js', array('travel-jquery', 'travel-swipebox'), false, true);
}

add_action( 'wp_enqueue_scripts', 'travel_register_load_script' );

/**
 *
 * Enqueue scripts
 *
 */
 
function travel_load_javascript_conditionally() {

	if ( is_home() ) {

		//loading bjqs for home gallery
		wp_enqueue_script( 'travel-bjqs-core' , false, array( 'travel-jquery' ), false, true);
		wp_enqueue_script( 'travel-bjqs' , false, array( 'travel-jquery' ), false, true);
	}

	if( is_single() || is_page()) {
		//loading Swipebox for single gallery
		wp_enqueue_script( 'travel-swipebox' , false, array( 'travel-jquery' ), false, true);
		wp_enqueue_script( 'travel-swipebox-init' , false, array( 'travel-jquery' ), false, true);
	}

	if( is_page() ) {
		wp_enqueue_script( 'travel-google-maps' , false, array(), false, true);
	}

	//loading jquery 1.10.1
	wp_enqueue_script( 'travel-jquery' , false, array(), false, false);
	//loading jquery validation engine
	wp_enqueue_script( 'travel-validation-engine' , false, array(), false, true);
	//loading Validation Engine language
	wp_enqueue_script( 'jquery.validation-engine-' . qtranxf_getLanguage() , false, array( 'travel-jquery', 'travel-validation-engine' ), false, true);
	//loading bootstrap 3.0
	wp_enqueue_script( 'travel-bootstrap' , false, array( 'travel-jquery' ), false, true);
	//loading main scripts for travel theme
	wp_enqueue_script( 'travel-main' , false, array( 'travel-jquery' ), false, true);
	//loading jquery UI 1.10.4
	wp_enqueue_script( 'travel-jquery-ui' , false, array( 'travel-jquery' ), false, true);
	//loading DatePicker language
	wp_enqueue_script( 'jquery.ui.datepicker-' . qtranxf_getLanguage() , false, array( 'travel-jquery', 'travel-jquery-ui' ), false, true);
}

add_action( 'wp_enqueue_scripts', 'travel_load_javascript_conditionally' );


function load_custom_tr_admin_scripts() {
	wp_register_script( 'travel-admin', get_stylesheet_directory_uri() . '/js/vendor/travel-admin.js', array(), false, true);
	wp_enqueue_script('travel-admin',false, array(), false, true);
}

add_action( 'admin_enqueue_scripts', 'load_custom_tr_admin_scripts' );

/**
 *
 * Enqueue Admin scripts
 *
 */

function load_custom_wp_admin_style() {
	wp_register_style( 'custom_wp_admin_css', get_template_directory_uri() . '/css/admin-style.css', false, '1.0.0' );
	wp_enqueue_style( 'custom_wp_admin_css' );
}
add_action( 'admin_enqueue_scripts', 'load_custom_wp_admin_style' );

/**
 *
 * Get attachments post
 *
 */

function tr_get_single_attachments($post_id) {

	$args = array(
		'post_type' => 'attachment',
		'posts_per_page' => -1,
		'post_status' => null,
		'post_parent' => $post_id
	);

	$attachments = get_posts( $args );
	$gallery = '';

	if ( !empty($attachments) ) {

		foreach ( $attachments as $attachment ) {
			$gallery .= '<li>';
			$gallery .= wp_get_attachment_image( $attachment->ID, 'travel-single-gallery', array() );
			$gallery .= '</li>';
		}
		return $gallery;
	} else {
		return false;
	}
}

/*
 *
 * Load external file to add support for MultiPostThumbnails. Allows you to set more than one "feature image" per post.
 *
 */

require_once('library/multi-post-thumbnail/multi-post-thumbnails.php');

/*
 *
 * Define additional "post thumbnails". Relies on MultiPostThumbnails to work
 *
 */

function createMultiPostObjects() {

	if (class_exists('MultiPostThumbnails')) {

		$thumbs = 2;
		$Sitesthumbs = (empty($GLOBALS['sites'][SC]['featured_images'])) ? 10 : $GLOBALS['sites'][SC]['featured_images'];

		while($thumbs <= $Sitesthumbs) {
			new MultiPostThumbnails(array(
				'label' => $thumbs . 'nd Feature Image',
				'id' => 'feature-image-' . $thumbs,
				'post_type' => 'post'
				)
			);
			$thumbs++;
		}
	}
}

add_action( 'admin_init', 'createMultiPostObjects' );

/*
 * Get Multipost thumbnails.
 */

function getMultiPostThumbnails($number = 10, $post_id = null) {

	if (class_exists('MultiPostThumbnails')) {

		$img = 1;
		$thumbnails = '';

		while ($img <= $number) {

			$image_name = 'feature-image-' . $img;

			if (MultiPostThumbnails::has_post_thumbnail('post', $image_name)) {

				$image_id = MultiPostThumbnails::get_post_thumbnail_id( 'post', $image_name, $post_id );
				$image_thumb_url = wp_get_attachment_image_src( $image_id, 'travel-single-gallery' );
				$image_feature_url = wp_get_attachment_image_src( $image_id, 'feature-image' );
				$image = wp_get_attachment_image( $image_id, 'travel-featured-thumbnail', false, array(
					'class' => 'img-responsive',
					'alt' => trim(strip_tags( get_post_meta($image_id, '_wp_attachment_image_alt', true) )),
					'src' => setImageScheme('travel-single-gallery')
				) );
				$thumbnails .= '<a style="display:none;" href="' . $image_thumb_url[0] . '" class="swipebox" title="' . get_the_title() . '">' . $image . '</a>';
			}
			$img++;
		}
		return $thumbnails;
	}
}

/*
 *
 * Create swipebox gallery.
 *
 */

function setSwipeboxGallery ($post_id = null) {
	if ( empty($post_id) )
		return;

	$thumbnails = (empty($GLOBALS['sites'][SC]['featured_images'])) ? 10 : $GLOBALS['sites'][SC]['featured_images'];

	if ( has_post_thumbnail() ) {

		$image_feature_id = get_post_thumbnail_id( $post_id );
		$image_thumb_url = wp_get_attachment_image_src( $image_feature_id, 'travel-single-gallery' );
		$image_featured = wp_get_attachment_image($image_feature_id, 'travel-single-gallery', false, array(
			'class' => 'img-responsive',
			'alt' => trim(strip_tags( get_post_meta($image_feature_id, '_wp_attachment_image_alt', true) )),
			'src' => setImageScheme('travel-single-gallery')
		) );
		
		$gallery = '<a href="' . $image_thumb_url[0] . '" class="swipebox" title="' . get_the_title() . '"><span class="rollover" ></span>' . $image_featured . '</a>';
		$gallery .= getMultiPostThumbnails($thumbnails, $post_id);
		return $gallery;
	}
}

/*
 *
 * Travels Pagination
 *
 */

function tr_pagination() {
	global $wp_query;
	$big = 999999999;

	$links = paginate_links( array(
		'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
		'format' => '?paged=%#%',
		'current' => max( 1, get_query_var('paged') ),
		'total' => $wp_query->max_num_pages,
		'type' => 'array',
	) );

	echo '<ul class="pagination">';

	if (!empty($links)) {
		foreach($links as $link) {
			echo '<li>' . $link . '</li>';
		}
		echo '</ul>';
	}
}

/**
 *  Custom Excerpt
 */

function the_excerpt_max_charlength($length = null) {
	$excerpt = substr(get_the_excerpt(), 0, $length);
	return $excerpt . ' ...';
}

/**
 *  Create Custom qTranslate Menu
 */

function custom_qTransalte_menu() {

	include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

	if ( is_plugin_active( 'qtranslate-x/qtranslate.php' ) ) {

		$languages = array(
			'es'	=>	'Español',
			'en'	=>	'English',
			'pt'	=>	'Português'
		);

		echo '<div class="dropdown tr-header-dropdown hidden-xs">';
		echo '<a class="btn dropdown-toggle" id="dropdownMenu1" data-toggle="dropdown">';
		echo '<div class="flag qtrans_flag_' .qtranxf_getLanguage(). '" ></div><span class="caret"></span>';
		echo '</a>';
		echo '<ul class="dropdown-menu dropdown-menu-left" role="menu" aria-labelledby="dropdownMenu1">';

		foreach(qtranxf_getSortedLanguages() as $language) {

			echo '<li class="dropdown-flag" role="presentation"><div class="flag flag-position qtrans_flag_'.$language.'" ></div><a role="menuitem" tabindex="-1" href="' .qtranxf_convertURL('', $language, false, true) . '"' . '/>' .$languages[$language]. '</a></li>';

		}
		echo '</ul>';
		echo '</div>';
		
	}
}

/**
 *  Translate Menus with qTranslate Plugin.
 */

if ( function_exists("qtrans_useTermLib") ) {
        function qtrans_menuitem2( $item ) {
                foreach ($item as $key=>$values){
                $item[$key]->title = qtrans_useTermLib($values->title);
        }
        //qtrans_useCurrentLanguageIfNotFoundUseDefaultLanguage( $item->title );
           return $item;
        }
        add_filter('wp_nav_menu_objects', 'qtrans_menuitem2', 0);
 
       function qtrans_menuitem( $menu_item ) {
			$menu_item->title = qtrans_useCurrentLanguageIfNotFoundUseDefaultLanguage( $menu_item->title );
			return $menu_item;
		}
        add_filter('wp_setup_nav_menu_item', 'qtrans_menuitem', 0);

		function qtranslate_menu_item( $menu_item ) {
			if (stripos($menu_item->url, get_site_url()) !== false) {
				$menu_item->url = qtranxf_convertURL($menu_item->url);
			}
			return $menu_item;
		}

		add_filter('wp_setup_nav_menu_item', 'qtranslate_menu_item', 0);

}

/**
 *  Set locale according qTransalte plugin.
 */

add_filter( 'locale', 'set_travels_locale' );

function set_travels_locale() {

	if (function_exists('qtrans_getLanguage')) {
		$locales = array(
			'en'	=>	'en_US',
			'es'	=>	'es_ES',
			'pt'	=>	'pt_BR'
		);

		return $locales[qtranxf_getLanguage()];
	}
}

add_action('after_setup_theme', 'travels_language_setup');

/**
 *  Set language
 */

function travels_language_setup() {
	load_theme_textdomain( 'travels' , get_template_directory() . '/languages');
}

/**
 *  Set Google Ads.
 */

function google_ads($type, $class = null, $format = null) {
	if (empty($type)) {
		return;
	}

	$ads = $GLOBALS['sites'][SC]['ads'];

	if (empty($ads[$type])) {
		return;
	}

	if (in_category($GLOBALS['sites'][SC]['ads']['exclude_categories'])) {
		return;
	}

	$slot = $ads[$type];

	$script = <<<JS
		<div class="{$class}">
		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
			<ins class="adsbygoogle" style="display:block;" data-ad-client="{$ads['pub-id']}" data-ad-slot="{$slot['id']}" data-ad-format="{$format}"></ins>
		<script>
			var tr_google_ad_channel = '{$slot['custom-channel']}';
			(adsbygoogle = window.adsbygoogle || []).push({
      params: { google_ad_channel: tr_google_ad_channel}
    });
		</script>
		</div>
JS;

	return $script;
}

function google_ads_unit() {

		if (in_category($GLOBALS['sites'][SC]['ads']['exclude_categories'])) {
			return;
		}

		$script = <<<JS
		<div>
			<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
				<ins class="adsbygoogle"
				style="display:block"
				data-ad-client="ca-pub-9191705010011352"
				data-ad-slot="4169165763"
				data-ad-format="link"></ins>
			<script>
				(adsbygoogle = window.adsbygoogle || []).push({});
			</script>
		</div>
JS;

	return $script;
}

/**
 *  Create Custom Form.
 */

function tr_custom_form() {

	//Excursiones Tradicionales
	if (is_category(56)) {

		$forms = array(
			'es'	=>	'7263',
			'en'	=>	'7264',
			'pt'	=>	'7265'
		);
		return do_shortcode('[contact-form-7 id="' .$forms[qtranxf_getLanguage()]. '"]');
	}

	//Excursiones a Bodegas
	if (is_category(57)) {

		$forms = array(
			'es'	=>	'7273',
			'en'	=>	'7272',
			'pt'	=>	'7274'
		);
		return do_shortcode('[contact-form-7 id="' .$forms[qtranxf_getLanguage()]. '"]');
	}
}

function print_pre($value) { 
    echo "<pre>",print_r($value, true),"</pre>";
}

/**
 *
 * Rewrite titles for multilanguage
 *
 **/

function filter_pagetitle() {

	if (function_exists('qtrans_getLanguage')) {
		$languages = array(
			'en'	=>	'English',
			'es'	=>	'',
			'pt'	=>	'Português'
		);
		
		return $languages[qtranxf_getLanguage()];
	}
}

add_filter('stylesheet_uri', 'travel_stylesheet_uri', 10, 2);

/**
 * travel_stylesheet_uri
 * overwrite default theme stylesheet uri
 * filter stylesheet_uri
 * @see get_stylesheet_uri()
 */

function travel_stylesheet_uri($stylesheet_uri, $stylesheet_dir_uri) {
	return $stylesheet_dir_uri . '/css/' .$GLOBALS['sites'][SC]['css_folder']. '/style.css';
}

/**
 *
 * Creating multilanguage footer menu.
 *
 **/

if ( function_exists( 'register_nav_menus' ) ) {
	register_nav_menus(
		array(
			'menu-footer-one-es'	=> 'IT Es',
			'menu-footer-one-en'	=> 'IT En',
			'menu-footer-one-pt'	=> 'IT Pt',
			'menu-footer-two-es'	=> 'PT Es',
			'menu-footer-two-en'	=> 'PT En',
			'menu-footer-two-pt'	=> 'PT Pt',
			'menu-footer-three-es'	=> 'GTM Es',
			'menu-footer-three-en'	=> 'GTM En',
			'menu-footer-three-pt'	=> 'GTM Pt',
			'menu-footer-four-es'	=> 'IG Es',
			'menu-footer-four-en'	=> 'IG En',
			'menu-footer-four-pt'	=> 'IG Pt',
			'menu-footer-five-es'	=> 'GTV Es',
			'menu-footer-five-en'	=> 'GTV En',
			'menu-footer-five-pt'	=> 'GTV Pt'
		)
	);
}

/**
 *
 * Get contact form.
 *
 **/

function get_contact_form() {

	if ($GLOBALS['sites'][SC]['plugins']['default_form']['enabled']) {
		$contact_template = dirname(__FILE__) . '/library/leads/contact-form.php';
		if (file_exists($contact_template)) {
			require_once('library/leads/contact-form.php');
		}
	}
}

function defineCols() {

	if ($GLOBALS['sites'][SC]['site_display'] == 'empty') {
		return 'col-sm-12 col-md-12 col-lg-12';
	} else {
		return 'col-sm-12 col-md-6 col-lg-7';
	}
}


/**
 *
 * Get single contact form.
 *
 **/

function get_single_contact_form() {

	$single_form = dirname(__FILE__) . '/library/leads/single-form.php';
	if (file_exists($single_form)) {
		require_once('library/leads/single-form.php');
	}
}

/**
 *
 * Create custom links by categories.
 *
 **/

function custom_category_link() {
	$categories = array();
	$categories = $GLOBALS['sites'][SC]['redirect_categories'];

	if (is_category($categories['pxsol_cat'])) {
		echo $categories['pxsol_uri'];
	} elseif (is_category($categories['despegar_cat'])) {
		echo $categories['despegar_uri'];
	} else {
		echo the_permalink();
	}
}

/**
 *
 * Define sender function for general purpose.
 *
 **/

function default_sender($fromEmail = null, $fromName = null, $recipients = array(), $subject = null, $msg = null, $bcc = null) {
	$config = $GLOBALS['sites'][SC]['email'];
	if (empty($fromEmail))
		$fromEmail = $config['from']['email'];
	if (empty($fromName))
		$fromName = $config['from']['name'];

	require_once( ABSPATH . '/wp-includes/class-phpmailer.php');
	$mail = new PHPMailer(true);

	try {
		$mail->IsSMTP();
		if (!empty($bcc)) {
			if (is_array($bcc)) {
				foreach($bcc as $copy) {
					$mail->addBCC($copy);
				}
			} else {
				$mail->addBCC($bcc);
			}
		}

		$mail->SMTPDebug 	= 0;
		if (true === WP_DEBUG) {
			$mail->SMTPDebug 	= 2;
		}

		$mail->Port 		= $config['port'];
		$mail->SMTPAuth 	= $config['smtpauth'];
		$mail->Username 	= $config['username'];
		$mail->Password 	= $config['password'];
		$mail->SMTPSecure 	= $config['smtpsecure'];
		$mail->Host 		= $config['host'];
		$mail->SetFrom($fromEmail, $fromName);

		if (!empty($recipients)) {
			if (is_array($recipients)) {
				foreach($recipients as $recipient) {
					$mail->AddAddress($recipient);
				}
			} else {
				$mail->AddAddress($recipients);
			}
		}

		$mail->Subject 		= $subject;
		$mail->CharSet 		= 'UTF-8';
		$mail->MsgHTML($msg);
		$mail->Send();
	} catch (phpmailerException $e) {
		write_log('PHPMailer: ' . $e->errorMessage());
		write_log($msg);
	} catch (Exception $e) {
		write_log('PHPMailer: ' . $e->getMessage());
		write_log($msg);
	}
}

/**
 *
 * Define pre debuger.
 *
 **/

function debug($arr = null) {
	echo '<pre>',print_r($arr,1),'</pre>';	
}

/**
 * Create facebook html button.
 *
 **/

function fb_html() {

	$html = <<<HTML
	<div id="facebook" 
	class="fb-like" 
	data-width="150" 
	data-layout="button_count" 
	data-show-faces="false" 
	data-send="false">
	</div>
HTML;
	return $html;
}

/**
 * Create twitter html button.
 *
 **/

function tw_html() {

	$html = <<<HTML
	<a href="https://twitter.com/share" class="twitter-share-button" data-lang="<?=qtranxf_getLanguage();?>">Tweet</a>
HTML;
	return $html;
}

/**
 * Create google html button.
 *
 **/

function gl_html() {
	$html = <<<HTML
		<div class="g-plusone" data-size="medium"></div>
HTML;
	return $html;
}

/**
 * Load social script on document ready function.
 *
 **/

function social_scripts_ready() {

	$fbAppId = $GLOBALS['sites'][SC]['facebook']['appID'];

	if (empty($fbAppId))
		return;

	$languages = array(
		'es'	=>	'es_ES',
		'en'	=>	'en_US',
		'pt'	=>	'pt_BR'
	);

	$button_lang = $languages[qtranxf_getLanguage()];

	$script = <<<JS
		<div id="fb-root"></div>
		<script type="text/javascript">
			$( document ).ready(function() {

				// Load Twitter Button
				var e = document.createElement('script');
				e.type="text/javascript"; e.async = true;
				e.src = 'https://platform.twitter.com/widgets.js';
				document.getElementsByTagName('head')[0].appendChild(e);

				// Load Google + Button
				var po = document.createElement('script');
				po.type = 'text/javascript';
				po.async = true;
				po.src = 'https://apis.google.com/js/plusone.js';
				var s = document.getElementsByTagName('script')[0];
				s.parentNode.insertBefore(po, s);

				// Load Facebook Button
				window.fbAsyncInit = function() {
					FB.init({
						appId      : '{$fbAppId}',
						xfbml      : true,
						version    : 'v2.8'
					});
					FB.AppEvents.logPageView();
					FB.Event.subscribe('send_to_messenger', function(e) {
						if (e.event == 'rendered') {
							console.log("Plugin was rendered");
						}
					});
				};

				(function(d, s, id){
					var js, fjs = d.getElementsByTagName(s)[0];
					if (d.getElementById(id)) {return;}
					js = d.createElement(s); js.id = id;
					js.async = true;
					js.src = "//connect.facebook.net/{$button_lang}/sdk.js";
					fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));

			});
		</script>
JS;
	return $script;
}

/**
 * Show socials scripts
 *
 **/

function socials_scripts() {

	if (is_single() || is_page() || is_category()) {
		echo social_scripts_ready();
	}
}

/**
 * Show image form 
 *
 **/

function image_form() {
	$categories = array();
	$categories = $GLOBALS['sites'][SC]['redirect_categories'];

	if (in_category($categories['pxsol_cat'])) { ?>
		<div class="form-single"><a href="<?php echo $categories['pxsol_uri']; ?>"><img src="<?php echo get_bloginfo('template_url'); ?>/img/<?php echo $GLOBALS['sites'][SC]['static_folder']?>/formulario.png" /></a></div>
	<?php 
	} elseif (in_category($categories['despegar_cat'])) { ?>
		<div class="form-single"><a href="<?php echo $categories['despegar_uri']; ?>"><img src="<?php echo get_bloginfo('template_url'); ?>/img/<?php echo $GLOBALS['sites'][SC]['static_folder']?>/formulario.png" /></a></div>
	<?php
	} else { ?>

	<?php
	}
}

/**
 *
 * Include meta boxes lib
 *
 **/

$meta_boxes_lib = dirname(__FILE__) . '/library/meta-boxes/main.php';

if (file_exists($meta_boxes_lib))
	require_once('library/meta-boxes/main.php');

/**
 *
 * Show contact-form if has email set.
 *
 **/

function show_contact_form($post_id = null) {
	$email = '';

	if (empty($post_id))
		return;

	$email = get_post_meta( $post_id, 'travels_email_contact', true );

	if (!empty($email))
		return get_single_contact_form();
}

/**
 *
 * Get form title
 *
 **/

function get_form_title() {

	$title = get_post_meta( get_the_ID(), 'travels_form_title_' .qtranxf_getLanguage(), true );

	if (!empty($title))
		return $title;
}

/**
 *
 * Get defualt image url.
 *
 **/

function get_default_img() {

	$site = $GLOBALS['sites'][SC]['static_folder'];

	return get_bloginfo( 'stylesheet_directory' ) . '/img/' . $site . '/no-image-available-277x277.png';

}

/**
 *
 * Get analytics code.
 *
 **/

function get_analytics() {

	$ua = $GLOBALS['sites'][SC]['analytics'];

	$script = <<<JS

		<script async src="https://www.googletagmanager.com/gtag/js?id=$ua">
		</script>
		<script>
			window.dataLayer = window.dataLayer || [];
			function gtag(){dataLayer.push(arguments);}
			gtag('js', new Date());
			gtag('config', '$ua');
		</script>
JS;
	return $script;
}

/**
 *
 * Get facebook remarketing code.
 *
 **/

function get_fb_remarketing() {

	$FBPixelId = $GLOBALS['sites'][SC]['facebook']['FBPixelId'];

	$script = <<<JS
<script>
	(function() {
		var _fbq = window._fbq || (window._fbq = []);
		if (!_fbq.loaded) {
			var fbds = document.createElement('script');
			fbds.async = true;
			fbds.src = '//connect.facebook.net/en_US/fbds.js';
			var s = document.getElementsByTagName('script')[0];
			s.parentNode.insertBefore(fbds, s);
			_fbq.loaded = true;
		}
  		_fbq.push(['addPixelId', '$FBPixelId']);
	})();
	window._fbq = window._fbq || [];
	window._fbq.push(['track', 'PixelInitialized', {}]);
</script>
<noscript>
	<img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?id=$FBPixelId&amp;ev=PixelInitialized" />
</noscript>
JS;
	return $script;
}

/**
 *
 * Set Scheme Org. Default: Article
 *
 **/

function set_scheme_org($type = 'Article') {

	return 'itemscope itemtype="http://schema.org/' . $type . '"';
}


function tr_get_tags() {
	$posttags = get_the_tags();
	if ($posttags) {
		$tags = '';
		foreach($posttags as $tag) {
			$tags .= $tag->name . ', ';
		}
		return $tags;
	}
}

function get_excerpt_by_id($post_id){
    $the_post = get_post($post_id); //Gets post ID
    $the_excerpt = $the_post->post_content; //Gets post_content to be used as a basis for the excerpt
    $excerpt_length = 35; //Sets excerpt length by word count
    $the_excerpt = strip_tags(strip_shortcodes($the_excerpt)); //Strips tags and images
    $words = explode(' ', $the_excerpt, $excerpt_length + 1);

    if(count($words) > $excerpt_length) :
        array_pop($words);
        array_push($words, '…');
        $the_excerpt = implode(' ', $words);
    endif;

    $the_excerpt = '<p>' . $the_excerpt . '</p>';

    return $the_excerpt;
}

/**
 *
 * Define Full Social Media Tag Template: Article & Category
 *
 **/

function social_markup() {

	$image_feature_id = get_post_thumbnail_id( get_the_ID() );
	$image_feature_url = wp_get_attachment_image_src( $image_feature_id, 'feature-image' );
	$image = $image_feature_url;
	$title = '';
	$description = '';
	$url = '';
	$section = '';
	$tags = '';

	if (is_single() || is_page() || is_tag()) {

		$title = get_the_title();
		$description = sanitize_text_field(get_excerpt_by_id(get_the_ID()));
		$url = get_the_permalink();
		$category = get_the_category();
		$section = '';
		if (is_single())
			$section = $category[0]->cat_name;
		$tags = tr_get_tags();
	}

	if (is_category()) {

		$category = array();
		$category = get_the_category();
		$category = $category[0];
		$title = $category->name;
		$description = __($category->description);
		$section = $category->name;
		$tags = '';
		$url = get_category_link($category->term_id);
	}

	if (is_home()) {

		$title = get_bloginfo('name');
		$description = __(get_bloginfo('description'));
		$section = get_bloginfo('name');
		$tags = '';
		$url = get_bloginfo('url');
		$image[0] = $url . '/' . UPLOADS . '/' . $GLOBALS['sites'][SC]['social']['Facebook']['default_img'];
	}

	//Get social configuration site.
	$social = $GLOBALS['sites'][SC]['social'];
	$twitter_site = $social['Twitter']['site'];
	$facebook_site = $social['Facebook']['site_name'];
	$facebook_appID = $social['Facebook']['appID'];


	$tags = <<<HTML
		<meta itemprop="name" content="$title">
		<meta itemprop="description" content="$description">
		<meta itemprop="image" content="$image[0]">

		<meta name="twitter:card" content="summary_large_image">
		<meta name="twitter:site" content="$twitter_site">
		<meta name="twitter:title" content="$title">
		<meta name="twitter:description" content="$description">
		<meta name="twitter:creator" content="$twitter_site">
		<meta name="twitter:image:src" content="$image[0]">

		<meta property="og:title" content="$title" />
		<meta property="og:type" content="article" />
		<meta property="og:url" content="$url" />
		<meta property="og:image" content="$image[0]" />
		<meta property="og:description" content="$description" />
		<meta property="og:site_name" content="$facebook_site" />
		<meta property="article:section" content="$section" />
		<meta property="article:tag" content="$tags" />
		<meta property="fb:app_id" content="$facebook_appID" />

HTML;
	return $tags;
}

/**
 *
 * Set track value if single belong to excursions categories.
 *
 */

function setTrackInput($post_id = null) {

	$categories = array();
	$response = array(
		0 => 'false',
		1 => 'true'
	);
	$categories = $GLOBALS['sites'][SC]['excursions_categories'];

	if (empty($post_id))
		return;

	if (empty($categories))
		return;

	return $response[in_category($categories, $post_id)];
}


/**
 * Get the current ID.
 *
 **/

function getTravelID() {
	if (is_category()) {
		return tr_get_category();
	}
	global $post;
	if (is_single()) {
		return $post->ID;
	}
}

/**
 * Get the current category ID.
 *
 **/

function tr_get_category($key = 'term_id') {
	$category = get_the_category();
	return $category[0]->$key;
}

/**
 * Save UTM parameters into $_SESSION variable.
 *
 **/

include(get_template_directory() . '/library/sessions/session.php' );

StartSession();

/**
 * Plugin Name: Enqueue jQuery in Footer
 * Description: Prints jQuery in footer on front-end.
 */

/** Remove Wordpress jquery **/

add_filter( 'wp_default_scripts', 'dequeue_jquery' );

function dequeue_jquery( &$scripts ) {
	if (!is_admin()) {
		$scripts->remove( 'jquery');
	}
}

/**
 * Add Bing Code meta
 */

function getBingMeta() {
	if (!isset($GLOBALS['sites'][SC]['bingcode']))
		return;	

	$bingcode = $GLOBALS['sites'][SC]['bingcode'];
	return '<meta name="msvalidate.01" content="' . $bingcode . '" />';
}

/**
 * Set scheme on images.
 */

function setImageScheme($size = '') {
	if (empty($size))
		return;

	$img_uri = wp_get_attachment_image_src(get_post_thumbnail_id(), $size);
	$full_path = $img_uri[0];

	if (is_ssl()) {
		$full_path = set_url_scheme($full_path, 'https');
		return $full_path;
	}
	return $full_path;
}

/*
 * Set email validator function.
 */

function email_validator() {

	$categories = array();

	if (isset($GLOBALS['sites'][SC]['plugins']['smtp-validate']['enabled']))
		$enabled = $GLOBALS['sites'][SC]['plugins']['smtp-validate']['enabled'];

	if (isset($GLOBALS['sites'][SC]['plugins']['smtp-validate']['enabled']))
		$categories = $GLOBALS['sites'][SC]['plugins']['smtp-validate']['categories'];

	if (empty($enabled))
		return;

	if (!in_category($categories))
		return;

	$jsEmailCheckUrl = get_bloginfo( 'template_url') . '/library/validate-email.php';
	$emailNotValid = __('E-mail address does not exist');
	$jsEmailNotValid = json_encode($emailNotValid);

	$script = <<<JS
		<script>
			var submitButton = $('#tr-send');
			var form = $('form#tr-custom-contact-form');
			var emailInput = $('#tr-email', form);
			var loadingInput = $('.email-container_icon');
			var checkInput = $('.check-container_icon');
			var timesInput = $('.times-container_icon');

			submitButton.attr('disabled', true);

			emailInput.focusin(function() {
				emailInput.popover('hide');
				emailInput.parent('div').removeClass('has-error');
				emailInput.parent('div').removeClass('has-success');
				loadingInput.css('display', 'none');
				checkInput.css('display', 'none');
				timesInput.css('display', 'none');
				submitButton.attr('disabled', true);
			});

			emailInput.focusout(function() {
				var loadingInput = $('.email-container_icon');
				var checkInput = $('.check-container_icon');
				var timesInput = $('.times-container_icon');
				loadingInput.css('display', 'none');
				checkInput.css('display', 'none');
				timesInput.css('display', 'none');
				submitButton.attr('disabled', true);
				var emailValue = $(this).val();

				if (emailValue == '') {
					return;
				}

				loadingInput.css('display', 'block');

				$.ajax({
					type: 'GET',
					url: '{$jsEmailCheckUrl}',
					data: 'email=' + emailValue,
					success: function(response) {
						var data = jQuery.parseJSON(response);
						if (data == true) {
							emailInput.parent('div').addClass('has-success');
							loadingInput.css('display', 'none');
							checkInput.css('display', 'block');
							submitButton.attr('disabled', false);
						}

						if (data == false) {
							$("#tr-email").validationEngine('showPrompt', 'Tu casilla de correo es incorrecta.', 'red', 'bottomLeft', true);
							emailInput.parent('div').addClass('has-error');
							loadingInput.css('display', 'none');
							timesInput.css('display', 'block');
						}
					}
				});
			});
		</script>
JS;
	echo $script;
}

function displayForm($form = 'home') {

	$forms_enabled = array(
		'home',
		'single-top',
		'single-bottom',
		'category'
	);

	if (!in_array($form, $forms_enabled))
		return;

	if (!getBookingLibStatus())
		return;

	$lib = getBookingLibStatus();

	if ($form == 'single-top' || $form == 'single-bottom') {
		if (!getSingleForm($lib)) {
			if ($form == 'single-bottom')
				return;
			$form = 'category';
		} else {
			$lib = getSingleForm();
		}
	}

	require_once('library/' . $lib . '/init.php');

	$dispatcherfunction = $lib . 'dispatcherForm';

	return $dispatcherfunction($form);
}

function getSingleForm($booking = '') {

	$libs = array(
		'despegar',
		'booking'
	);

	global $post;

	if (get_post_meta( $post->ID, 'travels_' . $booking . '_hotel_ID', true ))
		return $booking;

	foreach ($libs as $lib) {
		if (get_post_meta( $post->ID, 'travels_' . $lib . '_hotel_ID', true ))
			return $lib;
	}

	return false;
}

function getBookingLibStatus() {

	$libs = array(
		'despegar',
		'booking'
	);

	foreach ($libs as $lib) {

		$enabled = $GLOBALS['sites'][SC]['plugins'][$lib]['enabled'];

		if ($enabled)
			return $lib;

		continue;
	}
	return false;
}

/*
 * Allow to upload XML files for sitemaps.xml
 */

add_filter('upload_mimes', 'custom_upload_xml');
function custom_upload_xml($mimes) {
	$mimes = array_merge($mimes, array('xml' => 'application/xml'));
	return $mimes;
}

/*
 * Filter AIOSEO description on pagination.
 */

function custom_aioseop_description($description) {
	if (is_paged()) {
		global $wp_query;
		$description = $description . ' - Part ' . $wp_query->query_vars['paged'];
	}
	return $description;
}
add_filter('aioseop_description', 'custom_aioseop_description', 1);

/*
 * Include Tax Meta Class.
 */

require_once('library/Tax-Meta-Class/Tax-meta-class/Tax-meta-class.php');

if (is_admin()) {

	$prefix = 'travels_';
	$config = array(
		'id' => 'travels_meta_box',
		'title' => 'Travels Meta Boxes',
		'pages' => array('category'),
		'context' => 'normal',
		'fields' => array(),
		'local_images' => false,
		'use_with_theme' => false
	);
	$travelMeta = new Tax_Meta_Class($config);
	$travelMeta->addText($prefix . 'email_recipients', array(
		'name' => __('Correo Destinatario', 'tax-meta')
	));
	$travelMeta->addText($prefix . 'cc_recipients', array(
		'name' => __('Copia de Correo (CC)', 'tax-meta')
	));
	$travelMeta->addText($prefix . 'cat_subject_es', array(
		'name' => __('Asunto del Correo Español', 'tax-meta')
	));
	$travelMeta->addText($prefix . 'cat_subject_pt', array(
		'name' => __('Asunto del Correo Português', 'tax-meta')
	));
	$travelMeta->addText($prefix . 'cat_subject_en', array(
		'name' => __('Asunto del Correo Inglés', 'tax-meta')
	));
	$travelMeta->Finish();
}

function track_form() {

	$categories = $GLOBALS['sites'][SC]['excursions_categories'];
	if (in_category($categories)) {
		return 'True';
	}
	return 'False';
}

// REMOVE WP EMOJI
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');

remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );

// Remove WP API JSON
/*
 * Disable WP REST API JSON endpoints if user not logged in
 */
function chuck_disable_rest_endpoints( $access ) {
	if( ! is_user_logged_in() ) {
		return new WP_Error( 'rest_cannot_access', __( 'Only authenticated users can access the REST API.', 'disable-json-api' ), array( 'status' => rest_authorization_required_code() ) );
	}
  return $access;
}
add_filter( 'rest_authentication_errors', 'chuck_disable_rest_endpoints' );

/**
 *
 * Get automatics ads from Google Adsense.
 *
 **/

function get_auto_ads() {

	$pub_id = $GLOBALS['sites'][SC]['ads']['pub-id'];

	$script = <<<JS
		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
		<script>
			(adsbygoogle = window.adsbygoogle || []).push({
			google_ad_client: "$pub_id",
			enable_page_level_ads: true
			});
		</script>
JS;
	return $script;
}