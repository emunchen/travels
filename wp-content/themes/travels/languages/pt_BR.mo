��    3      �  G   L      h     i     w     �     �     �     �     �     �  	   �     �  	   �     �     �  
                  ,     <     L     `     q     ~     �     �     �     �  	   �     �     �  9     
   @     K  5   Q     �     �     �  	   �     �     �  	   �  
   �     �     �     �  2        F     W     j     ~  &   �  '  �     �     �     �     	     	     	     3	     <	     D	     K	  	   `	     j	     p	     �	     �	     �	     �	     �	     �	     �	     
     
  "   ,
     O
     i
     r
  	   �
     �
     �
  ?   �
     �
       =        K  
   e  	   p  	   z     �     �     �     �     �     �     �     �               /     I  #   [                )   ,             *   '           %   .      2                        $   (                                             &             #   !          1                 "   -             	             /            +   
   3             0       %d Passengers %d Room %d Rooms Accept Adults Archives for:  Best prices for hotels in  Book Check In Check Out Check Prices Childrens Close Contact Phone Contact Us E-Mail Eg. City or Hotel Name Featured Hotels Find Your Hotel General Information General Interest Headquarters Legal Advice Looking for the best prices ... Mendoza Travel Guide Message Mon to Fri 9:00am - 6:30pm More Info Name Obtaining Deals ... Oops! Something went wrong. You may be able to try again. Passengers Phone Please Contact us via Email - contacto@mendoza.travel Receive instant response Required Room %d Schedules Search Search Hotels See Price See Prices Send Sending Inquiry ... Terms and Conditions Thank You! Please check your email in few minutes. Tourism Products Tourist Guide Wine Tourist Information You are here: Your message has been sent. Thank you! Project-Id-Version: travels
POT-Creation-Date: 
PO-Revision-Date: 2015-02-04 20:15-0300
Last-Translator: 
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Poedit 1.5.4
Language: pt_BR
 %d Passageiros %s Quarto %s Quartos Aceitar Adulto Arquivos para:  Melhores preços em  Reservar Entrada Saída Verificar os preços Crianças Perto Telefone para Contato Contato E-Mail Eg. Cidade Ou Nome De Hotel Hotéis em Destaque Encontre O Seu Hotel Informações Gerais De Interesse Geral Nossa Empresa Declaração de Privacidade Procurando os melhores preços ... Guia de Viagem em Mendoza Mensagem Lun a Vie | 9:00hs - 18:30hs Mais Info Nome Obtenção de Ofertas ... Ops! Algo deu errado. Você pode ser capaz de tentar novamente. Passageiros Telefone Entre em contato conosco via e-mail - contacto@mendoza.travel Receber resposta imediata Solicitado Quarto %d Horários Procurar Pesquisar Hotel Ver o Preço Ver Preços Enviar Envio de consulta ... Termos do Uso Obrigado! Verifique seu e-mail. Produtos Turísticos Guia Turístico do Vinho Informações Turísticas Você está aqui: Sua mensagem foi enviada. Obrigado! 