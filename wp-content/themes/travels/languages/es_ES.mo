��    5      �  G   l      �     �     �     �     �     �     �     �     �  	   �     �  	                    
   +     6     =     T     d     t     �     �     �     �     �     �     �  	               9   .  
   h     s  5   y     �     �     �  	   �     �     �  	   �       
        (     -     A  2   V     �     �     �     �  &   �  '  �     	     +	     J	     R	     Z	     f	     ~	     �	     �	     �	     �	     �	  
   �	  
   �	     �	     �	     �	     �	     
     #
     8
     L
     ^
      y
     �
     �
     �
  	   �
     �
  !   �
  >     	   M  	   W  ]   a     �  	   �     �     �     �       
             ;     G     N     g  %        �     �     �     �  )   �     5          *   .                 (   !       &   0      4                            )       ,                       +              '             $   "   %      3                 #                	             1            -   
   /             2       %d Passengers %d Room %d Rooms Acept Adults Archives for:  Best prices for hotels in  Book Check In Check Out Check Prices Childrens Close Contact  Contact Phone Contact Us E-Mail Eg. City or Hotel Name Featured Hotels Find Your Hotel General Information General Interest Headquarters Legal Advice Looking for the best prices ... Mendoza Travel Guide Message Mon to Fri 9:00am - 6:30pm More Info Name Obtaining Deals ... Oops! Something went wrong. You may be able to try again. Passengers Phone Please Contact us via Email - contacto@mendoza.travel Receive instant response Required Room %d Schedules Search Search Hotels See Price See Price and Availability See Prices Send Sending Inquiry ... Terms and Conditions Thank You! Please check your email in few minutes. Tourism Products Tourist Guide Wine Tourist Information You are here: Your message has been sent. Thank you! Project-Id-Version: travels
POT-Creation-Date: 
PO-Revision-Date: 2015-02-04 21:11-0300
Last-Translator: 
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Poedit 1.5.4
Language: es_ES
 %d Pasajeros %s Habitación %s Habitaciones Aceptar Adultos Tags para:  Los mejores precios en  Reservar Entrada Salida Consultar Precios Niños Cerrar Contactar  Teléfonos Contactenos E-Mail Ej. Ciudad o Nombre del Hotel Hoteles Destacados Encuentra tu Hotel Información General De Interés General Nuestras Oficinas Declaración de Privacidad Buscando las mejores tarifas ... Guía Turística de Mendoza Mensaje Lun a Vie | 9:00hs a 18:30hs Más Info Nombre Obteniendo Ofertas Especiales ... ¡Ups! Algo salió mal. Por favor intenta de nuevo más tarde. Pasajeros Teléfono Póngase en contacto con nosotros a través del correo electrónico - contacto@mendoza.travel Reciba respuesta al instante Requerido Habitación %d Horarios Buscar Buscar Hoteles Ver Tarifa Ver Tarifa y Disponibilidad Ver Tarifas Enviar Enviando su consulta ... Términos y Condiciones Gracias! En minutos revise su correo. Productos Turísticos Guía Turística del Vino Información Turística Estas Aquí: Gracias! Tu mensaje se envió con éxito! 